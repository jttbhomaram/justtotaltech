<?php
require_once('wp-load.php' );
require_once(ABSPATH . 'wp-admin/includes/taxonomy.php');
//$con = mysqli_connect('localhost','root','jtt@#$sql','jtt') or die(mysqli_error($con));
$con = mysqli_connect('localhost','root','','jtt') or die(mysqli_error($con));
error_reporting(E_ALL);
ini_set('display_errors',1);
require_once ('wp-admin/includes/admin.php');
require_once( ABSPATH . 'wp-admin/includes/file.php' );



function upload_featured_image( $file, $post_id, $desc,$set = false){
    preg_match( '/[^\?]+\.(jpe?g|jpe|gif|png)\b/i', $file, $matches );
    if ( ! $matches ) {
        return new WP_Error( 'image_sideload_failed', __( 'Invalid image URL' ) );
    }

    $file_array = array();
    $file_array['name'] = basename( $matches[0] );

    $file_array['tmp_name'] = download_url( $file ,5);

    if ( is_wp_error( $file_array['tmp_name'] ) ) {
        return $file_array['tmp_name'];
    }

    $id = media_handle_sideload( $file_array, $post_id, $desc );

    if ( is_wp_error( $id ) ) {
        @unlink( $file_array['tmp_name'] );
        return $id;
    }
    if ($set) {
        return set_post_thumbnail( $post_id, $id );
    }else {
        return $id;
    }
}


$sql = "SELECT p.post_id, p.post_title,p.post_created, p.post_short_text,
p.post_text, p.post_featured_image, s.seo_image, s.seo_title, s.seo_url,
s.seo_object_type, c.category_id, c.category_name
FROM `post` AS p
INNER JOIN `seo` AS s ON s.`seo_id` = p.`post_seo_id`
LEFT JOIN `post_category` AS pc ON pc.`post_id` = p.post_id
LEFT JOIN `category` AS c ON c.`category_id` = pc.`category_id` 
WHERE c.`category_id` IS NULL";

$q = mysqli_query($con,$sql) or die(mysqli_error($con));


$post_arr = [];
$x=-1;
while($raw = mysqli_fetch_assoc($q)){
    $x++;
    $post_arr[$raw['post_id']]['category'][] = $raw['category_name'];
    $post_arr[$raw['post_id']]['post'] = [
        'title'=> $raw['post_title'],
        'post_slug'=> str_replace('blog/','',$raw['seo_url']),
        'post_created'=> $raw['post_created'],
        'post_content'=> $raw['post_text'],
        'featured_image_url'=> 'http://www.justtotaltech.co.uk'.$raw['seo_image'],
    ];
}

if ($post_arr) {

    foreach ($post_arr as $row) {
        $cat_id = [];
        if ($row['category']) {
            foreach ($row['category'] as $c) {
                $cat_slug = strtolower(str_replace(' ','-',$c));
                $d = get_category_by_slug($cat_slug);
                if ($d) {
                    $cat_id[] = $d->cat_ID;
                }else {
                    $category = [
                        'cat_name' => $c,
                        'category_description' => null,
                        'category_nicename' => $cat_slug
                    ];
                    $cat_id[] = wp_insert_category($category);
                }
            }
        }

        echo PHP_EOL.'$cat_id --> ';
        print_r($cat_id);
        echo ' ********'.PHP_EOL.PHP_EOL;
        $post = $row['post'];

        $title = $post['title'];
        $post_slug = $post['post_slug'];
        //$post_excerpt = $post['post_excerpt'];
        $post_created = $post['post_created'];
        $post_content = $post['post_content'];
        $featured_image = $post['featured_image_url'];

        preg_match_all('/<img[^>]+>/i',$post_content, $result);
        $img_url = [];
        if (isset($result[0]) && count($result[0]) > 0) {
            $img = [];
            foreach( $result[0] as $img_tag) {
                preg_match_all('/(src)=("[^"]*")/i',$img_tag, $img[]);
            }
            if (count($img) > 0) {
                $x=0;
                foreach ($img as $f) {
                    $x++;

                    $path = trim(str_replace('"','',$f[2][0]));
                    $sp = explode('/',$path);
                    $pr = 'http://www.justtotaltech.co.uk';
                    if (isset($sp[1]) && $sp[1] == 'uploads') {
                        $path = $pr.$path;
                    }
                    $img_url[$x] = str_replace('"','',$path);
                }
            }
        }



        // todo : upload dir
        //$url = wp_upload_dir()['url'];
        $url = ' http://justtotaltech.com/wp-content/uploads/2018/12';

        echo '***********$img_url*********** '.$url.PHP_EOL.PHP_EOL;
        print_r($img_url);


        $post_content = str_replace('http://www.pewinternet.org/files/2015/08',$url,$post_content);
        $post_content = str_replace('http://www.coupofy.com/social-media-in-realtime/static/img',$url,$post_content);
        $post_content = str_replace('http://www.justtotaltech.co.uk/wp-content/uploads/2013/08',$url,$post_content);
        $post_content = str_replace('http://www.justtotaltech.co.uk/wp-content/uploads/2014/03',$url,$post_content);
        $post_content = str_replace('http://www.justtotaltech.co.uk/wp-content/uploads/2015/03',$url,$post_content);
        $post_content = str_replace('http://www.justtotaltech.co.uk/wp-content/uploads/2014/01',$url,$post_content);
        $post_content = str_replace('http://www.justtotaltech.co.uk/uploads/imported',$url,$post_content);
        $post_content = str_replace('http://www.justtotaltech.co.uk/uploads/blog',$url,$post_content);
        $post_content = str_replace('/uploads/imported',$url,$post_content);
        $post_content = str_replace('/uploads/blog',$url,$post_content);
        $post_content = str_replace('http://www.justtotaltech.co.uk/uploads',$url,$post_content);

        $post_id = wp_insert_post([
            'post_author'=>1,
            'post_date_gmt'=> $post_created,
            'post_date'=> $post_created,
            'post_title'=>$title,
            'post_name'=>$post_slug,
            'post_content'=>utf8_encode($post_content),
            'post_status'=>'publish',
            'post_category'=>$cat_id,
        ]);


        if ($post_id) {

            $upload_featured = upload_featured_image($featured_image,$post_id,$title,true);
            echo PHP_EOL.'$post_id --> '.$post_id.' ********'.PHP_EOL.PHP_EOL;var_dump($upload_featured);
            echo PHP_EOL;

            if (count($img_url) > 0) {
                $folder_imported = [];
                $folder_blog = [];
                foreach ($img_url as $file) {
                    $img_id = upload_featured_image($file,$post_id,null);
                    echo PHP_EOL;
                    echo '$img_id : ';
                    var_dump($img_id);
                    echo PHP_EOL;
                }
            }
        }
    }
}

die;




$image_url = 'http://www.justtotaltech.co.uk/uploads/imported/Big-Data-Main-image_-763590693.jpg';


$folder_imported = [];
$folder_blog = [];
$seo_image = [];
$img_url = [];
while($raw = mysqli_fetch_assoc($q)) {
    preg_match_all('/<img[^>]+>/i', $raw['post_text'], $result);
    $seo_image[] = $raw['seo_image'];
    if (isset($result[0]) && count($result[0]) > 0) {
        $img = [];
        foreach ($result[0] as $img_tag) {
            preg_match_all('/(src)=("[^"]*")/i', $img_tag, $img[]);
        }
        if (count($img) > 0) {
            $x=0;
            foreach ($img as $f) {
                $path = trim(str_replace('"','',$f[2][0]));
                $sp = explode('/',$path);
                if (isset($sp[2]) && $sp[2] == 'blog') {
                    $folder_blog[] = $path;
                }else if (isset($sp[2]) && $sp[2] == 'imported') {
                    $folder_imported[] = $path;
                }else {
                    $img_url[] = $path;
                }
            }
        }
    }
}

echo '<pre>';
print_r($folder_imported);
echo '</pre>';

echo '<hr>';

echo '<pre>';
print_r($folder_blog);
echo '</pre>';
echo '<hr>';
echo '<pre>';
print_r($img_url);
echo '</pre>';

die;


