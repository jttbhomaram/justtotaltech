<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */

//define('ALLOW_UNFILTERED_UPLOADS', true);
define( 'WP_AUTO_UPDATE_CORE', false );

if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST'] == 'localhost') {
    define( 'DB_NAME', 'jtt_wp_new' );
    define( 'DB_USER', 'root' );
    define( 'DB_PASSWORD', '' );
    define( 'DB_HOST', 'localhost' );
}else {
    define( 'DB_NAME', 'jtt_com' );
    define( 'DB_USER', 'admin' );
    define( 'DB_PASSWORD', 'P4TrDKvHKuZKxTwx92SqqhTEqeB6tER6');
    define( 'DB_HOST', '206.189.163.178' );
}


/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'dC^e(c~MzE%MCd-*_u)*6(y(GL2 UT_sG)P~1Z]s=Yf6s!.|q[hFqE4#^^+H]h2!' );
define( 'SECURE_AUTH_KEY',  '40F+|5-O%<5Wl|C]6#W*$3g8]L)u1._/{(V5BK-hP2dzD<c%S?qvm#Z!sR*Io&< ' );
define( 'LOGGED_IN_KEY',    ')CDc-}2h>+5D{rQZBoDy3W_v/@Rp;U}QPR/$0<Z0N)Eal6ta@]?l+A~C;`V?7?nL' );
define( 'NONCE_KEY',        'Uxqx?gM8Y]P>GqWo_r4mc,|Sx]y^7y-8yCM6)4Qtx?G^>N@.AJ:[iO<i/5|uj:K]' );
define( 'AUTH_SALT',        '?c13q79x<A/L4m7OH7.}[>_?t_NdE[TlNvKCSfisOA.fQ1WHJ83rS;WuVKUJ5jj:' );
define( 'SECURE_AUTH_SALT', 'DqSk} V<zVV]e[=oE5Z$_};N!.GxE<hEXV5>5+EM%V)Y0lEgN)SA)EI/4/;;aSL-' );
define( 'LOGGED_IN_SALT',   '<`G~!4]%&H$0Mm6S?Obj*XOJC7iq)4N[/<]*Q&PrEGB;8~oJYX#_:9GdD,) 3$H#' );
define( 'NONCE_SALT',       'el?FH@qE%HG!B!`#lgQg$>AL.w)gg >P3t5?Kr:BZjCnb7;/<%Xy4`YAPG+B)_fp' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'jtt_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
    define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );