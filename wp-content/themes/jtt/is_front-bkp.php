<?php
/**
 * Created by IntelliJ IDEA.
 * User: TAG007
 * Date: 12/3/2018
 * Time: 3:23 PM
 */

?>
<div class="clearfix">
    <div class="container">
        <div class="row">
            <div class="recent-posts">
                <?php
                $recent_posts = wp_get_recent_posts(array(
                    'numberposts' => 5,
                    'post_status' => 'publish'
                ));
                $first_post = [];
                $middle_post = [];
                $last_post = [];

                $x=0;
                foreach($recent_posts as $post) :
                    $x++;
                    $thumb = get_the_post_thumbnail_url($post['ID']);
                    $link = get_permalink($post['ID']);
                    $title = $post['post_title'];
                    $content = $post['post_content'];


                    if ($x==1) {
                        $first_post = [
                            'thumb'=>$thumb,
                            'link'=>$link,
                            'title'=>$title,
                            'author'=>get_author_name($post['post_author']),
                            'date'=>get_the_date('d M Y',$post['ID']),
                            'content'=>wp_trim_words( $content, 15, NULL ),
                        ];
                    }

                    if ($x>1 && $x < 5) {
                        $middle_post[] = [
                            'thumb'=>$thumb,
                            'link'=>$link,
                            'title'=>$title,
                            'author'=>get_author_name($post['post_author']),
                            'date'=>get_the_date('d M Y',$post['ID']),
                            'content'=>wp_trim_words( $content, 9, NULL ),
                        ];
                    }

                    if ($x==5) {
                        $last_post = [
                            'thumb'=>$thumb,
                            'link'=>$link,
                            'title'=>$title,
                            'author'=>get_author_name($post['post_author']),
                            'date'=>get_the_date('d M Y',$post['ID']),
                            'content'=>wp_trim_words( $content, 15, NULL ),
                        ];
                    }

                endforeach; wp_reset_query();

                if (count($first_post) > 0) {?>
                    <div class="col-md-4 col-sm-4">
                        <div class="post-box">
                            <div class="first-post">
                                <a href="<?=$first_post['link']?>" class="post_thumbnail" style="background-image: url('<?=$first_post['thumb']?>')"></a>

                                <div class="content-box">
                                    <a href="<?=$first_post['link']?>" class="title"><?=$first_post['title']?></a>

                                    <p class="content"><?=$first_post['content']?></p>
                                    <div class="other-info">
                                        <p class="author"><?=$first_post['author']?></p>
                                        <p class="publish-date"><?=$first_post['date']?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }

                if (count($middle_post) > 0) { ?>

                    <div class="col-md-4 col-sm-4">
                        <div class="post-box">
                            <?php
                            foreach ($middle_post as $p) {
                                ?>
                                <div class="middle-post">
                                    <a href="<?= $p['link']?>" class="post_thumbnail" style="background-image: url('<?=$p['thumb']?>')"></a>

                                    <div class="content-box">
                                        <a href="<?= $p['link'] ?>" class="title"><?= $p['title']?></a>
                                        <!-- <p class="content"><?/*= $p['content']*/?></p>-->

                                        <div class="other-info">
                                            <p class="author"><?=$p['author']?></p>
                                            <p class="publish-date"><?=$p['date']?></p>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <?php
                }

                if (count($last_post) > 0) {?>
                    <div class="col-md-4 col-sm-4">
                        <div class="post-box">
                            <div class="last-post">
                                <a href="<?=$last_post['link']?>" class="post_thumbnail" style="background-image: url('<?=$last_post['thumb']?>')"></a>

                                <div class="content-box">
                                    <a href="<?=$last_post['link']?>" class="title"><?=$last_post['title']?></a>

                                    <p class="content"><?=$last_post['content']?></p>

                                    <div class="other-info">
                                        <p class="author"><?=$last_post['author']?></p>
                                        <p class="publish-date"><?=$last_post['date']?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="clearfix">
        <div class="divider"></div>
    </div>


    <div class="row">
        <div class="col-md-8 col-sm-8" id="left_content">
            <div class="left-content">
                <div class="clearfix">
                    <h1 class="main-title">Random posts</h1>
                    <div class="divider"></div>
                </div>
                <?php
                $page = (get_query_var('paged')) ? get_query_var('paged') : 1;
                query_posts('posts_per_page=10&orderby=rand&paged='.$page);
                if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <div class="row post-group">
                        <div class="col-md-9 col-sm-9">
                            <h1 class="content-post-title">
                                <a href="<?php the_permalink(); ?>"><?php the_title();?></a>
                            </h1>
                            <p class="content">
                                <?=wp_trim_words( get_the_content(), 20, NULL )?>
                            </p>custom-side-bar

                            <div class="other-info">
                                <p class="author"><?php echo get_the_author()?></p>
                                <p class="publish-date"><?=get_the_date('d M Y')?></p>
                            </div>
                        </div>

                        <div class="col-md-3 col-md-3">
                            <a href="<?=get_permalink()?>" class="post_thumbnail" style="background-image: url('<?=get_the_post_thumbnail_url(get_the_ID())?>')"></a>
                        </div>
                    </div>
                <?php
                endwhile; endif;
                wp_reset_query();
                ?>
            </div>
        </div>

        <div class="col-md-4 col-sm-4" id="right_content">
            <div class="clearfix">
                <h1 class="main-title">Popular post</h1>
                <div class="divider"></div>
            </div>

            <div class="custom-side-bar" style="min-height: 350px">
                <?php
                $recent_posts = wp_get_recent_posts(array(
                    'numberposts' => 4,
                    'meta_key'=>'post_views_count',
                    'orderby'=>'meta_value_num',
                    'ignore_sticky_posts' => 1,
                    'date_query' => array(
                        array(
                            'year' => date( 'Y' ),
                            'week' => date( 'W' ),
                        ),
                    ),
                    'order'=>'DESC',
                    'post_status' => 'publish'
                ));

                if (count($recent_posts) == 0) {
                    $recent_posts = wp_get_recent_posts(array(
                        'numberposts' => 4,
                        'meta_key'=>'post_views_count',
                        'orderby'=>'meta_value_num',
                        'ignore_sticky_posts' => 1,
                        'order'=>'DESC',
                        'post_status' => 'publish'
                    ));
                }

                $x=0;
                foreach($recent_posts as $post) {
                    $x++;
                    $thumb = get_the_post_thumbnail_url($post['ID']);
                    $link = get_permalink($post['ID']);
                    $title = $post['post_title'];
                    $content = wp_trim_words( $post['post_content'], 9, NULL );
                    $author = get_author_name($post['post_author']);
                    $date = get_the_date('d M Y',$post['ID']);

                    ?>
                    <div class="number">0<?=$x?></div>
                    <div class="content-box">
                        <a href="<?=$link?>" class="title"><?=$title?></a>
                        <p class="content"><?=$content?></p>
                        <div class="other-info">
                            <p class="author"><?=$author?></p>
                            <p class="publish-date"><?=$date?></p>
                        </div>
                    </div>

                    <?php
                }
                wp_reset_query();
                ?>
            </div>



            <div class="front-footer">
                <div class="footer_social">
                    <a target="_blank" href="https://twitter.com/justtotaltech"><span class="footer_social_img footer_twitter"></span></a>
                    <a target="_blank" href="https://www.facebook.com/justtotaltech"><span class="footer_social_img footer_fb"></span></a>
                    <a target="_blank" href="http://www.linkedin.com/company/just-total-tech-limited"><span class="footer_social_img footer_linkdin"></span></a>
                    <a target="_blank" href="https://plus.google.com/u/0/106305161069044863270/posts"><span class="footer_social_img footer_rss"></span></a>
                    <a target="_blank" href="http://www.youtube.com/user/justtotaltech"><span class="footer_social_img footer_youtube"></span></a>
                </div>
            </div>
        </div>
    </div>
</div>
