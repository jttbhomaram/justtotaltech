<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header();
setPostViews(get_the_ID());
?>
    <div class="single-page">
        <?php
        while ( have_posts() ) : the_post();
            $category = get_the_category();
            ?>
            <div class="post-header-group">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5 col-sm-5">
                            <div class="title-sec">
                                <?php
                                $x=0;
                                foreach ($category as $c ) {
                                    $x++;
                                    echo '<a href="'.site_url().'/category/'.$c->slug.'" class="cat-name">'.$c->name.($x!=count($category)?' / ':'').'</a>';
                                }
                                ?>
                                <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

                                <div class="author">
                                    <div class="avatar">
                                        <?=get_avatar(get_the_author_ID())?>
                                    </div>

                                    <div class="info">
                                        <div class="name"><?=get_the_author_meta( 'display_name',get_the_author_ID())?></div>
                                        <div class="publish-date"><?=get_the_date('d M Y',get_the_ID())?></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-7 col-sm-7">
                            <div class="post-thumbnail">
                                <a href="<?php the_permalink(); ?>">
                                    <?php the_post_thumbnail( 'twentyseventeen-featured-image' ); ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="post-content-group">
                <div class="container">
                    <div class="row">
                        <div class="col-md-2 col-sm-1 col-xs-1" id="social-sidebar">
                            <div id="post-sharing">
                                <?php
                                $share_link = get_permalink();
                                $share_title = get_the_title();
                                $share_thumb = get_the_post_thumbnail_url();
                                $share_content = wp_trim_words( get_the_content(), 33, NULL);
                                ?>
                                <a target="_blank" href="#" onclick="window.open('http://www.facebook.com/sharer.php?u=<?=$share_link?>&amp;t=<?=$share_title?>', 'facebookShare', 'width=626,height=436'); return false;"><i class="fa fa-facebook-square"></i></a>
                                <a target="_blank" href="#" onclick="window.open('http://twitter.com/share?text=<?=$share_title?> -&amp;url=<?=$share_link?>', 'twitterShare', 'width=626,height=436'); return false;"><i class="fa fa-twitter-square"></i></a>

                                <a target="_blank" href="#" onclick="window.open('https://www.linkedin.com/sharing/share-offsite/?url=<?=$share_link?>&amp;media=<?=esc_url($share_thumb)?>&amp;description=<?php the_title(); ?><?=$share_title?>', 'pinterestShare', 'width=750,height=350'); return false;"><i class="fa fa-linkedin"></i></a>
                                <!--<a href="#"><i class="fa fa-google-plus-square"></i></a>-->

                                <a target="_blank" href="#" onclick="window.open('http://pinterest.com/pin/create/button/?url=<?=$share_link?>&amp;media=<?=esc_url($thumb)?>&amp;description=<?=$share_link?>', 'pinterestShare', 'width=750,height=350'); return false;"><i class="fa fa-pinterest-square"></i></a>

                            </div>
                        </div>

                        <div id="single_post_content" class="col-md-9 col-sm-10 col-xs-11">
                            <div class="entry-content">
                                <?php
                                the_content( sprintf(
                                    __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentyseventeen' ),
                                    get_the_title()
                                ) );
                                wp_link_pages( array(
                                    'before'      => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
                                    'after'       => '</div>',
                                    'link_before' => '<span class="page-number">',
                                    'link_after'  => '</span>',
                                ) );

                                ?>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <?php
                            $prevPost = get_previous_post();
                            $nextPost = get_next_post();
                            ?>
                            <div class="pagination-links">
                                <?php
                                $ID = '';
                                $heading = '';
                                $title = '';
                                $content = '';
                                $thumb = '';
                                $link = '';
                                $date = '';
                                $author = '';
                                if ($nextPost) {
                                    $ID = $nextPost->ID;
                                    $heading = 'Read next';
                                    $title = $nextPost->post_title;
                                    $content = $nextPost->post_content;
                                    $thumb = get_the_post_thumbnail_url( $nextPost->ID );
                                    $link = get_permalink( $nextPost->ID );
                                    $date = get_the_date('d M Y',$nextPost->ID);
                                    $author = get_the_author_meta( 'display_name',$nextPost->post_author);
                                } else {
                                    $ID = $prevPost->ID;
                                    $heading = 'Read previous';
                                    $title = $prevPost->post_title;
                                    $content = $prevPost->post_content;
                                    $thumb = get_the_post_thumbnail_url( $prevPost->ID );
                                    $link = get_permalink( $prevPost->ID );
                                    $date = get_the_date('d M Y',$prevPost->ID);
                                    $author = get_the_author_meta( 'display_name',$prevPost->post_author);
                                }
                                $content = wp_trim_words( $content, 8, NULL);
                                ?>

                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <h1 class="title"><?=$heading?></h1>
                                        <a href="<?=$link?>" class="post-title"><?=$title?></a>
                                        <a href="<?=$link?>" class="post-content"><?=$content?></a>
                                        <p class="post-author"><?=$author.' &bull; '.$date?></p>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <a href="<?=$link?>">
                                            <img src="<?=$thumb?>" class="post_thumbnail">
                                        </a>
                                    </div>
                                </div>

                                <div class="row related-post">
                                    <?php

                                    $related = get_posts( [
                                        'category__in' => wp_get_post_categories($post->ID),
                                        'numberposts' => 3,
                                        'post__not_in' => [$post->ID,$ID]
                                    ] );
                                    if( $related ) {
                                        foreach( $related as $post ) {
                                            setup_postdata($post);
                                            $thumb = get_the_post_thumbnail_url();
                                            $content = wp_trim_words( get_the_content(), 8, NULL);
                                            $date = get_the_date('d M Y',$post->ID);
                                            $author = get_the_author_meta( 'display_name',$post->post_author);
                                            ?>
                                            <div class="col-md-4">
                                                <a href="<?php the_permalink() ?>" class="thumb" style="background-image: url('<?=$thumb?>')"></a>


                                                <a href="<?php the_permalink() ?>" class="post-title"><?php the_title(); ?></a>
                                                <a href="<?php the_permalink() ?>" class="post-content"><?=$content?></a>
                                                <p class="post-author"><?=$author.' &bull; '.$date?></p>
                                            </div>
                                            <?php
                                        }
                                    }
                                    wp_reset_postdata();
                                    ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



        <?php endwhile; ?>

    </div>



<?php get_footer();
