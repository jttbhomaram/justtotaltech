<?php
/* Template Name: new home page */
get_header();

?>

    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/custom-new.css">






<?php
the_content();
?>


<div class="blog-page">
<section class="blog-sec">
    <div class="container">
        <div class="main-blk">
            <div class="left-blk">
                <div class="sidebar">
                <ul>
                    <li>
                        <a class="active" href="">New/Trending</a>
                    </li>
                    <li>
                        <a href="">Digital Marketing</a>
                    </li>
                    <li>
                        <a href="">Customer Support</a>
                    </li>
                    <li>
                        <a href="">View all</a>
                    </li>
                </ul>
                <h3>Sign up to our product newsletter</h3>
                <form>
                <input type="text" placeholder="Enter email address">
                </form>
                <h4>Follow us</h4>

                <a target="_blank" href="http://www.linkedin.com/company/just-total-tech-limited">
                    <img alt="logo" src="<?php bloginfo('template_directory'); ?>/assets/images/twitter.svg">
                </a>
                <a target="_blank" href="https://plus.google.com/u/0/106305161069044863270/posts">
                    <img alt="logo" src="<?php bloginfo('template_directory'); ?>/assets/images/linkedin.svg">
                </a>
                <a target="_blank" href="http://www.youtube.com/user/justtotaltech">
                    <img alt="logo" src="<?php bloginfo('template_directory'); ?>/assets/images/facebook.svg">
                </a>
                <p>Copyright © 2012 - 2020 Just Total Tech</p>
                </div>
            </div>
            <div class="right-blk">
                <div class="inn-blk">
                    <div class="left-inn">
                      <img alt="logo" src="<?php bloginfo('template_directory'); ?>/assets/images/blog/blog-img.png">
                    </div>
                    <div class="right-inn">
                        <h4 class="small-title">Customer support</h4>
                    <h3 class="main-title">10 best practices to improve customer support with help desk software</h3>
                    <p>Offering a top-rate customer support and resolve issues and ...</p>
                    <span class="author-name">Guy Alexander</span>
                    <span class="date">33 mins ago | 4 min read</span>
                    </div>
                </div>
                <ul>
                    <li>
                        <img alt="logo" src="<?php bloginfo('template_directory'); ?>/assets/images/blog/image-01.png">
                        <h4 class="small-title">Customer  support</h4>
                        <h3 class="main-title">We need you to help cronycle get even better</h3>
                        <p>Offering a top-rate customer support and resolve issues and queries …</p>
                        <span class="author-name">Guy Alexander</span>
                        <span class="date">33 mins ago | 4 min read</span>
                    </li>
                    <li>
                        <img alt="logo" src="<?php bloginfo('template_directory'); ?>/assets/images/blog/image-01.png">
                        <h4 class="small-title">Customer  support</h4>
                        <h3 class="main-title">We need you to help cronycle get even better</h3>
                        <p>Offering a top-rate customer support and resolve issues and queries …</p>
                        <span class="author-name">Guy Alexander</span>
                        <span class="date">33 mins ago | 4 min read</span>
                    </li>
                    <li>
                        <img alt="logo" src="<?php bloginfo('template_directory'); ?>/assets/images/blog/image-01.png">
                        <h4 class="small-title">Customer  support</h4>
                        <h3 class="main-title">We need you to help cronycle get even better</h3>
                        <p>Offering a top-rate customer support and resolve issues and queries …</p>
                        <span class="author-name">Guy Alexander</span>
                        <span class="date">33 mins ago | 4 min read</span>
                    </li>
                </ul>
                <ul class="sec-list">
                    <li>
                        <img alt="logo" src="<?php bloginfo('template_directory'); ?>/assets/images/blog/big-img-01.png">
                        <h4 class="small-title">Customer  support</h4>
                        <h3 class="main-title">We need you to help cronycle get even better</h3>
                        <p>Offering a top-rate customer support and resolve issues and queries …</p>
                        <span class="author-name">Guy Alexander</span>
                        <span class="date">33 mins ago | 4 min read</span>
                    </li>
                    <li>
                        <img alt="logo" src="<?php bloginfo('template_directory'); ?>/assets/images/blog/big-img-01.png">
                        <h4 class="small-title">Customer  support</h4>
                        <h3 class="main-title">We need you to help cronycle get even better</h3>
                        <p>Offering a top-rate customer support and resolve issues and queries …</p>
                        <span class="author-name">Guy Alexander</span>
                        <span class="date">33 mins ago | 4 min read</span>
                    </li>
                </ul>
                <div class="newsletter-blk middle">
                    <div class="left-blk">
                        <h4 class="small-title">Customer  support</h4>
                        <h2 class="main-title">10 best practices to improve customer support with help desk software</h2>
                    </div>
                    <div class="right-blk">
                        <p>Offering a top-rate customer support and resolve issues and ...</p>
                        <span class="author-name">Guy Alexander</span>
                        <span class="date">33 mins ago | 4 min read</span>
                    </div>
                </div>
                <ul>
                    <li>
                        <img alt="logo" src="<?php bloginfo('template_directory'); ?>/assets/images/blog/image-01.png">
                        <h4 class="small-title">Customer  support</h4>
                        <h3 class="main-title">We need you to help cronycle get even better</h3>
                        <p>Offering a top-rate customer support and resolve issues and queries …</p>
                        <span class="author-name">Guy Alexander</span>
                        <span class="date">33 mins ago | 4 min read</span>
                    </li>
                    <li>
                        <img alt="logo" src="<?php bloginfo('template_directory'); ?>/assets/images/blog/image-01.png">
                        <h4 class="small-title">Customer  support</h4>
                        <h3 class="main-title">We need you to help cronycle get even better</h3>
                        <p>Offering a top-rate customer support and resolve issues and queries …</p>
                        <span class="author-name">Guy Alexander</span>
                        <span class="date">33 mins ago | 4 min read</span>
                    </li>
                    <li>
                        <img alt="logo" src="<?php bloginfo('template_directory'); ?>/assets/images/blog/image-01.png">
                        <h4 class="small-title">Customer  support</h4>
                        <h3 class="main-title">We need you to help cronycle get even better</h3>
                        <p>Offering a top-rate customer support and resolve issues and queries …</p>
                        <span class="author-name">Guy Alexander</span>
                        <span class="date">33 mins ago | 4 min read</span>
                    </li>
                </ul>
                <div class="newsletter-blk">
                    <div class="left-blk">
                        <h2 class="main-title">Sign up to newsletter</h2>
                        <p>News insights you won’t delete. Delivered to your inbox weekly.</p>
                    </div>
                    <div class="right-blk">
                        <input type="text" placeholder="Enter email address">
                    </div>
                </div>
            </div>
        </div>
        <hr>
    </div>
</section>
</div>








<?php get_footer(); ?>