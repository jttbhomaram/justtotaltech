<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<style>
    .post-categories li:first-child{
        display: inline-block;
        margin: 0 !important;
        border-bottom: 0 !important;
        width: 100% !important;
        padding: 10px 0 16px 0;
    }
    .post-categories{
        margin: 0 !important;
        padding: 0;
    }
    .post-categories li{
        display: none;
    }
    .post-categories li a{
        color: #898989;
        text-transform: uppercase;
        font-size: 12px;
    }
    .three-block img{
        height: 185px;
        object-fit: cover;
        object-position: center;
        width: 100%;
    }

    .blog-category .blog-list{
        flex-wrap: wrap;
    }
    .three-block{
        width: 30% !important;
        margin-bottom: 45px;
    }

    .active-link{
        color: #0000ff !important;
    }

    .page-header .page-title{
        margin-bottom: 20px;
    }
    .page-content p{
        padding: 0 0 30px 0;
    }
    .blog-category .blog-list .three-block{
        margin-right: 45px !important;
    }
    .blog-category .blog-list .three-block:nth-child(3n){
        margin-right: 0 !important;
    }
    .blog-category .blog-list .three-block:nth-child(2n){
        margin-left: 0 !important;
    }
    .blog-category{
        min-height: calc(100vh - 310px);
    }

    .pagination{
        margin: auto;
        margin-top: 60px;
        margin-bottom: 60px;
        width: 32%;
        border-bottom: 0 !important;
    }
    .pagination h2{
        display: none;
    }
    .pagination .page-numbers{
        /*margin: 0 25px 0 0;*/
        padding: 0 !important;
    }
    .pagination .page-numbers {
        color: #898989;
    }
    .pagination .current {
        color: #262626;
    }
    .pagination .icon{
        display: none;
    }


    /*.blog-category{*/
        /*height: 100vh;*/
    /*}*/
</style>
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/custom-new.css">
    <link href="<?php bloginfo('template_directory'); ?>/assets/css/custom-new-2.css" media="screen" rel="stylesheet"
          type="text/css">

    <div class="blog-category">
        <div class="container">

        </div>
        <hr>
            <div class="container">
            <div class="customer-support">
                <div class="d-flex filter-panel align-center space-between">
                    <h3><?php the_archive_title(); ?></h3>
<!--                    <a class="recent" href="#" >Most Recent</a>-->
                </div>
            </div>


            <div class="blog-list d-flex">
<?php
if ( have_posts() ) : ?>
    <?php
    /* Start the Loop */
    $i=0;
    while ( have_posts() ) : the_post();

        if($i==3){
//     echo '<div class="clearfix"></div>';
            $i=0;
        }
        $i++;
        /*
         * Include the Post-Format-specific template for the content.
         * If you want to override this in a child theme, then include a file
         * called content-___.php (where ___ is the Post Format name) and that will be used instead.
         */
        ?>
                <div class="three-block">

                     <a href="<?php the_permalink(); ?>">  <div class="img-wrap"><?php
                        the_post_thumbnail( 'blog-new-thumbnail', array( 'alt' => get_the_title() ) );
                         ?> <div class="image-placeholder"></div> </div></a>
                    <h4 class="title"><?php echo get_the_category_list();?></h4>
                    <a href="<?php the_permalink(); ?>"> <h2><span class="hover-line"><?php echo  get_the_title() ;?></span></h2></a>
                    <p class="content"><?php echo wp_trim_words( get_the_content(), 9, '...' );?></p>
                    <div class="d-flex bottom-stick">
                        <h5 class="name"><?php the_author(); ?></h5>
                        <p class="time-ago"><?php echo get_the_date();?></p>
                        <p class="bid-time"><?php echo do_shortcode('[rt_reading_time]');?> Min read</p>
                    </div>

                </div>
    <?php
    endwhile;
    ?>
    <div class="clearfix"></div>
    <?php
    the_posts_pagination( array(
        'prev_text' => twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '<span class="screen-reader-text">' . __( 'Previous page', 'twentyseventeen' ) . '</span>',
        'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'twentyseventeen' ) . '</span>' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ),
        'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( '', 'twentyseventeen' ) . ' </span>',
    ) );

else :
//    echo '<h2 class="no-content">Content on this topic is coming soon, subscribe to not miss it</h2>';
//    get_template_part( 'template-parts/post/content', 'none' );
    echo '<img class="no-blog" src="https://justtotaltech.com/wp-content/themes/jtt/assets/images/blog/no-blog.png" alt="no-blog">';

endif; ?>


            </div>



        </div>
    </div>




    <script defer>
        jQuery(function($) {
            var path = window.location.href; // because the 'href' property of the DOM element is the absolute path
            var url_path = path.split('/')[4];
            $('.tabs li a').each(function() {
                var href_string = $(this).attr("href");
                var fields = href_string.split('/');
                var street = fields[4];
                if (url_path === street) {
                    $(this).addClass('active-link');
                    $('.tabs li a').removeClass('active');
                    $('#customer-category').removeClass('active');
                }
            });
        });
    </script>



    <script defer>
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();

            if(scroll >= 10) {
                $(".subscribe-blk").addClass("change");
            } else {
                $(".subscribe-blk").removeClass("change");
            }
        });
    </script>

    <script defer>
        $(document).ready(function () {
            $("#close").click(function () {
                $("#subscribe-blk").hide();
            });
        });
    </script>


<!--    <script defer>-->
<!--        if($(window).width() < 767)-->
<!--        {-->
<!--            $('.img-wrap img').attr('width', 620).attr('height', 350);-->
<!--        }-->
<!--        else {-->
<!--            $('.img-wrap img').attr('width', '').attr('height', '');-->
<!--        }-->
<!--    </script>-->


<?php get_footer();
