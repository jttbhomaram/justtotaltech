<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>


    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/custom-new.css">

<style>
    .category-page {
        min-height: calc(100vh - 340px);
    }
</style>

    <div class="category-page">
        <div class="container">
            <div class="clearfix ">
<!--                <h1 class="page-title">--><?php //_e( 'Oops! That page can&rsquo;t be found.', 'twentyseventeen' ); ?><!--</h1>-->

                <div class="page-content">
<!--                    <p>--><?php //_e( 'It looks like nothing was found at this location. Maybe try a search?', 'twentyseventeen' ); ?><!--</p>-->

<!--                    --><?php //get_search_form(); ?>

                    <div class="not-found">
                        <div class="main-blk">
                            <div class="left-blk">
                                <h2>404<br> page not found</h2>
                                <p>The page you were looking for could not be found. It might have been removed, renamed, or did not exist in the first place.</p>
                                <a href="https://justtotaltech.com/">Go Back to Home</a>
                            </div>
                            <div class="right-blk">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/images/page-not-found.png" alt="page-not-found">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>



<?php get_footer();
