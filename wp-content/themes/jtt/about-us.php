<?php
/* Template Name: About us page */
get_header(); ?>
<link href="<?php bloginfo('template_directory'); ?>/assets/css/custom-new-2.css" media="screen" rel="stylesheet"
      type="text/css">

    <div class="about-us">
        <div class="container">
            <div class="top-banner">
                <h1 class="text-center">Some nice headline here about<br> the company that discribes<br> small bio about it.</h1>
            </div>
            <div class="who-are-we d-flex align-center space-between">
                <div class="left-blk">
                    <h2>Who are we</h2>
                    <p>Established in 2008, Just Total Tech’s UK office is a fully fledged, cross-divisional operation, that supports a growing clientbase throughout United Kingdom. We are an IT company with competences in providing strategic solutions to clients needs and what can make client business spring up.</p>
                </div>
                <div class="right-blk">
                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/about-us/who-are-we.png" alt="who-are-we">
                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/about-us/blue-dot.svg" alt="who-are-we" class="blue-dot">
                </div>
            </div>
<!--            <div class="team">-->
<!--                <h2 class="text-center">Team of Creators</h2>-->
<!--                <p class="text-center">We are proud of ourself in our area of speciality and compentence such as<br> Web Design and development.</p>-->
<!--                <div class="team-members d-flex">-->
<!--                    <div class="block">-->
<!--                        <div class="relative">-->
<!--                            <img src="--><?php //bloginfo('template_directory'); ?><!--/assets/images/about-us/soham.png" alt="who-are-we">-->
<!--                            <a href="#" target="_blank">-->
<!--                                <img src="--><?php //bloginfo('template_directory'); ?><!--/assets/images/about-us/linkedin.svg" alt="who-are-we" class="linkedin">-->
<!--                            </a>-->
<!--                        </div>-->
<!--                        <h3>Soham Cooper</h3>-->
<!--                        <p>Product designer</p>-->
<!--                    </div>-->
<!--                    <div class="block">-->
<!--                        <div class="relative">-->
<!--                            <img src="--><?php //bloginfo('template_directory'); ?><!--/assets/images/about-us/mychell.png" alt="who-are-we">-->
<!--                            <a href="#" target="_blank">-->
<!--                                <img src="--><?php //bloginfo('template_directory'); ?><!--/assets/images/about-us/linkedin.svg" alt="who-are-we" class="linkedin">-->
<!--                            </a>-->
<!--                        </div>-->
<!--                        <h3>Mitchell Simmmons</h3>-->
<!--                        <p>team manager</p>-->
<!--                    </div>-->
<!--                    <div class="block">-->
<!--                        <div class="relative">-->
<!--                            <img src="--><?php //bloginfo('template_directory'); ?><!--/assets/images/about-us/morris.png" alt="who-are-we">-->
<!--                            <a href="#" target="_blank">-->
<!--                                <img src="--><?php //bloginfo('template_directory'); ?><!--/assets/images/about-us/linkedin.svg" alt="who-are-we" class="linkedin">-->
<!--                            </a>-->
<!--                        </div>-->
<!--                        <h3>Morris Flores</h3>-->
<!--                        <p>UI/UX designer</p>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="team-members d-flex">-->
<!--                    <div class="block">-->
<!--                        <div class="relative">-->
<!--                            <img src="--><?php //bloginfo('template_directory'); ?><!--/assets/images/about-us/Marvin.png" alt="who-are-we">-->
<!--                            <a href="#" target="_blank">-->
<!--                                <img src="--><?php //bloginfo('template_directory'); ?><!--/assets/images/about-us/linkedin.svg" alt="who-are-we" class="linkedin">-->
<!--                            </a>-->
<!--                        </div>-->
<!--                        <h3>Marvin Miles</h3>-->
<!--                        <p>Social media manager</p>-->
<!--                    </div>-->
<!--                    <div class="block">-->
<!--                        <div class="relative">-->
<!--                            <img src="--><?php //bloginfo('template_directory'); ?><!--/assets/images/about-us/tech-head.png" alt="who-are-we">-->
<!--                            <a href="#" target="_blank">-->
<!--                                <img src="--><?php //bloginfo('template_directory'); ?><!--/assets/images/about-us/linkedin.svg" alt="who-are-we" class="linkedin">-->
<!--                            </a>-->
<!--                        </div>-->
<!--                        <h3>Mitchell Simmmons</h3>-->
<!--                        <p>technical head</p>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
        </div>
    </div>


<?php get_footer(); ?>
