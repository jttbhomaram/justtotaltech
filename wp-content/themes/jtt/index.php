<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header();?>
<?php $author_id = get_the_author_meta('ID');
$post_id = get_queried_object_id();

?>


    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/custom-new.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/custom-new-2.css">
<style>
    @media (max-width: 768px) {
        .blog-page .main-blk .right-blk .inn-blk .left-inn .img-wrap img {
            height: 200px !important ;
            width: 620px ;
        }
    }
</style>

    <div class="blog-page">
        <section class="blog-sec custom-blog">
            <div class="container">
                <div class="main-blk">

                    <div class="right-blk right-block-jtt right-order-blk" id="tab-1">

                        <div class="inn-blk">
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'post_status' => 'publish',
                                'posts_per_page'=>'1',
                                'category__not_in' => 25
                            );
                            $the_query = new WP_Query( $args );
                            $url= wp_get_attachment_image_src( get_post_thumbnail_id(),1000 );
                            $image_thumb=get_the_post_thumbnail( $post_id, array(410,432) );
                            $url = get_the_post_thumbnail_url( $post_id, 'medium' );
                            ?>
                            <?php if ( $the_query->have_posts() ) { ?>
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post();
                                    $read_url= wp_get_attachment_image_src( get_post_thumbnail_id(),'large' );
                                    ?>
                                    <div class="left-inn">
                                        <!--                                <img alt="logo" src="--><?php //bloginfo('template_directory'); ?><!--/assets/images/blog/blog-img.png">-->
                                        <a href="<?php the_permalink(); ?>"><div class="img-wrap"><img src="<?php echo $read_url[0] ?>" alt="<?php echo get_the_title()?>"/> <div class="image-placeholder"></div> </div></a>

                                    </div>
                                    <div class="right-inn">
                                        <h4 class="small-title"><?php echo get_the_category_list();?></h4>
                                        <a href="<?php the_permalink(); ?>"><h1 class="main-title"><span class="hover-line"><?php echo  get_the_title() ;?></span></h1></a>
                                        <p><?php echo wp_trim_words( get_the_excerpt(), 25, '...' );?></p>
                                        <!--                                <p>--><?php //echo get_the_excerpt();?><!--</p>-->
                                        <div class="author-data">
                                            <span class="author-name"><?php the_author(); ?></span>
                                            <span class="date"><?php echo get_the_date();?> | <?php echo do_shortcode('[rt_reading_time]');?> Min Read</span>
                                        </div>
                                    </div>
                                <?php endwhile; ?>

                                <?php wp_reset_postdata(); ?>
                            <?php } ?>
                        </div>

                        <h2 class="main-title new-title">Reviews & Gadgets</h2>
                        <ul class="sec-list">
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'post_status' => 'publish',
                                'posts_per_page'=>'2',
//    'meta_key' => 'my_post_viewed',
//    'orderby' => 'meta_value_num',
//    'order'=> 'DESC',
                                'category_name' => 'reviews'

                            );
                            $the_query = new WP_Query( $args );
                            $url= wp_get_attachment_image_src( get_post_thumbnail_id(),1000 );
                            $image_thumb=get_the_post_thumbnail( $post_id, array(410,432) );
                            $url = get_the_post_thumbnail_url( $post_id, 'blog-two-block' );
                            ?>
                            <?php if ( $the_query->have_posts() ) { ?>
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post();
                                    $read_url= wp_get_attachment_image_src( get_post_thumbnail_id(),'blog-two-block' );
                                    ?>
                                    <li>
                                        <a href="<?php the_permalink(); ?>">  <div class="img-wrap"> <img src="<?php echo $read_url[0] ?>" alt="<?php echo get_the_title()?>"/> <div class="image-placeholder"></div> </div></a>
                                        <h4 class="small-title"><?php echo get_the_category_list();?></h4>
                                        <a href="<?php the_permalink(); ?>"><h2 class="main-title"><span class="hover-line"><?php echo  get_the_title() ;?></span></h2></a>
                                        <p><?php echo wp_trim_words( get_the_excerpt(), 25, '...' );?></p>
                                        <div class="author-data">
                                            <span class="author-name"><?php the_author(); ?></span>
                                            <span class="date"><?php echo get_the_date();?> | <?php echo do_shortcode('[rt_reading_time]');?> Min Read</span>
                                        </div>
                                    </li>

                                <?php endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                            <?php } ?>
                        </ul>

                        <h2 class="main-title new-title">Trending Blogs</h2>
                        <ul class="second-row">
<!--                            --><?php
//                            $args = array(
//                                'post_type' => 'post',
//                                'post_status' => 'publish',
//                                'posts_per_page'=>'3',
//                                'offset'=>'1',
//                                'category__not_in' => 25
//                            );


                            $args = array(
                                'post_type' => 'post',
                                'post_status' => 'publish',
                                'category__not_in' => 25,
                                'posts_per_page' => 3,
//                        'meta_key'       => 'my_post_viewed',
                                'meta_key' => 'post_views_count',
                                'orderby' => 'meta_value_num', 'date',
                                'date_query' => array(
                                    array(
                                        'after'     => '-60 days',
                                        'column' => 'post_date',
                                    ),
                                ),
                                'paged' => $paged
                            );


                            $the_query = new WP_Query( $args );
                            $url= wp_get_attachment_image_src( get_post_thumbnail_id(),1000 );
                            $image_thumb=get_the_post_thumbnail( $post_id, array(410,432) );
                            $url = get_the_post_thumbnail_url( $post_id, 'blog-three-block' );
                            ?>
                            <?php if ( $the_query->have_posts() ) { ?>
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post();
                                    $read_url= wp_get_attachment_image_src( get_post_thumbnail_id(),'blog-three-block' );
                                    ?>
                                    <li>
                                        <!--                                <img alt="logo" src="--><?php //bloginfo('template_directory'); ?><!--/assets/images/blog/image-01.png">-->
                                        <a href="<?php the_permalink(); ?>"> <div class="img-wrap"><img src="<?php echo $read_url[0] ?>" alt="<?php echo get_the_title()?>"/> <div class="image-placeholder"></div> </div></a>

                                        <h4 class="small-title"><?php echo get_the_category_list();?></h4>
                                        <a href="<?php the_permalink(); ?>"><h2 class="main-title"><span class="hover-line"><?php echo  get_the_title() ;?></span></h2></a>
                                        <p><?php echo wp_trim_words( get_the_excerpt(), 25, '...' );?></p>
                                        <div class="author-data">
                                            <span class="author-name"><?php the_author(); ?></span>
                                            <span class="date"><?php echo get_the_date();?> | <?php echo do_shortcode('[rt_reading_time]');?> Min Read</span>
                                        </div>
                                    </li>
                                <?php endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                            <?php } ?>
                        </ul>



                        <h2 class="main-title new-title">Latest Blogs</h2>
                        <ul class="second-row latest-blog">
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'post_status' => 'publish',
                                'posts_per_page'=>'9',
                                'offset'  => '1',
                                'category__not_in' => 25
                            );
                            $the_query = new WP_Query( $args );
                            $url= wp_get_attachment_image_src( get_post_thumbnail_id(),1000 );
                            $image_thumb=get_the_post_thumbnail( $post_id, array(410,432) );
                            $url = get_the_post_thumbnail_url( $post_id, 'blog-three-block' );
                            ?>
                            <?php if ( $the_query->have_posts() ) { ?>
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post();
                                    $read_url= wp_get_attachment_image_src( get_post_thumbnail_id(),'blog-three-block' );
                                    ?>

                                    <li>
                                        <a href="<?php the_permalink(); ?>">  <div class="img-wrap"> <img src="<?php echo $read_url[0] ?>" alt="<?php echo get_the_title()?>"/> <div class="image-placeholder"></div></div></a>
                                        <h4 class="small-title"><?php echo get_the_category_list();?></h4>
                                        <a href="<?php the_permalink(); ?>"><h2 class="main-title"><span class="hover-line"><?php echo  get_the_title() ;?></span></h2></a>
                                        <p><?php echo wp_trim_words( get_the_excerpt(),25, '...' );?></p>
                                        <div class="author-data">
                                            <span class="author-name"><?php the_author(); ?></span>
                                            <span class="date"><?php echo get_the_date();?> | <?php echo do_shortcode('[rt_reading_time]');?> Min Read</span>
                                        </div>
                                    </li>
                                <?php endwhile; ?>

                                <?php wp_reset_postdata(); ?>
                            <?php } ?>

                        </ul>
                        <div class="newsletter-blk">
                            <div class="left-blk">
                                <h2 class="main-title">Sign up to newsletter</h2>
                                <p>News insights you won’t delete. Delivered to your inbox weekly.</p>
                                <button type="button" class="btn btn-primary subscribe-btn" data-toggle="modal" data-target="#exampleModal">
                                    Subscribe
                                </button>
                            </div>
                        </div>
                    </div>



                    <div class="right-blk right-block-jtt hidden-blk" id="tab-2">
                        <div class="inn-blk">
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'post_status' => 'publish',
                                'posts_per_page'=>'1',
                                'category__and' => array(4),
                            );
                            $the_query = new WP_Query( $args );
                            $url= wp_get_attachment_image_src( get_post_thumbnail_id(),1000 );
                            $image_thumb=get_the_post_thumbnail( $post_id, array(410,432) );
                            $url = get_the_post_thumbnail_url( $post_id, 'medium' );
                            ?>
                            <?php if ( $the_query->have_posts() ) { ?>
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post();
                                    $read_url= wp_get_attachment_image_src( get_post_thumbnail_id(),'1000' );
                                    ?>
                                    <div class="left-inn">
                                        <!--                                <img alt="logo" src="--><?php //bloginfo('template_directory'); ?><!--/assets/images/blog/blog-img.png">-->
                                        <a href="<?php the_permalink(); ?>">  <img src="<?php echo $read_url[0] ?>" alt="<?php echo get_the_title()?>"/></a>

                                    </div>
                                    <div class="right-inn">
                                        <h4 class="small-title"><?php echo get_the_category_list();?></h4>
                                        <a href="<?php the_permalink(); ?>"> <h2 class="main-title"><?php echo  get_the_title() ;?></h2></a>
                                        <p><?php echo wp_trim_words( get_the_content(), 10, '...' );?></p>
                                        <div class="author-data">
                                            <span class="author-name"><?php the_author(); ?></span>
                                            <span class="date"><?php echo get_the_date();?> | <?php echo do_shortcode('[rt_reading_time]');?> Min Read</span>
                                        </div>
                                    </div>
                                <?php endwhile; ?>

                                <?php wp_reset_postdata(); ?>
                            <?php } ?>
                        </div>

                        <ul class="second-row">
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'post_status' => 'publish',
                                'posts_per_page'=>'3',
                                'offset'=>'1',
                                'category__and' => array(4),
                            );
                            $the_query = new WP_Query( $args );
                            $url= wp_get_attachment_image_src( get_post_thumbnail_id(),1000 );
                            $image_thumb=get_the_post_thumbnail( $post_id, array(410,432) );
                            $url = get_the_post_thumbnail_url( $post_id, 'medium' );
                            ?>
                            <?php if ( $the_query->have_posts() ) { ?>
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post();
                                    $read_url= wp_get_attachment_image_src( get_post_thumbnail_id(),'1000' );
                                    ?>
                                    <li>
                                        <!--                                <img alt="logo" src="--><?php //bloginfo('template_directory'); ?><!--/assets/images/blog/image-01.png">-->
                                        <a href="<?php the_permalink(); ?>">   <img src="<?php echo $read_url[0] ?>" alt="<?php echo get_the_title()?>"/></a>

                                        <h4 class="small-title"><?php echo get_the_category_list();?></h4>
                                        <a href="<?php the_permalink(); ?>"><h2 class="main-title"><?php echo  get_the_title() ;?></h2></a>
                                        <p><?php echo wp_trim_words( get_the_content(), 9, '...' );?></p>
                                        <div class="author-data">
                                            <span class="author-name"><?php the_author(); ?></span>
                                            <span class="date"><?php echo get_the_date();?> | <?php echo do_shortcode('[rt_reading_time]');?> Min Read</span>
                                        </div>
                                    </li>
                                <?php endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                            <?php } ?>
                        </ul>


                        <ul class="second-row">
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'post_status' => 'publish',
                                'posts_per_page'=>'3',
                                'offset'=>'4',
                                'category__and' => array(4),
                            );
                            $the_query = new WP_Query( $args );
                            $url= wp_get_attachment_image_src( get_post_thumbnail_id(),1000 );
                            $image_thumb=get_the_post_thumbnail( $post_id, array(410,432) );
                            $url = get_the_post_thumbnail_url( $post_id, 'medium' );
                            ?>
                            <?php if ( $the_query->have_posts() ) { ?>
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post();
                                    $read_url= wp_get_attachment_image_src( get_post_thumbnail_id(),'1000' );
                                    ?>

                                    <li>
                                        <a href="<?php the_permalink(); ?>">   <img src="<?php echo $read_url[0] ?>" alt="<?php echo get_the_title()?>"/></a>
                                        <h4 class="small-title"><?php echo get_the_category_list();?></h4>
                                        <a href="<?php the_permalink(); ?>"><h2 class="main-title"><?php echo  get_the_title() ;?></h2></a>
                                        <p><?php echo wp_trim_words( get_the_content(), 9, '...' );?></p>
                                        <div class="author-data">
                                            <span class="author-name"><?php the_author(); ?></span>
                                            <span class="date"><?php echo get_the_date();?> | <?php echo do_shortcode('[rt_reading_time]');?> Min Read</span>
                                        </div>
                                    </li>
                                <?php endwhile; ?>

                                <?php wp_reset_postdata(); ?>
                            <?php } ?>

                        </ul>
                        <div class="newsletter-blk">
                            <div class="left-blk">
                                <h2 class="main-title">Sign up to newsletter</h2>
                                <p>News insights you won’t delete. Delivered to your inbox weekly.</p>
                            </div>
                            <div class="right-blk">

                            </div>
                        </div>
                    </div>

                    <div class="right-blk right-block-jtt hidden-blk" id="tab-3">
                        <div class="inn-blk">
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'post_status' => 'publish',
                                'posts_per_page'=>'1',
                                'category__and' => array(5),
                            );
                            $the_query = new WP_Query( $args );
                            $url= wp_get_attachment_image_src( get_post_thumbnail_id(),1000 );
                            $image_thumb=get_the_post_thumbnail( $post_id, array(410,432) );
                            $url = get_the_post_thumbnail_url( $post_id, 'medium' );
                            ?>
                            <?php if ( $the_query->have_posts() ) { ?>
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post();
                                    $read_url= wp_get_attachment_image_src( get_post_thumbnail_id(),'1000' );
                                    ?>
                                    <div class="left-inn">
                                        <!--                                <img alt="logo" src="--><?php //bloginfo('template_directory'); ?><!--/assets/images/blog/blog-img.png">-->
                                        <a href="<?php the_permalink(); ?>">  <img src="<?php echo $read_url[0] ?>" alt="<?php echo get_the_title()?>"/></a>

                                    </div>
                                    <div class="right-inn">
                                        <h4 class="small-title"><?php echo get_the_category_list();?></h4>
                                        <a href="<?php the_permalink(); ?>"><h2 class="main-title"><?php echo  get_the_title() ;?></h2></a>
                                        <p><?php echo wp_trim_words( get_the_content(), 10, '...' );?></p>
                                        <div class="author-data">
                                            <span class="author-name"><?php the_author(); ?></span>
                                            <span class="date"><?php echo get_the_date();?> | <?php echo do_shortcode('[rt_reading_time]');?> Min Read</span>
                                        </div>
                                    </div>
                                <?php endwhile; ?>

                                <?php wp_reset_postdata(); ?>
                            <?php } ?>
                        </div>

                        <ul class="second-row">
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'post_status' => 'publish',
                                'posts_per_page'=>'3',
                                'offset'=>'1',
                                'category__and' => array(5),
                            );
                            $the_query = new WP_Query( $args );
                            $url= wp_get_attachment_image_src( get_post_thumbnail_id(),1000 );
                            $image_thumb=get_the_post_thumbnail( $post_id, array(410,432) );
                            $url = get_the_post_thumbnail_url( $post_id, 'medium' );
                            ?>
                            <?php if ( $the_query->have_posts() ) { ?>
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post();
                                    $read_url= wp_get_attachment_image_src( get_post_thumbnail_id(),'1000' );
                                    ?>
                                    <li>
                                        <!--                                <img alt="logo" src="--><?php //bloginfo('template_directory'); ?><!--/assets/images/blog/image-01.png">-->
                                        <a href="<?php the_permalink(); ?>">   <img src="<?php echo $read_url[0] ?>" alt="<?php echo get_the_title()?>"/></a>

                                        <h4 class="small-title"><?php echo get_the_category_list();?></h4>
                                        <a href="<?php the_permalink(); ?>"><h2 class="main-title"><?php echo  get_the_title() ;?></h2></a>
                                        <p><?php echo wp_trim_words( get_the_content(), 9, '...' );?></p>
                                        <div class="author-data">
                                            <span class="author-name"><?php the_author(); ?></span>
                                            <span class="date"><?php echo get_the_date();?> | <?php echo do_shortcode('[rt_reading_time]');?> Min Read</span>
                                        </div>
                                    </li>
                                <?php endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                            <?php } ?>
                        </ul>


                        <ul class="second-row">
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'post_status' => 'publish',
                                'posts_per_page'=>'3',
                                'offset'=>'4',
                                'category__and' => array(5),
                            );
                            $the_query = new WP_Query( $args );
                            $url= wp_get_attachment_image_src( get_post_thumbnail_id(),1000 );
                            $image_thumb=get_the_post_thumbnail( $post_id, array(410,432) );
                            $url = get_the_post_thumbnail_url( $post_id, 'medium' );
                            ?>
                            <?php if ( $the_query->have_posts() ) { ?>
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post();
                                    $read_url= wp_get_attachment_image_src( get_post_thumbnail_id(),'1000' );
                                    ?>

                                    <li>
                                        <a href="<?php the_permalink(); ?>">   <img src="<?php echo $read_url[0] ?>" alt="<?php echo get_the_title()?>"/></a>
                                        <h4 class="small-title"><?php echo get_the_category_list();?></h4>
                                        <a href="<?php the_permalink(); ?>"><h2 class="main-title"><?php echo  get_the_title() ;?></h2></a>
                                        <p><?php echo wp_trim_words( get_the_content(), 9, '...' );?></p>
                                        <div class="author-data">
                                            <span class="author-name"><?php the_author(); ?></span>
                                            <span class="date"><?php echo get_the_date();?> | <?php echo do_shortcode('[rt_reading_time]');?> Min Read</span>
                                        </div>
                                    </li>
                                <?php endwhile; ?>

                                <?php wp_reset_postdata(); ?>
                            <?php } ?>

                        </ul>
                        <div class="newsletter-blk">
                            <div class="left-blk">
                                <h2 class="main-title">Sign up to newsletter</h2>
                                <p>News insights you won’t delete. Delivered to your inbox weekly.</p>
                            </div>
                            <div class="right-blk">

                            </div>
                        </div>
                    </div>

                    <div class="right-blk right-block-jtt hidden-blk" id="tab-4">
                        <div class="inn-blk">
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'post_status' => 'publish',
                                'posts_per_page'=>'1',
                                'category__and' => array(3),
                            );
                            $the_query = new WP_Query( $args );
                            $url= wp_get_attachment_image_src( get_post_thumbnail_id(),1000 );
                            $image_thumb=get_the_post_thumbnail( $post_id, array(410,432) );
                            $url = get_the_post_thumbnail_url( $post_id, 'medium' );
                            ?>
                            <?php if ( $the_query->have_posts() ) { ?>
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post();
                                    $read_url= wp_get_attachment_image_src( get_post_thumbnail_id(),'1000' );
                                    ?>
                                    <div class="left-inn">
                                        <!--                                <img alt="logo" src="--><?php //bloginfo('template_directory'); ?><!--/assets/images/blog/blog-img.png">-->
                                        <a href="<?php the_permalink(); ?>">  <img src="<?php echo $read_url[0] ?>" alt="<?php echo get_the_title()?>"/></a>

                                    </div>
                                    <div class="right-inn">
                                        <h4 class="small-title"><?php echo get_the_category_list();?></h4>
                                        <a href="<?php the_permalink(); ?>"><h2 class="main-title"><?php echo  get_the_title() ;?></h2></a>
                                        <p><?php echo wp_trim_words( get_the_content(), 10, '...' );?></p>
                                        <div class="author-data">
                                            <span class="author-name"><?php the_author(); ?></span>
                                            <span class="date"><?php echo get_the_date();?> | <?php echo do_shortcode('[rt_reading_time]');?> Min Read</span>
                                        </div>
                                    </div>
                                <?php endwhile; ?>

                                <?php wp_reset_postdata(); ?>
                            <?php } ?>
                        </div>

                        <ul class="second-row">
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'post_status' => 'publish',
                                'posts_per_page'=>'3',
                                'offset'=>'1',
                                'category__and' => array(3),
                            );
                            $the_query = new WP_Query( $args );
                            $url= wp_get_attachment_image_src( get_post_thumbnail_id(),1000 );
                            $image_thumb=get_the_post_thumbnail( $post_id, array(410,432) );
                            $url = get_the_post_thumbnail_url( $post_id, 'medium' );
                            ?>
                            <?php if ( $the_query->have_posts() ) { ?>
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post();
                                    $read_url= wp_get_attachment_image_src( get_post_thumbnail_id(),'1000' );
                                    ?>
                                    <li>
                                        <!--                                <img alt="logo" src="--><?php //bloginfo('template_directory'); ?><!--/assets/images/blog/image-01.png">-->
                                        <a href="<?php the_permalink(); ?>">   <img src="<?php echo $read_url[0] ?>" alt="<?php echo get_the_title()?>"/></a>

                                        <h4 class="small-title"><?php echo get_the_category_list();?></h4>
                                        <a href="<?php the_permalink(); ?>"><h2 class="main-title"><?php echo  get_the_title() ;?></h2></a>
                                        <p><?php echo wp_trim_words( get_the_content(), 9, '...' );?></p>
                                        <div class="author-data">
                                            <span class="author-name"><?php the_author(); ?></span>
                                            <span class="date"><?php echo get_the_date();?> | <?php echo do_shortcode('[rt_reading_time]');?> Min Read</span>
                                        </div>
                                    </li>
                                <?php endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                            <?php } ?>
                        </ul>


                        <ul class="second-row">
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'post_status' => 'publish',
                                'posts_per_page'=>'3',
                                'offset'=>'4',
                                'category__and' => array(3),
                            );
                            $the_query = new WP_Query( $args );
                            $url= wp_get_attachment_image_src( get_post_thumbnail_id(),1000 );
                            $image_thumb=get_the_post_thumbnail( $post_id, array(410,432) );
                            $url = get_the_post_thumbnail_url( $post_id, 'medium' );
                            ?>
                            <?php if ( $the_query->have_posts() ) { ?>
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post();
                                    $read_url= wp_get_attachment_image_src( get_post_thumbnail_id(),'1000' );
                                    ?>

                                    <li>
                                        <a href="<?php the_permalink(); ?>">   <img src="<?php echo $read_url[0] ?>" alt="<?php echo get_the_title()?>"/></a>
                                        <h4 class="small-title"><?php echo get_the_category_list();?></h4>
                                        <a href="<?php the_permalink(); ?>"><h2 class="main-title"><?php echo  get_the_title() ;?></h2></a>
                                        <p><?php echo wp_trim_words( get_the_content(), 9, '...' );?></p>
                                        <div class="author-data">
                                            <span class="author-name"><?php the_author(); ?></span>
                                            <span class="date"><?php echo get_the_date();?> | <?php echo do_shortcode('[rt_reading_time]');?> Min Read</span>
                                        </div>
                                    </li>
                                <?php endwhile; ?>

                                <?php wp_reset_postdata(); ?>
                            <?php } ?>

                        </ul>
                        <div class="newsletter-blk">
                            <div class="left-blk">
                                <h2 class="main-title">Sign up to newsletter</h2>
                                <p>News insights you won’t delete. Delivered to your inbox weekly.</p>
                            </div>
                            <div class="right-blk">

                            </div>
                        </div>
                    </div>

                    <div class="right-blk right-block-jtt hidden-blk" id="tab-5">
                        <div class="inn-blk">
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'post_status' => 'publish',
                                'posts_per_page'=>'1',
                                'category__and' => array(10),
                            );
                            $the_query = new WP_Query( $args );
                            $url= wp_get_attachment_image_src( get_post_thumbnail_id(),1000 );
                            $image_thumb=get_the_post_thumbnail( $post_id, array(410,432) );
                            $url = get_the_post_thumbnail_url( $post_id, 'medium' );
                            ?>
                            <?php if ( $the_query->have_posts() ) { ?>
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post();
                                    $read_url= wp_get_attachment_image_src( get_post_thumbnail_id(),'1000' );
                                    ?>
                                    <div class="left-inn">
                                        <!--                                <img alt="logo" src="--><?php //bloginfo('template_directory'); ?><!--/assets/images/blog/blog-img.png">-->
                                        <a href="<?php the_permalink(); ?>">  <img src="<?php echo $read_url[0] ?>" alt="<?php echo get_the_title()?>"/></a>

                                    </div>
                                    <div class="right-inn">
                                        <h4 class="small-title"><?php echo get_the_category_list();?></h4>
                                        <a href="<?php the_permalink(); ?>"><h2 class="main-title"><?php echo  get_the_title() ;?></h2></a>
                                        <p><?php echo wp_trim_words( get_the_content(), 10, '...' );?></p>
                                        <div class="author-data">
                                            <span class="author-name"><?php the_author(); ?></span>
                                            <span class="date"><?php echo get_the_date();?> | <?php echo do_shortcode('[rt_reading_time]');?> Min Read</span>
                                        </div>
                                    </div>
                                <?php endwhile; ?>

                                <?php wp_reset_postdata(); ?>
                            <?php } ?>
                        </div>

                        <ul class="second-row">
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'post_status' => 'publish',
                                'posts_per_page'=>'3',
                                'offset'=>'1',
                                'category__and' => array(10),
                            );
                            $the_query = new WP_Query( $args );
                            $url= wp_get_attachment_image_src( get_post_thumbnail_id(),1000 );
                            $image_thumb=get_the_post_thumbnail( $post_id, array(410,432) );
                            $url = get_the_post_thumbnail_url( $post_id, 'medium' );
                            ?>
                            <?php if ( $the_query->have_posts() ) { ?>
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post();
                                    $read_url= wp_get_attachment_image_src( get_post_thumbnail_id(),'1000' );
                                    ?>
                                    <li>
                                        <!--                                <img alt="logo" src="--><?php //bloginfo('template_directory'); ?><!--/assets/images/blog/image-01.png">-->
                                        <a href="<?php the_permalink(); ?>">   <img src="<?php echo $read_url[0] ?>" alt="<?php echo get_the_title()?>"/></a>

                                        <h4 class="small-title"><?php echo get_the_category_list();?></h4>
                                        <a href="<?php the_permalink(); ?>"><h2 class="main-title"><?php echo  get_the_title() ;?></h2></a>
                                        <p><?php echo wp_trim_words( get_the_content(), 9, '...' );?></p>
                                        <div class="author-data">
                                            <span class="author-name"><?php the_author(); ?></span>
                                            <span class="date"><?php echo get_the_date();?> | <?php echo do_shortcode('[rt_reading_time]');?> Min Read</span>
                                        </div>
                                    </li>
                                <?php endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                            <?php } ?>
                        </ul>


                        <ul class="second-row">
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'post_status' => 'publish',
                                'posts_per_page'=>'3',
                                'offset'=>'4',
                                'category__and' => array(10),
                            );
                            $the_query = new WP_Query( $args );
                            $url= wp_get_attachment_image_src( get_post_thumbnail_id(),1000 );
                            $image_thumb=get_the_post_thumbnail( $post_id, array(410,432) );
                            $url = get_the_post_thumbnail_url( $post_id, 'medium' );
                            ?>
                            <?php if ( $the_query->have_posts() ) { ?>
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post();
                                    $read_url= wp_get_attachment_image_src( get_post_thumbnail_id(),'1000' );
                                    ?>

                                    <li>
                                        <a href="<?php the_permalink(); ?>">   <img src="<?php echo $read_url[0] ?>" alt="<?php echo get_the_title()?>"/></a>
                                        <h4 class="small-title"><?php echo get_the_category_list();?></h4>
                                        <a href="<?php the_permalink(); ?>"><h2 class="main-title"><?php echo  get_the_title() ;?></h2></a>
                                        <p><?php echo wp_trim_words( get_the_content(), 9, '...' );?></p>
                                        <div class="author-data">
                                            <span class="author-name"><?php the_author(); ?></span>
                                            <span class="date"><?php echo get_the_date();?> | <?php echo do_shortcode('[rt_reading_time]');?> Min Read</span>
                                        </div>
                                    </li>
                                <?php endwhile; ?>

                                <?php wp_reset_postdata(); ?>
                            <?php } ?>

                        </ul>
                        <div class="newsletter-blk">
                            <div class="left-blk">
                                <h2 class="main-title">Sign up to newsletter</h2>
                                <p>News insights you won’t delete. Delivered to your inbox weekly.</p>
                            </div>
                            <div class="right-blk">

                            </div>
                        </div>
                    </div>
                    <div class="right-blk right-block-jtt hidden-blk" id="tab-6">
                        <div class="inn-blk">
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'post_status' => 'publish',
                                'posts_per_page'=>'1',
                                'category__and' => array(12),
                            );
                            $the_query = new WP_Query( $args );
                            $url= wp_get_attachment_image_src( get_post_thumbnail_id(),1000 );
                            $image_thumb=get_the_post_thumbnail( $post_id, array(410,432) );
                            $url = get_the_post_thumbnail_url( $post_id, 'medium' );
                            ?>
                            <?php if ( $the_query->have_posts() ) { ?>
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post();
                                    $read_url= wp_get_attachment_image_src( get_post_thumbnail_id(),'1000' );
                                    ?>
                                    <div class="left-inn">
                                        <!--                                <img alt="logo" src="--><?php //bloginfo('template_directory'); ?><!--/assets/images/blog/blog-img.png">-->
                                        <a href="<?php the_permalink(); ?>">  <img src="<?php echo $read_url[0] ?>" alt="<?php echo get_the_title()?>"/></a>

                                    </div>
                                    <div class="right-inn">
                                        <h4 class="small-title"><?php echo get_the_category_list();?></h4>
                                        <a href="<?php the_permalink(); ?>"><h2 class="main-title"><?php echo  get_the_title() ;?></h2></a>
                                        <p><?php echo wp_trim_words( get_the_content(), 10, '...' );?></p>
                                        <div class="author-data">
                                            <span class="author-name"><?php the_author(); ?></span>
                                            <span class="date"><?php echo get_the_date();?> | <?php echo do_shortcode('[rt_reading_time]');?> Min Read</span>
                                        </div>
                                    </div>
                                <?php endwhile; ?>

                                <?php wp_reset_postdata(); ?>
                            <?php } ?>
                        </div>

                        <ul class="second-row">
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'post_status' => 'publish',
                                'posts_per_page'=>'3',
                                'offset'=>'1',
                                'category__and' => array(12),
                            );
                            $the_query = new WP_Query( $args );
                            $url= wp_get_attachment_image_src( get_post_thumbnail_id(),1000 );
                            $image_thumb=get_the_post_thumbnail( $post_id, array(410,432) );
                            $url = get_the_post_thumbnail_url( $post_id, 'medium' );
                            ?>
                            <?php if ( $the_query->have_posts() ) { ?>
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post();
                                    $read_url= wp_get_attachment_image_src( get_post_thumbnail_id(),'1000' );
                                    ?>
                                    <li>
                                        <!--                                <img alt="logo" src="--><?php //bloginfo('template_directory'); ?><!--/assets/images/blog/image-01.png">-->
                                        <a href="<?php the_permalink(); ?>">   <img src="<?php echo $read_url[0] ?>" alt="<?php echo get_the_title()?>"/></a>

                                        <h4 class="small-title"><?php echo get_the_category_list();?></h4>
                                        <a href="<?php the_permalink(); ?>"><h2 class="main-title"><?php echo  get_the_title() ;?></h2></a>
                                        <p><?php echo wp_trim_words( get_the_content(), 9, '...' );?></p>
                                        <div class="author-data">
                                            <span class="author-name"><?php the_author(); ?></span>
                                            <span class="date"><?php echo get_the_date();?> | <?php echo do_shortcode('[rt_reading_time]');?> Min Read</span>
                                        </div>
                                    </li>
                                <?php endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                            <?php } ?>
                        </ul>


                        <ul class="second-row">
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'post_status' => 'publish',
                                'posts_per_page'=>'3',
                                'offset'=>'4',
                                'category__and' => array(12),
                            );
                            $the_query = new WP_Query( $args );
                            $url= wp_get_attachment_image_src( get_post_thumbnail_id(),1000 );
                            $image_thumb=get_the_post_thumbnail( $post_id, array(410,432) );
                            $url = get_the_post_thumbnail_url( $post_id, 'medium' );
                            ?>
                            <?php if ( $the_query->have_posts() ) { ?>
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post();
                                    $read_url= wp_get_attachment_image_src( get_post_thumbnail_id(),'1000' );
                                    ?>

                                    <li>
                                        <a href="<?php the_permalink(); ?>">   <img src="<?php echo $read_url[0] ?>" alt="<?php echo get_the_title()?>"/></a>
                                        <h4 class="small-title"><?php echo get_the_category_list();?></h4>
                                        <a href="<?php the_permalink(); ?>"><h2 class="main-title"><?php echo  get_the_title() ;?></h2></a>
                                        <p><?php echo wp_trim_words( get_the_content(), 9, '...' );?></p>
                                        <div class="author-data">
                                            <span class="author-name"><?php the_author(); ?></span>
                                            <span class="date"><?php echo get_the_date();?> | <?php echo do_shortcode('[rt_reading_time]');?> Min Read</span>
                                        </div>
                                    </li>
                                <?php endwhile; ?>

                                <?php wp_reset_postdata(); ?>
                            <?php } ?>

                        </ul>
                        <div class="newsletter-blk">
                            <div class="left-blk">
                                <h2 class="main-title">Sign up to newsletter</h2>
                                <p>News insights you won’t delete. Delivered to your inbox weekly.</p>
                            </div>
                            <div class="right-blk">

                            </div>
                        </div>
                    </div>
                    <div class="right-blk right-block-jtt hidden-blk" id="tab-7">
                        <div class="inn-blk">
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'post_status' => 'publish',
                                'posts_per_page'=>'1',
                            );
                            $the_query = new WP_Query( $args );
                            $url= wp_get_attachment_image_src( get_post_thumbnail_id(),1000 );
                            $image_thumb=get_the_post_thumbnail( $post_id, array(410,432) );
                            $url = get_the_post_thumbnail_url( $post_id, 'medium' );
                            ?>
                            <?php if ( $the_query->have_posts() ) { ?>
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post();
                                    $read_url= wp_get_attachment_image_src( get_post_thumbnail_id(),'1000' );
                                    ?>
                                    <div class="left-inn">
                                        <!--                                <img alt="logo" src="--><?php //bloginfo('template_directory'); ?><!--/assets/images/blog/blog-img.png">-->
                                        <a href="<?php the_permalink(); ?>">  <img src="<?php echo $read_url[0] ?>" alt="<?php echo get_the_title()?>"/></a>

                                    </div>
                                    <div class="right-inn">
                                        <h4 class="small-title"><?php echo get_the_category_list();?></h4>
                                        <a href="<?php the_permalink(); ?>"><h2 class="main-title"><?php echo  get_the_title() ;?></h2></a>
                                        <p><?php echo wp_trim_words( get_the_content(), 10, '...' );?></p>
                                        <div class="author-data">
                                            <span class="author-name"><?php the_author(); ?></span>
                                            <span class="date"><?php echo get_the_date();?> | <?php echo do_shortcode('[rt_reading_time]');?> Min Read</span>
                                        </div>
                                    </div>
                                <?php endwhile; ?>

                                <?php wp_reset_postdata(); ?>
                            <?php } ?>
                        </div>





                        <ul class="second-row view-all-posts">

                            <?php
                            $limit = 2;
                            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                            $offset = $paged!=1 ? ( $limit * $paged ) - $limit : 1;
                            $args = array(
                                'post_type' => 'post',
                                'post_status' => 'publish',
                                'offset' => $offset,
                            );
                            $the_query = new WP_Query( $args );
                            $url= wp_get_attachment_image_src( get_post_thumbnail_id(),1000 );
                            $image_thumb=get_the_post_thumbnail( $post_id, array(410,432) );
                            $url = get_the_post_thumbnail_url( $post_id, 'medium' );
                            ?>
                            <?php if ( $the_query->have_posts() ) { ?>
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post();
                                    $read_url= wp_get_attachment_image_src( get_post_thumbnail_id(),'1000' );
                                    ?>

                                    <li>
                                        <!--                                <img alt="logo" src="--><?php //bloginfo('template_directory'); ?><!--/assets/images/blog/image-01.png">-->
                                        <a href="<?php the_permalink(); ?>">   <img src="<?php echo $read_url[0] ?>" alt="<?php echo get_the_title()?>"/></a>

                                        <h4 class="small-title"><?php echo get_the_category_list();?></h4>
                                        <a href="<?php the_permalink(); ?>"><h2 class="main-title"><?php echo  get_the_title() ;?></h2></a>
                                        <p><?php echo wp_trim_words( get_the_content(), 9, '...' );?></p>
                                        <div class="author-data">
                                            <span class="author-name"><?php the_author(); ?></span>
                                            <span class="date"><?php echo get_the_date();?> | <?php echo do_shortcode('[rt_reading_time]');?> Min Read</span>
                                        </div>
                                    </li>
                                <?php endwhile; ?>
                                <?php    the_posts_pagination( array(
                                    'prev_text' => '<span class="screen-reader-text">' . __( 'Previous page', 'twentyseventeen' ) . '</span>',
                                    'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'twentyseventeen' ) . '</span>',
                                    'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( '', 'twentyseventeen' ) . ' </span>',
                                ) );?>
                                <?php wp_reset_postdata(); ?>
                            <?php } ?>

                        </ul>



                        <div class="newsletter-blk">
                            <div class="left-blk">
                                <h2 class="main-title">Sign up to newsletter</h2>
                                <p>News insights you won’t delete. Delivered to your inbox weekly.</p>
                            </div>
                            <div class="right-blk">

                            </div>
                        </div>
                    </div>
                </div>

            </div>


            <div class="blog-details-page">
                <div class="main-blk">
                    <div class="right-blk">
                        <div class="subscribe-blk" id="subscribe-blk">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/blog/newsletter-icon.svg" alt="newsletter-icon"/>
                            <span id="close"><img src="<?php bloginfo('template_directory'); ?>/assets/images/blog/input-arrow.svg" alt="input-arrow"/></span>
                            <h5>Sign up to our newsletter</h5>
                            <div class="email-blk">
                                <button type="button" class="btn btn-primary subscribe-btn white" data-toggle="modal" data-target="#exampleModal">
                                    Subscribe
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>







        </section>
    </div>



    <!-- Modal -->
    <div class="modal custom-popup" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <!--                        <span aria-hidden="true">&times;</span>-->
                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/blog/input-arrow.svg" alt="input-arrow"/>
                </button>
                <div class="modal-body" id="subscribe-popup">
                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/blog/subscribe-popup-img.svg" alt="popup-img">
                    <!-- Begin Mailchimp Signup Form -->
                    <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
                    <style type="text/css">
                        #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
                        /* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.
                        We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
                    </style>
                    <div id="mc_embed_signup">
                        <form action="https://justtotaltech.us18.list-manage.com/subscribe/post-json?u=24195892ef9a3e2c3b04b68ae&amp;id=a100e07e01&c=?" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                            <div id="mc_embed_signup_scroll">
                                <h2>Subscribe for daily updates</h2>
                                <!--                                <div class="indicates-required"><span class="asterisk">*</span> indicates required</div>-->
                                <div class="mc-field-group">
                                    <!--                                    <label for="mce-EMAIL">Email Address <span class="asterisk">*</span></label>-->
                                    <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" autofocus placeholder="Email Address" >
                                </div>
                                <div class="mc-field-group">
                                    <!--                                    <label for="mce-FNAME">First Name <span class="asterisk">*</span></label>-->
                                    <input type="text" value="" name="FNAME" class="required" id="mce-FNAME" placeholder="First Name">
                                </div>
                                <div class="mc-field-group">
                                    <!--                                    <label for="mce-LNAME">Last Name <span class="asterisk">*</span></label>-->
                                    <input type="text" value="" name="LNAME" class="required" id="mce-LNAME" placeholder="Last Name">
                                </div>
                                <div id="mce-responses" class="clear">
                                    <div class="response" id="mce-error-response" style="display:none"></div>
                                    <div class="response" id="mce-success-response" style="display:none"></div>
                                </div> <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_24195892ef9a3e2c3b04b68ae_a100e07e01" tabindex="-1" value=""></div>
                                <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button submit"></div>
                            </div>
                        </form>
                        <div id="subscribe-result"></div>
                    </div>

                    <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[3]='ADDRESS';ftypes[3]='address';fnames[4]='PHONE';ftypes[4]='phone';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
                    <!--End mc_embed_signup-->
                </div>

            </div>
        </div>
        <div class="thanks-popup">
            <div class="modal-body hidden" id="my-popup">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <!--                        <span aria-hidden="true">&times;</span>-->
                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/blog/closecon.svg" alt="input-arrow"/>
                </button>

                <img src="<?php bloginfo('template_directory'); ?>/assets/images/blog/thanks-blog.png" alt="popup-img">
                <h3>You’re almost done</h3>
                <p>Please click on the link sent onto your registered email address to confirem your account and complete the subscription process</p>
            </div>
        </div>
    </div>
    <div class="random-box">
        <div class="lightbox lightbox_1" >
            <div class="box">
                <span id="close"><img src="<?php bloginfo('template_directory'); ?>/assets/images/blog/input-arrow.svg" alt="input-arrow"/></span>
                <div class="content">
                    <h3>Thank you for visiting our site</h3>
                    <!--                <p class="bottom-info">Before you leave, checkout this trending AI open source tools write-up. Have an insightful reading session!</p>-->
                    <div class="d-flex space-between top-box">
                        <div class="img-wrap">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/blog/open-source.png" alt="open-source"/>
                        </div>
                        <div class="content-side">
                            <h4>Top 10 Open Source Artificial Intelligence Software You Should Be Aware Of</h4>
                            <!--                        <p>Apple’s Siri. Google’s OK Google. Amazon’s Echo services.</p>-->
                            <div class="read-more">
                                <a href="https://justtotaltech.com/open-source-artificial-intelligence-software/">Read full Article</a>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex space-between bottom-box align-center">
                        <div class="info">
                            <p>Don’t miss the new articles!</p>
                        </div>
                        <div class="footer-subscribe-btn">

                            <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
                            <style type="text/css">
                                #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
                                /* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.
                                   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
                            </style>
                            <div id="mc_embed_signup">
                                <form action="https://thenextscoop.us3.list-manage.com/subscribe/post-json?u=2ea26dad4a6c54fec6e6ff666&amp;id=703166db06&c=?" method="post" id="mc-embedded-subscribe-form-two" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                    <div id="mc_embed_signup_scroll">

                                        <div class="mc-field-group">
                                            <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address" >
                                        </div>

                                        <div id="mce-responses" class="clear">
                                            <div class="response" id="mce-error-response" style="display:none"></div>
                                            <div class="response" id="mce-success-response" style="display:none"></div>
                                        </div>
                                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_2ea26dad4a6c54fec6e6ff666_703166db06" tabindex="-1" value=""></div>
                                        <div class="clear arrow-img ">
                                            <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe"  class="subscribe-btn">
                                        </div>
                                    </div>
                                </form>
                                <div id="subscribe-result-two"></div>
                            </div>
                            <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($)
                                {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='ADDRESS';ftypes[3]='address';fnames[4]='PHONE';ftypes[4]='phone';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lightbox lightbox_2 dark" >
            <div class="box">
                <span id="close"><img src="<?php bloginfo('template_directory'); ?>/assets/images/blog/close-white.svg" alt="input-arrow"/></span>
                <div class="content">
                    <h3>Thank you for visiting our site</h3>
                    <!--                <p class="bottom-info">Before you leave, checkout this trending AI open source tools write-up. Have an insightful reading session!</p>-->
                    <div class="d-flex space-between top-box">
                        <div class="img-wrap">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/blog/open-source.png" alt="open-source"/>
                        </div>
                        <div class="content-side">
                            <h4>Top 10 Open Source Artificial Intelligence Software You Should Be Aware Of</h4>
                            <!--                        <p>Apple’s Siri. Google’s OK Google. Amazon’s Echo services.</p>-->
                            <div class="read-more">
                                <a href="https://justtotaltech.com/open-source-artificial-intelligence-software/">Read full Article</a>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex space-between bottom-box align-center">
                        <div class="info">
                            <p>Don’t miss the new articles!</p>
                        </div>
                        <div class="footer-subscribe-btn">
                            <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
                            <style type="text/css">
                                #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
                                /* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.
                                   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
                            </style>
                            <div id="mc_embed_signup">
                                <form action="https://thenextscoop.us3.list-manage.com/subscribe/post-json?u=2ea26dad4a6c54fec6e6ff666&amp;id=703166db06&c=?" method="post" id="mc-embedded-subscribe-form-three" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                    <div id="mc_embed_signup_scroll">

                                        <div class="mc-field-group">
                                            <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address" >
                                        </div>

                                        <div id="mce-responses" class="clear">
                                            <div class="response" id="mce-error-response" style="display:none"></div>
                                            <div class="response" id="mce-success-response" style="display:none"></div>
                                        </div>
                                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_2ea26dad4a6c54fec6e6ff666_703166db06" tabindex="-1" value=""></div>
                                        <div class="clear arrow-img ">
                                            <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe"  class="subscribe-btn">
                                        </div>
                                    </div>
                                </form>
                                <div id="subscribe-result-three"></div>
                            </div>
                            <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($)
                                {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='ADDRESS';ftypes[3]='address';fnames[4]='PHONE';ftypes[4]='phone';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script defer>
        $(".blog-page .main-blk .sidebar a.cl").click(function () {
            $(".blog-page .main-blk .sidebar a.cl").removeClass("active");
            $(this).addClass("active");
            $('.right-block-jtt').fadeOut(0);
            $($(this).attr('href')).show();
        });

        $("a.cl").click(function (event) {
            event.preventDefault();
            $("html, body").animate({scrollTop: $($(this).attr("href")).offset().top - 137}, 500);
        });


        jQuery(function($) {
            var path = window.location.href; // because the 'href' property of the DOM element is the absolute path
            var url_path = path.split('/')[4];
            if (url_path === "page") {
                $(".blog-page .main-blk .sidebar a.cl").removeClass("active");
                $('#view-all').addClass('active');
                $('.right-block-jtt').fadeOut(0);
                $($('#view-all').attr('href')).show();
            }
        });



        $(document).ready(function() {
          
            $('body').find('img').each(function() {
                    if($(this).attr('src') == "") {
                        $(this).attr('src','https://justtotaltech.com/wp-content/themes/jtt/assets/images/just-total-tech-post.jpg');
                    }
                }
            )
        });
    </script>



    <script defer>
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();

            if(scroll >= 5) {
                $(".subscribe-blk").addClass("change");
            } else {
                $(".subscribe-blk").removeClass("change");
            }
        });
    </script>

    <script defer>
        $(document).ready(function () {
            $("#close").click(function () {
                $("#subscribe-blk").hide();
            });
        });

        $(document).ready(function () {
            var $form = $('#mc-embedded-subscribe-form')
            if ($form.length > 0) {
                $('form input[type="submit"]').bind('click', function (event) {
                    if (event) event.preventDefault()
                    register($form)
                })
            }
        })

        function register($form) {
            $('#mc-embedded-subscribe').val('Sending...');
            $.ajax({
                type: $form.attr('method'),
                url: $form.attr('action'),
                data: $form.serialize(),
                cache: false,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                error: function (err) { alert('Could not connect to the registration server. Please try again later.') },
                success: function (data) {
                    $('#mc-embedded-subscribe').val('Subscribe')
                    if (data.result === 'success') {
                        // Yeahhhh Success
                        // $('#mce-EMAIL').css('borderColor', '#ffffff')
                        $('#my-popup').show()
                        $('#subscribe-popup').hide()
                        $('#subscribe-result').css('color', 'rgb(53, 114, 210)')
                        $('#subscribe-result').html('<p>Thank you for subscribing. We have sent you a confirmation email.</p>')
                        $('#mce-EMAIL').val('')
                        $('#mce-FNAME').val('')
                        $('#mce-LNAME').val('')
                        $('#mce-EMAIL').css('borderColor', '#d0d7eb')
                        $('#mce-FNAME').css('borderColor', '#d0d7eb')
                        $('#mce-LNAME').css('borderColor', '#d0d7eb')
                    } else {
                        // Something went wrong, do something to notify the user.
                        console.log(data.msg)
                        $('#my-popup').hide()
                        $('#subscribe-popup').show()
                        $('#mce-EMAIL').css('borderColor', 'red')
                        $('#mce-FNAME').css('borderColor', 'red')
                        $('#mce-LNAME').css('borderColor', 'red')
                        $('#subscribe-result').css('color', '#ff8282')
                        $('#subscribe-result').html('<p>' + data.msg.substring(4) + '</p>')
                    }
                }
            })
        };
    </script>

    <script defer>



        $(document).ready(function () {
            var $form_two = $('#mc-embedded-subscribe-form-two')
            if ($form_two.length > 0) {
                $('#mc-embedded-subscribe-form-two input[type="submit"]').bind('click', function (event) {
                    if (event) event.preventDefault()
                    register2($form_two)
                })
            }
        })
        function register2($form_two) {
            // $('#mc-embedded-subscribe').val('Sending...');
            $.ajax({
                type: $form_two.attr('method'),
                url: $form_two.attr('action'),
                data: $form_two.serialize(),
                cache: false,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                error: function (err) { alert('Could not connect to the registration server. Please try again later.') },
                success: function (data) {
                    $('#mc-embedded-subscribe').val('subscribe')
                    if (data.result === 'success') {
                        $('#subscribe-result-two').css('color', 'rgb(53, 114, 210)')
                        $('#subscribe-result-two').html('<p>Thank you for subscribing. We have sent you a confirmation email.</p>')
                        $('.long-subscribe-btn #mce-EMAIL').val('')
                        $('.lightbox .content .bottom-box .footer-subscribe-btn #mc_embed_signup .mc-field-group input#mce-EMAIL').css('borderColor', '#dedede')

                    } else {
                        // console.log(data.msg)
                        $('.lightbox .content .bottom-box .footer-subscribe-btn #mc_embed_signup .mc-field-group input#mce-EMAIL').css('borderColor', 'red')
                        $('.lightbox .content .bottom-box .footer-subscribe-btn #mc_embed_signup .mc-field-group input#mce-FNAME').css('borderColor', 'red')
                        $('.lightbox .content .bottom-box .footer-subscribe-btn #mc_embed_signup .mc-field-group input#mce-LNAME').css('borderColor', 'red')
                        $('#subscribe-result-two').css('color', '#ff8282')
                        $('#subscribe-result-two').html('<p>' + data.msg.substring(4) + '</p>')
                    }
                }
            })
        };


        // Exit intent
        function addEvent(obj, evt, fn) {
            if (obj.addEventListener) {
                obj.addEventListener(evt, fn, false);
            }
            else if (obj.attachEvent) {
                obj.attachEvent("on" + evt, fn);
            }
        }


        $(document).ready(function(){
            const popup = [ "lightbox_1" , "lightbox_2"];
            const random = Math.floor((Math.random()) * popup.length);
            // console.log(random,popup[random]);

            function setCookie(name,value,days) {
                var expires = "";
                if (days) {
                    var date = new Date();
                    date.setTime(date.getTime() + (days*24*60*60*1000));
                    expires = "; expires=" + date.toUTCString();
                }
                document.cookie = name + "=" + (value || "") + expires + "; path=/";
            }
            function getCookie(name) {
                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for(var i=0;i < ca.length;i++) {
                    var c = ca[i];
                    while (c.charAt(0)==' ') c = c.substring(1,c.length);
                    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
                }
                return null;
            }
            document.addEventListener("mouseleave", function(e){
                var x = getCookie('popup_close');
                if(!x && (e.clientY < 0 )) {
                    if(random == 1) {
                        $('.lightbox_1').fadeOut();
                        $('.lightbox_2').fadeIn();

                    }
                    setCookie('popup_close', '1', 1);
                    if (random == 0) {
                        $('.lightbox_2').fadeOut();
                        $('.lightbox_1').fadeIn();
                    }
                }

            }, false);
        });


        // Closing the Popup Box
        $('.lightbox_1 span#close').click(function(){
            $('.lightbox_1').fadeOut();
        });
        $('.lightbox_2 span#close').click(function(){
            $('.lightbox_2').fadeOut();
        });
        jQuery("#exampleModal").click(function () {
            jQuery("#my-popup").hide();
            jQuery("#subscribe-popup").show();
        });

    </script>


<!--    <script defer>-->
<!--        if($(window).width() < 767)-->
<!--        {-->
<!--            $('.img-wrap img').attr('width', 620).attr('height', 350);-->
<!--        }-->
<!--        else {-->
<!--            $('.img-wrap img').attr('width', '').attr('height', '');-->
<!--        }-->
<!--    </script>-->


<?php get_footer();
