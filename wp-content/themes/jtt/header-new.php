<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

<!--    <link rel="stylesheet" type="text/css" href="--><?//=get_template_directory_uri().'/assets/css/bootstrap.css'?><!--">-->
<!--    <link rel="stylesheet" type="text/css" href="--><?//=get_template_directory_uri().'/assets/css/fonts.css'?><!--">-->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Fira+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=PT+Serif:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

    <?php wp_head();?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-130959501-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-130959501-1');
    </script>


    <style>
        body{
            font-family: 'Fira Sans', sans-serif;
        }
        .custom-nav .visible-small{
            display: none;
        }
        .custom-nav .dropdown-toggle::after{
            vertical-align: middle;
        }

        .custom-nav .navbar {
            padding: 0; }
        .custom-nav .nav-link {
            font-size: 17px;
            color: #262626 !important;
            margin-right: 50px; }
        .custom-nav .form-control {
            border-radius: 4px;
            border: solid 1px #dedede;
            background-color: #ffffff;
            margin-right: 0 !important;
            height: 50px;
            padding: 0 40px 0 15px; }
        .custom-nav .form-control:focus {
            box-shadow: none; }
        .custom-nav ::placeholder {
            color: #001033 !important;
            opacity: 0.5 !important; }
        .custom-nav .form-inline {
            margin-right: 32px;
            position: relative;
            max-width: 307px; }
        .custom-nav .search-btn {
            background: none !important;
            position: absolute;
            top: 5px;
            right: 4px; }
        .custom-nav .search-btn:focus {
            box-shadow: none; }
        .custom-nav .subscribe-btn {
            font-size: 17px;
            color: #ffffff;
            padding: 12px 34px;
            border-radius: 4px;
            background-color: #0000ff;
            }
        .custom-nav .navbar-nav {
            margin-left: auto; }
        .custom-nav .nav-item:hover .dropdown-menu {
            display: block;
            box-shadow: 0 22px 54px 0 rgba(18, 51, 153, 0.22);
            -webkit-box-shadow: 0 22px 54px 0 rgba(18, 51, 153, 0.22);
            border: 1px solid transparent; }

        .dropdown-menu:before {
            content: url(../images/top-arrow.png);
            top: 6px;
            left: 50%;
            position: absolute;
            width: 100%;
            height: 100%; }

        #site-footer {
            background: #ffffff; }
        #site-footer p {
            font-size: 14px;
            color: #868686;
            margin: 50px 0 0 0; }
        #site-footer .footer-links {
            text-align: right;
            margin: 0; }
        #site-footer .footer-links li {
            display: inline-block; }
        #site-footer .footer-links li a {
            font-size: 17px;
            color: #121214;
            margin-left: 43px; }
        #site-footer .footer_social {
            border-top: none !important;
            margin: 50px 0 0 0; }
        #site-footer .footer_social a {
            margin-left: 10px; }
        #site-footer .footer_social a img {
            max-width: 20px;
        }
        .top-header-bar.header-fixed {
            box-shadow: none;
            padding: 20px 0; }

        @media (min-width: 1200px) {
            .container {
                max-width: 1240px; } }
        @media only screen and (max-width: 991px) {
            .custom-nav .form-inline {
                margin-right: 0;
                max-width: 100%; }
            .custom-nav .form-inline input{
                width: 100%;
                margin: 0;
            }

            #site-footer .footer-links {
                width: 100%;
                padding: 0; }
            #site-footer .footer-links li {
                width: 50%;
                float: left;
                text-align: left;
                margin: 15px 0 0; }
            #site-footer .footer-links li a {
                margin: 0; }
            #site-footer .footer_social {
                margin: 30px 0 0 0;
                float: left;
                text-align: center;
                width: 100%; }
            #site-footer p {
                margin: 20px 0 0 0; }
            .custom-nav .subscribe-btn{
                width: 100%;
                max-width: 100%;
                display: inline-block;
                text-align: center;

            }
            .dropdown-toggle{
                font-size: 17px;
                color: #262626 !important;
                background: #ffffff;
                text-align: left;
                border: none !important;
                padding: 7px 0;
            }
            .dropdown-toggle:after{
                vertical-align: middle;
            }
            .dropdown-toggle:focus {
                outline: none !important;
            }
            .custom-nav .hidden-small{
                display: none;
            }
            .custom-nav .visible-small{
                display: block;
            }
            .custom-nav .list-items ul{
                 padding: 0 !important;
            }
            .custom-nav .navbar-nav{
                margin: 20px 0 0 0;
            }

        }

        .header.animateIt {
            position: fixed;
            top: 0px !important;
            left: 0;
            right: 0;
            z-index: 999;
            background: #ffffff;
            box-shadow: 0px 20px 40px rgba(0, 0, 0, 0.02) !important;
            /*height: 80px;*/
            animation: smoothScroll 0.5s;
        / transition: slide-in--down 420ms cubic-bezier(.165,.84,.44,1); /
        / transition: 0.5s ease-in-out; /
        padding-bottom: 0;
        }

        @keyframes smoothScroll {
            0% {
                transform: translateY(-100px);

            }
            100% {
                transform: translateY(0px);

            }

        }

        .progress-container {
            width: 100%;
            height: 3px;
            background: #ffffff;
            margin-top: 20px;
        }

        .progress-bar {
            height: 3px;
            background: #0000ff !important;
            width: 0%;
        }
    </style>
</head>

<body <?php body_class(); ?>>


<div class="top-header-bar header-fixed">
    <div class="progress-container">
        <div class="progress-bar" id="myBar"></div>
    </div>
    <div class="container">
<!--        <div class="row">-->
<!--            <div class="col-md-3 col-sm-2 col-xs-12">-->
<!--                <div class="site-logo">-->
<!--                    --><?php //$url = get_template_directory_uri().'/assets/images/jtt logo.svg'; ?>
<!--                    <a href="--><?//=site_url()?><!--"><img src="--><?//=$url?><!--" class="custom-logo-img"></a>-->
<!--                </div>-->
<!--            </div>-->
<!---->
<!--            <div class="col-md-7 col-sm-8 col-xs-12">-->
<!--                <div id="header_menu">-->
<!--                    --><?php
//                    wp_nav_menu( array(
//                        'theme_location' => 'top',
//                        'menu_id'        => 'top-menu',
//                    ) );
//                    ?>
<!--                </div>-->
<!--            </div>-->
<!---->
<!--            <div class="col-md-2 col-sm-2 col-xs-12">-->
<!--                <div class="search">-->
<!--                    <ul>-->
<!--                        <li class="search-button">-->
<!--                    <span class="srch" onclick="document.getElementById('s_form').classList.toggle('f_toggle');">-->
<!--                        <svg class="svgIcon-use" width="25" height="25"><path d="M20.067 18.933l-4.157-4.157a6 6 0 1 0-.884.884l4.157 4.157a.624.624 0 1 0 .884-.884zM6.5 11c0-2.62 2.13-4.75 4.75-4.75S16 8.38 16 11s-2.13 4.75-4.75 4.75S6.5 13.62 6.5 11z"></path></svg>-->
<!--                    </span>-->
<!---->
<!--                            <form method="get" action="--><?//=site_url()?><!--" class="search-form" id="s_form">-->
<!--                                <input type="search" id="search-form" class="search-field" placeholder="Search..." value="--><?//=isset($_GET['s'])?$_GET['s']:''?><!--" name="s">-->
<!--                            </form>-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <a target="_blank" href="https://acquire.io" class="btn-link-started">Get started</a>-->
<!--                        </li>-->
<!--                    </ul>-->
<!--                </div>-->
<!--            </div>-->
<!---->
<!---->
<!---->
<!--        </div>-->

        <div class="custom-nav">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="#"><img alt="logo" src="<?php bloginfo('template_directory'); ?>/assets/images/header-logo.svg"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav">
                    <li class="nav-item dropdown hidden-small">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            category
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="<?php echo site_url(); ?>/category/digital-marketing/">Digital Marketing</a>
                            <a class="dropdown-item" href="<?php echo site_url(); ?>/category/customer-support/">Customer Support</a>
                            <a class="dropdown-item" href="<?php echo site_url(); ?>/category/web-design/">Web design</a>
                        </div>
                    </li>
                    <button type="button" class="visible-small dropdown-toggle collapse-btn" data-toggle="collapse" data-target="#features-btn">Features</button>
                    <div id="features-btn" class="collapse list-items">
                        <ul>
                            <a class="dropdown-item" href="<?php echo site_url(); ?>/category/digital-marketing/">Digital Marketing</a>
                            <a class="dropdown-item" href="<?php echo site_url(); ?>/category/customer-support/">Customer Support</a>
                            <a class="dropdown-item" href="<?php echo site_url(); ?>/category/web-design/">Web design</a>
                        </ul>
                    </div>

                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo site_url(); ?>/abount-us/">About us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo site_url(); ?>/contact-us/">Contact us</a>
                    </li>
                </ul>
                <form method="get" action="<?=site_url()?>" class="search-form form-inline my-2 my-lg-0" id="s_form">
                    <input type="search" id="search-form" class="search-field form-control mr-sm-2" placeholder="Search..." value="<?=isset($_GET['s'])?$_GET['s']:''?>" name="s"><button class="btn search-btn" type="submit"><img alt="logo" src="https://justtotaltech.com/wp-content/themes/jtt/assets/images/search.png"></button>
                </form>
                <a class="subscribe-btn" href="">Subscribe</a>
            </div>
        </nav>
        </div>

    </div>
</div>
<div class="header-manage"></div>
<div class="page-body" onclick="check_toggle();drop_list(null)">
    <!--<div class="top-header-menu">
    <div class="container">
        <?php
    /*        wp_nav_menu( array(
                'theme_location' => 'top',
                'menu_id'        => 'top-menu',
            ) );
            */?>
    </div>
</div>-->

    <script defer>
        // When the user scrolls the page, execute myFunction
        window.onscroll = function() {myFunction()};

        function myFunction() {
            var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
            var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
            var scrolled = (winScroll / height) * 100;
            document.getElementById("myBar").style.width = scrolled + "%";
        }
    </script>