<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

    <div class="category-page">
        <div class="container">

            <?php if ( have_posts() ) : ?>
                <header class="page-header">
                    <?php
                    the_archive_title( '<h1 class="main-title">', '</h1>' );
                    the_archive_description( '<div class="taxonomy-description">', '</div>' );
                    ?>
                </header>
            <?php endif; ?>

            <div class="row">
                <div class="col-md-8 col-sm-8" id="left_content">
                    <?php
                    if ( have_posts() ) : ?>
                        <?php
                        while ( have_posts() ) : the_post();
                            if (is_archive()) {
                                get_template_part( 'template-parts/post/category', '');
                            }else {
                                get_template_part( 'template-parts/post/content', get_post_format() );
                            }
                        endwhile;

                        the_posts_pagination( array(
                            'prev_text' => '<span class="screen-reader-text">' . __( 'Previous page', 'twentyseventeen' ) . '</span>',
                            'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'twentyseventeen' ) . '</span>',
                            'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( '', 'twentyseventeen' ) . ' </span>',
                        ) );

                    else :
                        if (is_archive())
                            get_template_part( 'template-parts/post/content', 'category' );
                        else
                            get_template_part( 'template-parts/post/content', 'none' );

                    endif; ?>
                </div>

                <div class="col-md-4 col-sm-4" id="right_content">
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>
    </div>

<?php get_footer();
