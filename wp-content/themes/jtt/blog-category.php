<?php
/* Template Name: blog-category page */
get_header(); ?>
<link href="<?php bloginfo('template_directory'); ?>/assets/css/custom-new-2.css" media="screen" rel="stylesheet"
      type="text/css">

    <div class="blog-category">
        <div class="container">
            <div class="tabs">
               <ul class="d-flex">
                   <li>
                       <a href="#" target="_blank">Customer Support</a>
                   </li>
                   <li>
                       <a href="#" target="_blank">Product</a>
                   </li>
                   <li>
                       <a href="#" target="_blank">Design</a>
                   </li>
                   <li>
                       <a href="#" target="_blank">Marketing</a>
                   </li>
                   <li>
                       <a href="#" target="_blank">Web design</a>
                   </li>
                   <li>
                       <a href="#" target="_blank">Digital Marketing</a>
                   </li>
               </ul>
            </div>
            <div class="customer-support">
                <div class="d-flex filter-panel align-center space-between">
                    <h3>Customer Support</h3>
                    <a class="recent" href="#" target="_blank">Most Recent</a>
                </div>
            </div>
            <div class="blog-list d-flex">
                <div class="three-block">
                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/blog-category/call-center-girl.png" alt="call-center-girl">
                    <p class="title">Customer  support</p>
                    <h3>10 best practices to improve customer support with help desk software</h3>
                    <p class="content">Offering a top-rate customer support and resolve issues and queries …</p>
                    <div class="d-flex bottom-stick">
                        <h5 class="name">Guy Alexander</h5>
                        <p class="time-ago">33 mins ago</p>
                        <p class="bid-time">4 min read</p>
                    </div>
                </div>
                <div class="three-block">
                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/blog-category/how-live-chat.png" alt="call-center-girl">
                    <p class="title">Customer  support</p>
                    <h3>6 Techniques – How Live Chat Creates Better Landing Page</h3>
                    <p class="content">Offering a top-rate customer support and resolve issues and queries …</p>
                    <div class="d-flex bottom-stick">
                        <h5 class="name">Guy Alexander</h5>
                        <p class="time-ago">33 mins ago</p>
                        <p class="bid-time">4 min read</p>
                    </div>
                </div>
                <div class="three-block">
                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/blog-category/help-desk-software.png" alt="call-center-girl">
                    <p class="title">Customer  support</p>
                    <h3>How to Use Help Desk Software to Improve Your Customer Support</h3>
                    <p class="content">Offering a top-rate customer support and resolve issues and queries …</p>
                    <div class="d-flex bottom-stick">
                        <h5 class="name">Guy Alexander</h5>
                        <p class="time-ago">33 mins ago</p>
                        <p class="bid-time">4 min read</p>
                    </div>
                </div>
            </div>
            <div class="blog-list d-flex">
                <div class="three-block">
                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/blog-category/10-best-practice.png" alt="call-center-girl">
                    <p class="title">Customer  support</p>
                    <h3>10 best practices to improve customer support with help desk software</h3>
                    <p class="content">Offering a top-rate customer support and resolve issues and queries …</p>
                    <div class="d-flex bottom-stick">
                        <h5 class="name">Guy Alexander</h5>
                        <p class="time-ago">33 mins ago</p>
                        <p class="bid-time">4 min read</p>
                    </div>
                </div>
                <div class="three-block">
                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/blog-category/customer-support.png" alt="call-center-girl">
                    <p class="title">Customer  support</p>
                    <h3>6 Techniques – How Live Chat Creates Better Landing Page</h3>
                    <p class="content">Offering a top-rate customer support and resolve issues and queries …</p>
                    <div class="d-flex bottom-stick">
                        <h5 class="name">Guy Alexander</h5>
                        <p class="time-ago">33 mins ago</p>
                        <p class="bid-time">4 min read</p>
                    </div>
                </div>
                <div class="three-block">
                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/blog-category/gadgets.png" alt="call-center-girl">
                    <p class="title">Customer  support</p>
                    <h3>10 best practices to improve customer support with help desk software</h3>
                    <p class="content">Offering a top-rate customer support and resolve issues and queries …</p>
                    <div class="d-flex bottom-stick">
                        <h5 class="name">Guy Alexander</h5>
                        <p class="time-ago">33 mins ago</p>
                        <p class="bid-time">4 min read</p>
                    </div>
                </div>
            </div>
            <div class="blog-list d-flex">
                <div class="three-block">
                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/blog-category/control-room.png" alt="call-center-girl">
                    <p class="title">Customer  support</p>
                    <h3>10 best practices to improve customer support with help desk software</h3>
                    <p class="content">Offering a top-rate customer support and resolve issues and queries …</p>
                    <div class="d-flex bottom-stick">
                        <h5 class="name">Guy Alexander</h5>
                        <p class="time-ago">33 mins ago</p>
                        <p class="bid-time">4 min read</p>
                    </div>
                </div>
                <div class="three-block">
                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/blog-category/laptop.png" alt="call-center-girl">
                    <p class="title">Customer  support</p>
                    <h3>6 Techniques – How Live Chat Creates Better Landing Page</h3>
                    <p class="content">Offering a top-rate customer support and resolve issues and queries …</p>
                    <div class="d-flex bottom-stick">
                        <h5 class="name">Guy Alexander</h5>
                        <p class="time-ago">33 mins ago</p>
                        <p class="bid-time">4 min read</p>
                    </div>
                </div>
                <div class="three-block">
                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/blog-category/robot.png" alt="call-center-girl">
                    <p class="title">Customer  support</p>
                    <h3>10 best practices to improve customer support with help desk software</h3>
                    <p class="content">Offering a top-rate customer support and resolve issues and queries …</p>
                    <div class="d-flex bottom-stick">
                        <h5 class="name">Guy Alexander</h5>
                        <p class="time-ago">33 mins ago</p>
                        <p class="bid-time">4 min read</p>
                    </div>
                </div>
            </div>
            <div class="pagination justify-center">
                <span aria-current="page" class="page-numbers current">1</span>
                <a class="page-numbers" href="https://acquire.io/blog/page/2/">2</a>
                <a class="page-numbers" href="https://acquire.io/blog/page/3/">3</a>
<!--                <span class="page-numbers dots">…</span>-->
<!--                <a class="page-numbers" href="https://acquire.io/blog/page/88/">88</a>-->
                <a class="next page-numbers" href="https://acquire.io/blog/page/2/">Next Page</a>
            </div>
        </div>
    </div>

<?php get_footer(); ?>
