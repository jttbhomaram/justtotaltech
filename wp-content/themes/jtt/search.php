<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<style>
    .page-header .main-title{
        margin-bottom: 50px;
    }
    .post-group{
        display: flex !important;
    }
    .content-post-title{
        font-weight: 500;
        padding: 0 0 15px 0 !important;
    }
    .search-list article{
        display: none;
    }
    .search-list{
        padding: 0;
        margin: 0;
        display: flex;
        flex-flow: wrap;
        width: 102%;
    }
    .search-list li{
        width: 31%;
        display: inline-block;
        margin: 0 28px 50px 0 !important;
        list-style: none !important;
        position: relative;
    }
    .search-list li img{
        height: 185px;
        object-fit: cover;
        object-position: center;
        transform: scale(1);
        -webkit-transition: -webkit-transform .4s ease-out 0s;
        transition: -webkit-transform .4s ease-out 0s;
        -o-transition: -o-transform .4s ease-out 0s;
        transition: transform .4s ease-out 0s;
        transition: transform .4s ease-out 0s,-webkit-transform .4s ease-out 0s,-o-transform .4s ease-out 0s;
        max-width: 100%;
        display: block;
        width: 100%;
    }
    .search-list li .img-wrap{
        overflow: hidden;
        height: 185px;
    }
    .search-list li:hover img{
        transform: scale(1.05) !important;
        -webkit-transform: scale(1.05) !important;
        -o-transform: scale(1.05) !important;
    }
    .search-list li:nth-child(1){
        display: none;
    }
    /*.search-list li:nth-child(5n){*/
        /*margin: 0 0 20px 0;*/
    /*}*/
    /*.search-list li:nth-child(8n){*/
        /*margin: 0 28px 20px 0;*/
    /*}*/
    .search-list li h4{
        font-size: 26px;
        color: #000000;
        font-family: 'PT Serif';
        font-weight: bold;
        margin: 0;
        padding: 15px 0;
    }
    .search-list li h5{
        font-size: 12px;
        color: #898989;
        font-family: 'Fira Sans';
        margin: 0;
        padding: 0 0 10px 0;
    }
    .navigation.pagination{
        margin: 60px auto;
        float: none;
        text-align: center;
    }
    .navigation.pagination .nav-links{
        margin: 0 auto;
    }
    .navigation.pagination .screen-reader-text{
        display: none;
    }
    .navigation.pagination .page-numbers .screen-reader-text{
        display: inline-block;
    }
    .page-header{
        border-bottom: 1px solid #ebedf0;
        margin-bottom: 50px;
    }
    .page-header .main-title{
        margin: 0 0 30px 0;
        font-family: 'PT Serif';
        font-weight: bold;
    }
    span.under-line{
        width: calc(100%);
        background-image: linear-gradient(transparent calc(100% - 2px), #001033 2px) !important;
        background-repeat: no-repeat;
        background-size: 0% 100%;
        transition: background-size 0.2s;
    }
    .search-list li:hover span.under-line{
        background-size: 100% 100%;
    }


    @media (max-width: 991px) {
        .search-list {
            display: block;
            width: 100%;
        }
        .search-list li {
            width: 100%;
        }
    }
    .category-page {
        min-height: calc(100vh - 310px);
    }
    .author-name {
        font-size: 12px;
        color: #0000ff;
        padding: 0 20px 0 0;
        float: left;
    }
    .date {
        padding: 0 !important;
        font-size: 12px;
        color: #898989;
        width: auto;
        float: left;
    }
    .author-data {
        position: absolute;
        bottom: -25px;
    }
</style>
    <div class="category-page">
        <div class="container">

            <header class="page-header">
                <?php if ( have_posts() ) : ?>
                    <h1 class="main-title"><?php printf( __( 'Search Results for: %s', 'twentyseventeen' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
                <?php else : ?>
                    <h1 class="main-title"><?php _e( 'Nothing Found', 'twentyseventeen' ); ?></h1>
                <?php endif; ?>
            </header>

            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div id="primary" class="content-area">
<!--                        --><?php
//                        if ( have_posts() ) :
//                            while ( have_posts() ) : the_post();
//                                get_template_part( 'template-parts/post/category');
//                            endwhile;
//
//                            the_posts_pagination( array(
//                                'prev_text' =>  '<span class="screen-reader-text">' . __( 'Previous page', 'twentyseventeen' ) . '</span>',
//                                'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'twentyseventeen' ) . '</span>',
//                                'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( '', 'twentyseventeen' ) . ' </span>',
//                            ) );
//
//                        else : ?>
<!---->
<!--                            <p>--><?php //_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'twentyseventeen' ); ?><!--</p>-->
<!--                        --><?php
//                        endif;
//                        ?>




                        <ul class="search-list">
                            <?php
                            if ( have_posts() ) :
                                $i=0;
                                /* Start the Loop */
                                while ( have_posts() ) : the_post();
                                    if($i==3){
//                                        echo '<div class="clearfix"></div>';
                                        $i=0;
                                    }
                                    $i++;
                                    /**
                                     * Run the loop for the search to output the results.
                                     * If you want to overload this in a child theme then include a file
                                     * called content-search.php and that will be used instead.
                                     */
                                    ?>

                                    <li>

                                        <a href="<?php the_permalink(); ?>">
                                            <div class="img-wrap">
                                                <?php
                                                the_post_thumbnail( 'blog-new-thumbnail', array( 'alt' => get_the_title() ) );
                                                ?>
                                            </div>
                                            <h4><span class="under-line"> <?php echo get_the_title();?></span></h4>
<!--                                            <h5>--><?php //echo get_the_date();?><!--</h5>-->
                                            <div class="author-data">
                                                <span class="author-name"><?php the_author(); ?></span>
                                                <span class="date"><?php echo get_the_date();?> | <?php echo do_shortcode('[rt_reading_time]');?> Min Read</span>
                                            </div>
                                        </a>
                                    </li>
                                    <?php
                                    get_template_part( 'template-parts/post/content', 'excerpt' );

                                endwhile; // End of the loop.

                                the_posts_pagination( array(
                                    'prev_text' =>  '<span class="screen-reader-text">' . __( 'Previous page', 'twentyseventeen' ) . '</span>',
                                    'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'twentyseventeen' ) . '</span>',
                                    'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( '', 'twentyseventeen' ) . ' </span>',
                                ) );

                            else : ?>

                                <p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'twentyseventeen' ); ?></p>
                            <?php
//				get_search_form();

                            endif;
                            ?>
                        </ul>





                    </div>
                </div>

<!--                <div class="col-md-4 col-sm-4">-->
<!--                    --><?php //get_sidebar(); ?>
<!--                </div>-->
            </div>

        </div><!-- .wrap -->
    </div>
<?php get_footer();
