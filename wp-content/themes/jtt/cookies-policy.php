<?php
/* Template Name: Cookies Policy page */
get_header(); ?>
<link href="<?php bloginfo('template_directory'); ?>/assets/css/custom-new-2.css" media="screen" rel="stylesheet"
      type="text/css">

    <div class="policy-page">
        <div class="top-panel text-center">
            <h1>Cookies Policy</h1>
        </div>
        <div class="container">
            <div class="content-panel">
                <p>This is the Cookies Policy for Just Total Tech, accessible from <a href="https://justtotaltech.com/" target="_blank">https://justtotaltech.com/</a></p>
                <h3>What Are Cookies</h3>
                <p>Almost all professional websites use cookies as is common practice, this site also uses cookies. Cookies are nothing but tiny files that are downloaded to your computer, to enhance your experience. This policy describes how we use it, what information it gathers, and why we need to store these cookies to enhance website services. We will also provide an overview of how to disable these cookies; however, it will result in also disabling certain features and functionality of this site.</p>
                <h3>How We Use Cookies</h3>
                <p>When you visit our website, we may place various types of cookies in your browser as detailed below. We use first-party and third-party cookies for analyzing and tracking usage, understanding visitor’s preferences and thus improving their experience. As per law, we can also store cookies which are strictly evident for the operation of this site.</p>
                <h3>Disabling Cookies</h3>
                <p>You can disable cookies by making changes to your browser settings (refer to browser Help). Make sure to understand the consequences of disabling cookies as it will affect your website features and functionalities. Therefore it is recommended that you do not disable cookies.</p>
                <ul class="links">
                    <li>
                        <a href="https://support.google.com/chrome/answer/95647?hl=en" target="_blank">Cookie settings in Chrome</a>
                    </li>
                    <li>
                        <a href="https://support.mozilla.org/en-US/kb/enhanced-tracking-protection-firefox-desktop?redirectslug=enable-and-disable-cookies-website-preferences&redirectlocale=en-US" target="_blank">Cookie settings in Firefox</a>
                    </li>
                    <li>
                        <a href="https://support.microsoft.com/en-us/help/17442/windows-internet-explorer-delete-manage-cookies" target="_blank">Cookie settings in Internet Explorer</a>
                    </li>
                    <li>
                        <a href="https://support.apple.com/en-in/guide/safari/sfri11471/mac" target="_blank">Cookie setting in Safari or iOS</a>
                    </li>
                </ul>
                <h3>The Cookies We Set</h3>
                <ul class="list-content">
                    <li><strong>Forms related cookies</strong>
                        <p>These cookies may be set to remember your user details for future correspondence when you provide data through contact pages or comment forms.</p>
                    </li>
                    <li><strong>Site preferences cookies</strong>
                        <p>We have the functionality to set your preferences on how this site operates when you use it, in order to enhance your experience on this site. We need to set cookies to remember your preferences; so that this information can be called whenever you interact with a page is affected by your preferences.</p>
                    </li>
                </ul>
                <h3>Third-Party Cookies</h3>
                <p>For special cases, we also use cookies provided by trusted third parties. The following section provides details on which third party cookies used on this site.</p>
                <p>This site uses Google Analytics which is one of the most widespread and trusted analytics solutions on the web for helping us to understand how you use the site and ways that we can improve your experience. These cookies may track things such as how long you spend on the site and the pages that you visit so we can continue to produce engaging content.</p>
                <p>For more information on Google Analytics cookies, see the official Google Analytics page.</p>
                <p>The Google AdSense service we use to serve to advertise uses a DoubleClick cookie to serve more relevant ads across the web and limit the number of times that a given ad is shown to you. For more information on Google AdSense see the official <a href="https://support.google.com/adsense/answer/3394713?hl=en" target="_blank">Google AdSense privacy FAQ.</a></p>
                <h3>More Information</h3>
                <p>Hopefully, that has clarified things for you and as was previously mentioned if there is something that you aren’t sure whether you need or not it’s usually safer to leave cookies enabled in case it does interact with one of the features you use on our site.</p>
                <p>However, if you are still looking for more information then you can contact us through one of our preferred contact methods:</p>
                <p>By visiting this link: <a href="https://justtotaltech.com/contact-us/" target="_blank">https://justtotaltech.com/contact-us/</a></p>
            </div>
        </div>
    </div>

<?php get_footer(); ?>
