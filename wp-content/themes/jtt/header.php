<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <!--    <link rel="stylesheet" type="text/css" href="--><?//=get_template_directory_uri().'/assets/css/bootstrap.css'?><!--">-->
    <!--    <link rel="stylesheet" type="text/css" href="--><?//=get_template_directory_uri().'/assets/css/fonts.css'?><!--">-->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Fira+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=PT+Serif:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;0,700;0,900;1,100;1,300;1,400;1,700;1,900&display=swap" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

    <?php wp_head();?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<!--    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-130959501-1"></script>-->
<!--    <script>-->
<!--        window.dataLayer = window.dataLayer || [];-->
<!--        function gtag(){dataLayer.push(arguments);}-->
<!--        gtag('js', new Date());-->
<!---->
<!--        gtag('config', 'UA-130959501-1');-->
<!--    </script>-->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-130959501-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-130959501-1');
    </script>
    <style>
        body{
            font-family: 'Fira Sans', sans-serif;
        }
        .custom-nav .visible-small{
            display: none;
        }
        .custom-nav .dropdown-toggle::after{
            vertical-align: middle;
        }

        .custom-nav .navbar {
            padding: 0; }
        .custom-nav .nav-link {
            font-size: 17px;
            color: #262626 !important;
            margin-right: 50px; }
        .custom-nav .form-control {
            border-radius: 4px;
            border: solid 1px #dedede;
            background-color: #ffffff;
            margin-right: 0 !important;
            height: 50px;
            padding: 0 40px 0 15px; }
        .custom-nav .form-control:focus {
            box-shadow: none; }
        .custom-nav ::placeholder {
            color: #001033 !important;
            opacity: 0.5 !important; }
        .custom-nav .form-inline {
            margin-right: 0;
            position: relative;
            max-width: 260px; }
        .custom-nav .search-btn {
            background: none !important;
            position: absolute;
            top: 5px;
            right: 4px; }
        .custom-nav .search-btn:focus {
            box-shadow: none; }
        .custom-nav .subscribe-btn {
            font-size: 17px;
            color: #ffffff;
            padding: 14px 34px;
            border-radius: 4px;
            background-color: #0000ff;
            border-color: #0000ff;
            box-shadow: none;
            outline: none;
            -webkit-appearance: none;
            border: none;
            font-family: 'Fira Sans', sans-serif;
            cursor: pointer;
        }
        .custom-nav .navbar-nav {
            margin-left: auto; }
        .custom-nav .nav-item:hover .dropdown-menu {
            display: block;
            box-shadow: 0 22px 54px 0 rgba(18, 51, 153, 0.22);
            -webkit-box-shadow: 0 22px 54px 0 rgba(18, 51, 153, 0.22);
            border: 1px solid transparent; }

        .dropdown-menu:before {
            content: url(../images/top-arrow.png);
            top: 6px;
            left: 50%;
            position: absolute;
            width: 100%;
            height: 100%;
            display: none;
        }

        #site-footer {
            background: #ffffff; }
        #site-footer p {
            font-size: 14px;
            color: #868686;
            margin: 50px 0 0 0; }
        #site-footer .footer-links {
            text-align: right;
            margin: 0; }
        #site-footer .footer-links li {
            display: inline-block; }
        #site-footer .footer-links li a {
            font-size: 17px;
            color: #121214;
            margin-left: 43px; }
        #site-footer .footer_social {
            border-top: none !important;
            margin: 50px 0 0 0; }
        #site-footer .footer_social a {
            margin-left: 10px; }
        #site-footer .footer_social a img {
            max-width: 20px;
        }
        .top-header-bar.header-fixed {
            padding: 20px 0;
            z-index: 99999
            box-shadow: 0 3px 16px 0 rgba(0,0,0,.1);
        }

        @media (min-width: 1200px) {
            .container {
                max-width: 1240px; } }
        @media only screen and (max-width: 991px) {
            .custom-nav .form-inline {
                margin-right: 0;
                max-width: 100%;
                display: none;
            }
            .custom-nav .form-inline input{
                width: 100%;
                margin: 0;
            }

            #site-footer .footer-links {
                width: 100%;
                padding: 0; }
            #site-footer .footer-links li {
                width: 50%;
                float: left;
                text-align: left;
                margin: 18px 0 0; }
            #site-footer .footer-links li a {
                margin: 0; }
            #site-footer .footer_social {
                margin: 30px 0 0 0;
                float: left;
                text-align: center;
                width: 100%; }
            #site-footer p {
                margin: 20px 0 0 0; }
            .custom-nav .subscribe-btn{
                width: 100%;
                max-width: 100%;
                display: inline-block;
                text-align: center;

            }
            .dropdown-toggle{
                font-size: 17px;
                color: #262626 !important;
                background: #ffffff;
                text-align: left;
                border: none !important;
                padding: 15px 0;
            }
            .dropdown-item{
                padding: 15px 10px;
            }
            .dropdown-toggle:after{
                vertical-align: middle;
            }
            .dropdown-toggle:focus {
                outline: none !important;
            }
            .custom-nav .hidden-small{
                display: none;
            }
            .custom-nav .visible-small{
                display: block;
            }
            .custom-nav .list-items ul{
                padding: 0 !important;
            }
            .custom-nav .navbar-nav{
                margin: 20px 0 0 0;
            }
            #site-footer .footer_social a {
                margin-left: 15px;
            }

        }


        @keyframes smoothScroll {
            0% {
                transform: translateY(-100px);

            }
            100% {
                transform: translateY(0px);

            }

        }

        .progress-container {
            width: 100%;
            height: 3px;
            background: transparent;
            margin-top: 21px;
            top: 80px;
            position: fixed;
            z-index: 99;
            left: 0;
            right: 0;
            animation: smoothScroll 0.5s;
            /*transition: slide-in--down 420ms cubic-bezier(.165,.84,.44,1);*/
            /*transition: 0.5s ease-in-out;*/
            padding-bottom: 0;
        }


        .progress-bar {
            height: 3px;
            background: #0000ff !important;
            width: 0%;
        }

        @media (max-width:991px) {
            .progress-container {display:none;}

            .icon-bar {
                width: 22px;
                height: 2px;
                background-color: #7f7f7f;
                display: block;
                transition: all 0.2s;
                margin-top: 4px;
            }

            .navbar-toggler {
                padding: 10px 15px 10px 17px;
            }

            .navbar-toggler.collapsed {
                padding: 10px 16px;
            }

            .navbar-toggler .top-bar {
                transform: rotate(45deg);
                transform-origin: 10% 10%;
            }

            .navbar-toggler .middle-bar {
                opacity: 0;
            }

            .navbar-toggler .bottom-bar {
                transform: rotate(-45deg);
                transform-origin: 10% 90%;
            }

            .navbar-toggler.collapsed .top-bar {
                transform: rotate(0);
            }

            .navbar-toggler.collapsed .middle-bar {
                opacity: 1;
            }

            .navbar-toggler.collapsed .bottom-bar {
                transform: rotate(0);
            }

            .navbar-toggler:focus, .navbar-toggler:hover {
                outline: none;
            }
            .page-body {
                padding-top: 30px;
            }
        }

        .header-subscribe  #mc_embed_signup {
            width: auto !important;
        }

        .header-subscribe  #mc_embed_signup  input.email {
            display: none !important;
        }

        @media (min-width: 992px) {
            .home-page {
                display: none;
            }
            .page-body {
                padding-top: 50px;
            }
        }


        .dropdown-item.active, .dropdown-item:active{
            background: transparent;
            color: #212529;
        }
    </style>
</head>

<body <?php body_class(); ?>>


<div class="top-header-bar header-fixed">
    <div class="container">
        <!--        <div class="row">-->
        <!--            <div class="col-md-3 col-sm-2 col-xs-12">-->
        <!--                <div class="site-logo">-->
        <!--                    --><?php //$url = get_template_directory_uri().'/assets/images/jtt logo.svg'; ?>
        <!--                    <a href="--><?//=site_url()?><!--"><img src="--><?//=$url?><!--" class="custom-logo-img"></a>-->
        <!--                </div>-->
        <!--            </div>-->
        <!---->
        <!--            <div class="col-md-7 col-sm-8 col-xs-12">-->
        <!--                <div id="header_menu">-->
        <!--                    --><?php
        //                    wp_nav_menu( array(
        //                        'theme_location' => 'top',
        //                        'menu_id'        => 'top-menu',
        //                    ) );
        //                    ?>
        <!--                </div>-->
        <!--            </div>-->
        <!---->
        <!--            <div class="col-md-2 col-sm-2 col-xs-12">-->
        <!--                <div class="search">-->
        <!--                    <ul>-->
        <!--                        <li class="search-button">-->
        <!--                    <span class="srch" onclick="document.getElementById('s_form').classList.toggle('f_toggle');">-->
        <!--                        <svg class="svgIcon-use" width="25" height="25"><path d="M20.067 18.933l-4.157-4.157a6 6 0 1 0-.884.884l4.157 4.157a.624.624 0 1 0 .884-.884zM6.5 11c0-2.62 2.13-4.75 4.75-4.75S16 8.38 16 11s-2.13 4.75-4.75 4.75S6.5 13.62 6.5 11z"></path></svg>-->
        <!--                    </span>-->
        <!---->
        <!--                            <form method="get" action="--><?//=site_url()?><!--" class="search-form" id="s_form">-->
        <!--                                <input type="search" id="search-form" class="search-field" placeholder="Search..." value="--><?//=isset($_GET['s'])?$_GET['s']:''?><!--" name="s">-->
        <!--                            </form>-->
        <!--                        </li>-->
        <!--                        <li>-->
        <!--                            <a target="_blank" href="https://acquire.io" class="btn-link-started">Get started</a>-->
        <!--                        </li>-->
        <!--                    </ul>-->
        <!--                </div>-->
        <!--            </div>-->
        <!---->
        <!---->
        <!---->
        <!--        </div>-->

        <div class="custom-nav">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="navbar-brand" href="<?php echo site_url(); ?>"><img alt="logo" src="<?php bloginfo('template_directory'); ?>/assets/images/header-logo.svg"></a>
                <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon-bar top-bar"></span>
                    <span class="icon-bar middle-bar"></span>
                    <span class="icon-bar bottom-bar"></span>
<!--                    <span class="navbar-toggler-icon"></span>-->
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav">
                        <li class="nav-item home-page">
                            <a class="nav-link" href="<?php echo site_url(); ?>">Home</a>
                        </li>

                        <ul class="navbar-nav">
                            <li class="nav-item home-page">
                                <a class="nav-link" href="<?php echo site_url(); ?>">Home</a>
                            </li>
                            <li class="nav-item dropdown hidden-small">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Technology
                                </a>
                                <?php
                                $args = array(
                                    'orderby'    => 'name',
                                    'hide_empty'       => false,
                                    'parent' => 12
                                );

                                $cats = get_categories($args);
                                ?>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <!--                                <a class="dropdown-item" href="--><?php //echo site_url(); ?><!--/category/ai">AI</a>-->
                                    <!--                                <a class="dropdown-item" href="--><?php //echo site_url(); ?><!--/category/big-data">Big Data</a>-->
                                    <!--                                <a class="dropdown-item" href="--><?php //echo site_url(); ?><!--/category/iot">IoT</a>-->
                                    <!--                                <a class="dropdown-item" href="--><?php //echo site_url(); ?><!--/category/cyber-security">Cyber Security</a>-->
                                    <?php foreach($cats as $cat) {?>
                                        <?php echo $cat->category_description; ?>
                                        <a class="dropdown-item" href="<?php echo get_category_link($cat->term_id); ?>"><?php echo $cat->name; ?></a>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </li>
                            <li class="nav-item dropdown hidden-small">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Entrepreneurship
                                </a>
                                <?php
                                $args = array(
                                    'orderby'    => 'name',
                                    'hide_empty'       => false,
                                    'parent' => 24
                                );

                                $cats = get_categories($args);
                                ?>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <!--                                <a class="dropdown-item" href="--><?php //echo site_url(); ?><!--/category/small-business">Small Business</a>-->
                                    <!--                                <a class="dropdown-item" href="--><?php //echo site_url(); ?><!--/category/startup-funding">Startup Funding</a>-->
                                    <!--                                <a class="dropdown-item" href="--><?php //echo site_url(); ?><!--/category/business-leadership">Business Leadership</a>-->
                                    <?php foreach($cats as $cat) {?>
                                        <?php echo $cat->category_description; ?>
                                        <a class="dropdown-item" href="<?php echo get_category_link($cat->term_id); ?>"><?php echo $cat->name; ?></a>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </li>
                            <li class="nav-item dropdown hidden-small">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Reviews
                                </a>
                                <?php
                                $args = array(
                                    'orderby'    => 'name',
                                    'hide_empty'       => false,
                                    'parent' => 25
                                );

                                $cats = get_categories($args);
                                ?>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <!--                                <a class="dropdown-item" href="--><?php //echo site_url(); ?><!--/category/ai-powered-tools">AI Powered Tools</a>-->
                                    <!--                                <a class="dropdown-item" href="--><?php //echo site_url(); ?><!--/category/email-marketing-tools">Email Marketing Tools</a>-->
                                    <!--                                <a class="dropdown-item" href="--><?php //echo site_url(); ?><!--/category/project-management-tools">Project Management Tools</a>-->
                                    <!--                                <a class="dropdown-item" href="--><?php //echo site_url(); ?><!--/category/marketing-automation-tools">Marketing Automation Tools</a>-->
                                    <?php foreach($cats as $cat) {?>
                                        <?php echo $cat->category_description; ?>
                                        <a class="dropdown-item" href="<?php echo get_category_link($cat->term_id); ?>"><?php echo $cat->name; ?></a>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </li>

<!--                            <li class="nav-item dropdown hidden-small">-->
<!--                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">-->
<!--                                    Marketing-->
<!--                                </a>-->
<!--                                --><?php
//                                $args = array(
//                                    'orderby'    => 'name',
//                                    'hide_empty'       => false,
//                                    'parent' => 8
//                                );
//
//                                $cats = get_categories($args);
//                                ?>
<!--                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">-->
<!--                                    --><?php //foreach($cats as $cat) {?>
<!--                                        --><?php //echo $cat->category_description; ?>
<!--                                        <a class="dropdown-item" href="--><?php //echo get_category_link($cat->term_id); ?><!--">--><?php //echo $cat->name; ?><!--</a>-->
<!--                                        --><?php
//                                    }
//                                    ?>
<!--                                </div>-->
<!--                            </li>-->

                            <li class="nav-item dropdown hidden-small">
                                <?php
                                $category_id = get_cat_ID( 'Gadgets' );
                                $category_link = get_category_link( $category_id );
                                ?>
                                <a class="nav-link" href="<?php echo esc_url( $category_link ); ?>">
                                    Gadgets
                                </a>

                            </li>
                            <button type="button" class="visible-small dropdown-toggle collapse-btn" data-toggle="collapse" data-target="#features-btn">Technology</button>

                            <?php
                            $args = array(
                                'orderby'    => 'name',
                                'hide_empty'       => false,
                                'parent' => 12
                            );

                            $cats = get_categories($args);
                            ?>
                            <div id="features-btn" class="collapse list-items">
                                <ul>
<!--                                    <a class="dropdown-item" href="--><?php //echo site_url(); ?><!--/category/ai">AI</a>-->
<!--                                    <a class="dropdown-item" href="--><?php //echo site_url(); ?><!--/category/big-data">Big Data</a>-->
<!--                                    <a class="dropdown-item" href="--><?php //echo site_url(); ?><!--/category/iot">IoT</a>-->
<!--                                    <a class="dropdown-item" href="--><?php //echo site_url(); ?><!--/category/cyber-security">Cyber Security</a>-->
                                    <?php foreach($cats as $cat) {?>
                                        <?php echo $cat->category_description; ?>
                                        <a class="dropdown-item" href="<?php echo get_category_link($cat->term_id); ?>"><?php echo $cat->name; ?></a>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </div>

                            <button type="button" class="visible-small dropdown-toggle collapse-btn" data-toggle="collapse" data-target="#entrepreneurship-btn">Entrepreneurship</button>
                            <?php
                            $args = array(
                                'orderby'    => 'name',
                                'hide_empty'       => false,
                                'parent' => 24
                            );

                            $cats = get_categories($args);
                            ?>
                            <div id="entrepreneurship-btn" class="collapse list-items">
                                <ul>
<!--                                    <a class="dropdown-item" href="--><?php //echo site_url(); ?><!--/category/small-business">Small Business</a>-->
<!--                                    <a class="dropdown-item" href="--><?php //echo site_url(); ?><!--/category/startup-funding">Startup Funding</a>-->
<!--                                    <a class="dropdown-item" href="--><?php //echo site_url(); ?><!--/category/business-leadership">Business Leadership</a>-->
                                    <?php foreach($cats as $cat) {?>
                                        <?php echo $cat->category_description; ?>
                                        <a class="dropdown-item" href="<?php echo get_category_link($cat->term_id); ?>"><?php echo $cat->name; ?></a>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                            <button type="button" class="visible-small dropdown-toggle collapse-btn" data-toggle="collapse" data-target="#reviews-btn">Reviews</button>
                            <?php
                            $args = array(
                                'orderby'    => 'name',
                                'hide_empty'       => false,
                                'parent' => 25
                            );

                            $cats = get_categories($args);
                            ?>
                            <div id="reviews-btn" class="collapse list-items">
                                <ul>
<!--                                    <a class="dropdown-item" href="--><?php //echo site_url(); ?><!--/category/ai-powered-tools">AI Powered Tools</a>-->
<!--                                    <a class="dropdown-item" href="--><?php //echo site_url(); ?><!--/category/email-marketing-tools">Email Marketing Tools</a>-->
<!--                                    <a class="dropdown-item" href="--><?php //echo site_url(); ?><!--/category/project-management-tools">Project Management Tools</a>-->
<!--                                    <a class="dropdown-item" href="--><?php //echo site_url(); ?><!--/category/marketing-automation-tools">Marketing Automation Tools</a>-->
                                    <?php foreach($cats as $cat) {?>
                                        <?php echo $cat->category_description; ?>
                                        <a class="dropdown-item" href="<?php echo get_category_link($cat->term_id); ?>"><?php echo $cat->name; ?></a>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </div>
<!--                            <button type="button" class="visible-small dropdown-toggle collapse-btn" data-toggle="collapse" data-target="#Marketing-btn">Marketing</button>-->
<!--                            --><?php
//                            $args = array(
//                                'orderby'    => 'name',
//                                'hide_empty'       => false,
//                                'parent' => 8
//                            );
//
//                            $cats = get_categories($args);
//                            ?>
<!--                            <div id="Marketing-btn" class="collapse list-items">-->
<!--                                <ul>-->
<!--                                             --><?php //foreach($cats as $cat) {?>
<!--                                        --><?php //echo $cat->category_description; ?>
<!--                                        <a class="dropdown-item" href="--><?php //echo get_category_link($cat->term_id); ?><!--">--><?php //echo $cat->name; ?><!--</a>-->
<!--                                        --><?php
//                                    }
//                                    ?>
<!--                                </ul>-->
<!--                            </div>-->
<!--                            <button type="button" class="visible-small dropdown-toggle collapse-btn" data-toggle="collapse" data-target="#Marketing-btn">Gadgets</button>-->
<!--                            --><?php
//                            $args = array(
//                                'orderby'    => 'name',
//                                'hide_empty'       => false,
//                                'parent' => 115
//                            );
//
//                            $cats = get_categories($args);
//                            ?>
<!--                            <div id="Marketing-btn" class="collapse list-items">-->
<!--                                <ul>-->
<!--                                    --><?php //foreach($cats as $cat) {?>
<!--                                        --><?php //echo $cat->category_description; ?>
<!--                                        <a class="dropdown-item" href="--><?php //echo get_category_link($cat->term_id); ?><!--">--><?php //echo $cat->name; ?><!--</a>-->
<!--                                        --><?php
//                                    }
//                                    ?>
<!--                                </ul>-->
<!--                            </div>-->

                            <?php
                            $category_id = get_cat_ID( 'Gadgets' );
                            $category_link = get_category_link( $category_id );
                            ?>
                            <a class="nav-link visible-small" href="<?php echo esc_url( $category_link ); ?>">
                                Gadgets
                            </a>


                        </ul>


                </div>
                </ul>
                     <form method="get" action="<?=site_url()?>" class="search-form form-inline my-2 my-lg-0" id="s_form">
                        <input type="search" id="search-form" class="search-field form-control mr-sm-2" placeholder="Search..." value="<?=isset($_GET['s'])?$_GET['s']:''?>" name="s"><button class="btn search-btn" type="submit"><img alt="logo" src="https://justtotaltech.com/wp-content/themes/jtt/assets/images/search.png"></button>
                    </form>
                </div>

            </nav>
        </div>

    </div>
</div>
<!--<div class="header-manage"></div>-->
<div class="page-body" onclick="check_toggle();drop_list(null)">
    <!--<div class="top-header-menu">
    <div class="container">
        <?php
    /*        wp_nav_menu( array(
                'theme_location' => 'top',
                'menu_id'        => 'top-menu',
            ) );
            */?>
    </div>
</div>-->

    <script defer>
    // When the user scrolls the page, execute myFunction
    // window.onscroll = function() {myFunction()};
    //
    // function myFunction() {
    //     var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
    //     var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
    //     var scrolled = (winScroll / height) * 100;
    //     document.getElementById("myBar").style.width = scrolled + "%";
    // }
    $( ".dropdown-item" ).each(function() {
        if($(this).text() == 'Uncategorized') {
        $(this).hide();
        }
    });
    </script>