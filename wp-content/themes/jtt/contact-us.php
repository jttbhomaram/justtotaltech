<?php
/* Template Name: contact-us page */
get_header(); ?>

<link href="<?php bloginfo('template_directory'); ?>/assets/css/custom-new-2.css" media="screen" rel="stylesheet"
      type="text/css">

<div id="the_contact">
    <div class="container">
        <?php while ( have_posts() ) : the_post(); ?>
            <!--<div class="banner-image">
            <img src="<?/*=get_the_post_thumbnail_url($post->ID)*/?>" class="img-responsive">
        </div>-->
            <?php the_content(); ?>

            <div class="contact-form d-flex space-between">
                <div class="left-sec">
                    <h1>We’re listening</h1>
                    <p>We are always ears to what our readers have to say. We’re fond of constructive criticism and are eager to know your opinions. Have something to share with us? Fill in the form below, and we’ll get back to you the soonest. </p>
                    <div class="row">
                        <div class="col-md-12">
                            <form method="post" id="the_contact_form">
                                <div class="form-group names fname">
                                    <label><span class="screen-reader-text">First Name</span></label>
                                    <input type="text" class="form-control" placeholder="Rio" id="contact_name" name="contact_name" required />
                                </div>
                                <div class="form-group names">
                                    <label><span class="screen-reader-text">Last Name</span></label>
                                    <input type="text" class="form-control" placeholder="Smith" id="contact_lname" name="contact_lname" required />
                                </div>
                                <div class="form-group">
                                    <label><span class="screen-reader-text">Email address</span></label>
                                    <input type="email" class="form-control" placeholder="example@email.com" id="contact_email" name="contact_email" required />
                                </div>
                                <!--                            <div class="form-group">-->
                                <!--                                <label><span class="screen-reader-text">Phone</span></label>-->
                                <!--                                <input type="text" class="form-control" placeholder="Phone *" id="contact_phone" name="contact_phone" required />-->
                                <!--                            </div>-->
                                <div class="form-group">
                                    <label><span class="screen-reader-text">Subject</span></label>
                                    <input type="text" class="form-control" placeholder="Subject" id="contact_subject" name="contact_subject" required />
                                </div>
                                <div class="form-group">
                                    <label><span class="screen-reader-text">Note</span></label>
                                    <textarea cols="10" class="form-control" placeholder="Enter message" id="contact_message" name="contact_message" required></textarea>
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary" value="Get in touch" name="send_message">
                                    <p class="resp_msg" style="margin-left:15px;display: inline-block;"></p>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="right-sec">
                    <div class=" relative">
                        <img src="<?php bloginfo('template_directory'); ?>/assets/images/contact/contact.png" alt="contact-us">
                        <img src="<?php bloginfo('template_directory'); ?>/assets/images/about-us/blue-dot.svg" alt="who-are-we" class="blue-dot">
                    </div>
                    <!--            <div class="contact-details">-->
                    <!--                <h4>San Francisco, CA.</h4>-->
                    <!--                <div class="d-flex">-->
                    <!--                    <div class="address">-->
                    <!--                        <p>133 Kearny street. suite 401, San<br> Francisco, CA 94108</p>-->
                    <!--                    </div>-->
                    <!--                    <div class="email-detail">-->
                    <!--                        <a href="mailto:hello@jtt.io">hello@jtt.io</a>-->
                    <!--                        <a href="tel:+1 (415) 212-5151">+1 (415) 212-5151</a>-->
                    <!--                    </div>-->
                    <!--                </div>-->
                    <!--            </div>-->

                </div>
            </div>
        <?php endwhile; ?>
    </div>

</div>

<?php get_footer(); ?>

<script>
    jQuery(document).ready(function ($) {
        $(document).on('submit', 'form#the_contact_form', function (e) {
            e.preventDefault();
            var form = $('#the_contact_form');
            var data = {
                'action': 'the_contact_form',
                'contact_name': form.find('#contact_name').val(),
                'contact_lname': form.find('#contact_lname').val(),
                'contact_email': form.find('#contact_email').val(),
                // 'contact_phone': form.find('#contact_phone').val(),
                'contact_message': form.find('#contact_message').val(),
                'contact_subject': form.find('#contact_subject').val(),

            };
            $.ajax({
                url: ajaxurl,
                type: 'POST',
                data: data,
                dataType: "json",
                cache: false,
                beforeSend: function () {
                    $(".resp_msg").html('<img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==">').fadeIn();
                },
                success: function (resp) {
                    var color = '#e43300';
                    if (resp.status){
                        color = '#009200';
                        form[0].reset();
                    }
                    $(".resp_msg").html(resp.msg).css('color',color);
                }
            });
        });
    });
</script>
