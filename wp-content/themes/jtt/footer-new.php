<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

<br>
<br>



    <footer id="site-footer" class="site-footer" role="contentinfo">

        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <?php get_template_part( 'template-parts/footer/footer', 'widgets' ); ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <img alt="logo" src="<?php bloginfo('template_directory'); ?>/assets/images/footer-logo.svg">
                    <p>© Copyright 2020 JUST TOTAL TECH™</p>
                </div>

                <div class="col-md-6">
                    <ul class="footer-links">
                        <li>
                            <a href="#">Help</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url();?>">Blog</a>
                        </li>
                        <li>
                            <a href="#">Privacy</a>
                        </li>
                        <li>
                            <a href="#">Terms</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url();?>/abount-us/">About</a>
                        </li>
                    </ul>
                    <div class="footer_social">
                        <a target="_blank" href="https://twitter.com/justtotaltech">
                            <img alt="logo" src="<?php bloginfo('template_directory'); ?>/assets/images/instagram.svg">
                        </a>
                        <a target="_blank" href="http://www.youtube.com/user/justtotaltech">
                            <img alt="logo" src="<?php bloginfo('template_directory'); ?>/assets/images/youtube.svg">
                        </a>
                        <a target="_blank" href="https://twitter.com/justtotaltech">
                            <img alt="logo" src="<?php bloginfo('template_directory'); ?>/assets/images/twitter.svg">
                        </a>
                        <a target="_blank" href="http://www.linkedin.com/company/just-total-tech-limited">
                            <img alt="logo" src="<?php bloginfo('template_directory'); ?>/assets/images/linkedin.svg">
                        </a>
                        <a target="_blank" href="https://www.facebook.com/justtotaltech">
                            <img alt="logo" src="<?php bloginfo('template_directory'); ?>/assets/images/facebook.svg">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </footer>




</div>
<script>

    if (document.getElementById("contribute_email") !==null) {
        document.getElementById("contribute_email").onchange = function (e) {
            var email = e.target.value;

            var filter = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (!filter.test(email)) {
                alert('Please provide a valid email address : '+email);
            }else {
                alert('Thanks !'+email);
            }
        };
    }



    function check_toggle() {
        let cl = document.getElementById('s_form');
        if (cl.className.indexOf("f_toggle") !==-1)
            cl.classList.toggle('f_toggle');
    }

    function drop_list(elem=null) {
        var lis = document.querySelectorAll('#top-menu .sub-menu.f_toggle');
        if (elem == null && lis.length > 0) {
            let cc = lis[0].className+"";
            if (cc.indexOf("f_toggle") !==-1)
                lis[0].classList.toggle('f_toggle');
        }else if (elem !== null) {

            for (var x = 0; x < lis.length; x++) {
                if (lis[x].id !== elem)
                    lis[x].classList.remove('f_toggle');
            }
            document.getElementById(elem).classList.toggle('f_toggle');
        }
        return false;
    }

    function _getOffset( el ) {
        var _x = 0;
        var _y = 0;
        while( el && !isNaN( el.offsetLeft ) && !isNaN( el.offsetTop ) ) {
            _x += el.offsetLeft - el.scrollLeft;
            _y += el.offsetTop - el.scrollTop;
            el = el.offsetParent;
        }
        return { top: _y, left: _x };
    }


    window.addEventListener('load', ()=> {

        if (document.getElementById('left_content'))
            document.getElementById('left_content').setAttribute('style','height:'+document.getElementById('right_content').clientHeight+'px');

        var lis = document.querySelectorAll('#top-menu .menu-item-has-children');
        for (var x = 0; x < lis.length; x++) {
            let list = lis[x].children;
            for (var i = 0; i < list.length; i++) {
                var id = 'child-'+lis[x].id;
                list[i].setAttribute('onclick', 'drop_list("'+id+'")');
                list[i].removeAttribute('href');
                if (i > 0)
                    list[i].setAttribute('id', id);
            }

        }

    });

    var r = null;
    var s = null;
    if (document.getElementById('single_post_content')) {
        r = document.getElementById('single_post_content');
        s = document.getElementById('social-sidebar');
        var right_offset = _getOffset(r).top;
        var right_height = r.clientHeight;
        var side_bar_height = s.clientHeight;
        var side_bar_offset = s.offsetTop;
    }


    window.addEventListener('scroll', () => {
        var scroll = this.pageYOffset;

        /*if (scroll > 200) {
            document.getElementsByClassName('top-header-bar')[0].classList.add('header-fixed');
        }
        if (scroll < 15) {
            document.getElementsByClassName('top-header-bar')[0].classList.remove('header-fixed');
        }*/


        if (r !==null && s !== null) {
            var side_bar = document.getElementsByClassName('custom-sidebar');
            var length = (right_height - side_bar_height + right_offset);

            var height = s.clientHeight-70 + 'px';

            if (document.getElementById("post-sharing")) {
                var side_bar = document.getElementById("post-sharing");
                if (scroll < _getOffset(document.getElementById('social-sidebar')).top) {
                    side_bar.style.position = 'absolute';
                    side_bar.style.top = 0;

                } else if (scroll > length) {
                    side_bar.style.position = 'absolute';
                    side_bar.style.bottom = 0;
                    side_bar.style.top = 'auto';

                } else {
                    side_bar.style.position = 'fixed';
                    side_bar.style.height = height;
                    side_bar.style.top = '50px';
                }
            }
        }
    });
</script>
<?php wp_footer(); ?>
</body>
</html>
