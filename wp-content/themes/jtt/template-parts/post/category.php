<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

$link = get_permalink(get_the_ID());
$thumb = get_the_post_thumbnail_url();
$author = 'A';
$date = ' D';
$content = wp_trim_words( get_the_content(), 70, NULL);

/*echo '<div class="post-group">';
$thumb = get_the_post_thumbnail_url();
echo '<a href="'.get_the_permalink().'" class="thumb" style="background-image: url('.$thumb.')"></a>';
the_title( '<h1 class="title">', '</h1>' );
$content = wp_trim_words( get_the_content(), 70, NULL);
echo '<p><a href="'.get_permalink(get_the_ID()).'">'.$content.'</a></p>';
echo '</div>';*/
?>



<div class="row post-group">
    <div class="col-md-8 col-sm-8">
        <h1 class="content-post-title">
            <a href="<?php the_permalink(); ?>"><?php the_title();?></a>
        </h1>
        <p class="content">
            <?=wp_trim_words( get_the_content(), 15, NULL )?>
        </p>

        <div class="other-info">
            <p class="author"><?php echo get_the_author()?></p>
            <p class="publish-date"><?=get_the_date('d M Y')?></p>
        </div>
    </div>

    <div class="col-md-4 col-md-4">
        <a href="<?=get_permalink()?>" class="post_thumbnail" style="background-image: url('<?=get_the_post_thumbnail_url(get_the_ID())?>')"></a>
    </div>
</div>