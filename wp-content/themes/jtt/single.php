<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header();
setPostViews(get_the_ID());
$author_id = get_the_author_meta('ID');
$post_id = get_queried_object_id();
?>

    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/custom-new.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/custom-new-2.css">


<style>
    .blog-details-page .main-blk .right-blk .related-post ul li:nth-child(2){
        padding: 0;
    }
    .jtt-post-content iframe{
        max-width: 100%;
    }

</style>

    <a id="button"></a>

    <div class="blog-page blog-details-page">
        <section class="blog-sec">
            <div class="progress-container">
                <div class="progress-bar" id="myBar"></div>
            </div>
            <div class="container">
                <div class="main-blk top-bar">
                    <div class="left-blk">
                        <button class="back-btn" type="button" onclick="history.back();"> Go back </button>
                        <div class="desk-top">
                            <div class="inn-author">
                                <?php $author_id = get_the_author_meta('ID');
                                ?>
                                <img class="logo-author" alt="logo" src="<?php echo get_avatar_url($author_id)?>">
                                <?php
                                while ( have_posts() ) : the_post();
                                    ?>
                                    <span><?php the_author(); ?></span><br>
                                <?php endwhile; ?>
                                <div class="date"><span><?php echo get_the_date();?><br> <?php echo do_shortcode('[rt_reading_time]');?> Min Read</span></div>
                            </div>
                            <h4><?php echo get_the_category_list();?></h4>
                        </div>
                    </div>
                    <div class="right-blk">
                        <?php
                        $image_url= wp_get_attachment_image_src( get_post_thumbnail_id(),array( 1300, 513 ) );
                        if(!empty($image_url)){
                            echo '<img class="blog-details-img"  src="'.$image_url[0].'" alt="'.get_the_title().'"/>';
                        }
                        ?>
                    </div>
                </div>
                <div class="main-blk bottom-bar">
                    <div class="left-blk">
                        <div class="sidebar left-sidebar">
                            <h5>Content</h5>
                            <ul id="summery-list">
                            </ul>
                        </div>

                    </div>
                    <div class="right-blk">
<!--                        <a href="--><?php //echo site_url(); ?><!--" class="back-btn"><img alt="logo" src="--><?php //bloginfo('template_directory'); ?><!--/assets/images/blog/back-btn.svg">Back</a>-->

<!--                        --><?php
//                        $image_url= wp_get_attachment_image_src( get_post_thumbnail_id(),array( 1300, 513 ) );
//                        if(!empty($image_url)){
//                            echo '<img class="blog-details-img"  src="'.$image_url[0].'" alt="'.get_the_title().'"/>';
//                        }
//                        ?>
                        <div class="inn-blk">
                            <div class="left-inn">
                                <div class="top-part">


                                    <div class="inn-author">
                                    <?php $author_id = get_the_author_meta('ID');
                                    ?>
                                    <img class="logo-author" alt="logo" src="<?php echo get_avatar_url($author_id)?>">
                                    <?php
                                    while ( have_posts() ) : the_post();
                                        ?>
                                        <span><?php the_author(); ?></span><br>
                                    <?php endwhile; ?>
                                    <div class="date"><span><?php echo get_the_date();?> | <?php echo do_shortcode('[rt_reading_time]');?> Min Read</span></div>
                                    </div>
                                    <h4><?php echo get_the_category_list();?></h4>

                                    <h1><?php the_title();?></h1>
                                <div class="jtt-post-content">
                                    <?php
                                    $con= do_shortcode(get_post_field('post_content', get_the_ID()));
                                    $con=str_replace("[site_url]",site_url(),$con);
                                    $con=str_replace("[template_url]",get_bloginfo('template_url'),$con);
//                                    $con=str_replace("<a",'<a target="_blank"',$con);

                                    echo wpautop($con);

                                    ?>


<!--                                    <div class="table-content">-->
<!--                                        <table>-->
<!--                                            <tbody><tr>-->
<!--                                                <th>Name</th>-->
<!--                                                <th>Supported Platforms</th>-->
<!--                                            </tr>-->
<!--                                            <tr>-->
<!--                                                <td><a href="#">Name01</a></td>-->
<!--                                                <td>Windows, Mac, Linux and Ubuntu</td>-->
<!--                                            </tr>-->
<!--                                            <tr>-->
<!--                                                <td><a href="#">Name02</a></td>-->
<!--                                                <td>Windows</td>-->
<!--                                            </tr>-->
<!--                                            <tr>-->
<!--                                                <td><a href="#">Name03</a></td>-->
<!--                                                <td>Windows and Mac</td>-->
<!--                                            </tr>-->
<!--                                            <tr>-->
<!--                                                <td><a href="#">Name04</a></td>-->
<!--                                                <td>Windows, Mac and Linux</td>-->
<!--                                            </tr>-->
<!--                                            <tr>-->
<!--                                                <td><a href="#">Name05</a></td>-->
<!--                                                <td>Yoshi Tannamuri</td>-->
<!--                                            </tr>-->
<!--                                            </tbody>-->
<!--                                        </table>-->
<!--                                    </div>-->

                                </div>
                                </div>
                            </div>
                        </div>

                        <div class="subscribe-blk" id="subscribe-blk">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/blog/newsletter-icon.svg" alt="newsletter-icon"/>
                            <span id="close"><img src="<?php bloginfo('template_directory'); ?>/assets/images/blog/input-arrow.svg" alt="input-arrow"/></span>
                            <h5>Sign up to<br> our newsletter</h5>
                            <div class="email-blk">
                                <button type="button" class="btn btn-primary subscribe-btn white" data-toggle="modal" data-target="#exampleModal">
                                    Subscribe
                                </button>
                            </div>
                        </div>



                    </div>
                    <div class="side-blk">
                        <div class="sidebar">
                            <div class="grey-border">
                            <h4>Share Article on</h4>
                            <div class="social-icons">
                                <a target="_blank" href="http://twitter.com/share?url=<?php the_permalink(); ?>&text=<?php echo  get_the_title() ;?>&via=justtotaltech">
                                    <img alt="logo" src="<?php bloginfo('template_directory'); ?>/assets/images/twitter.svg">
                                    <img alt="logo" class="hide-logo" src="<?php bloginfo('template_directory'); ?>/assets/images/twitter-blue.svg">
                                </a>
                                <a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink(); ?>">
                                    <img alt="logo" src="<?php bloginfo('template_directory'); ?>/assets/images/linkedin.svg">
                                    <img alt="logo" class="hide-logo" src="<?php bloginfo('template_directory'); ?>/assets/images/linkedin-blue.svg">
                                </a>
                                <a target="_blank" href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>">
                                    <img alt="logo" src="<?php bloginfo('template_directory'); ?>/assets/images/facebook.svg">
                                    <img alt="logo" class="hide-logo" src="<?php bloginfo('template_directory'); ?>/assets/images/facebook-blue.svg">
                                </a>
                            </div>
                            <!--                            <p>Copyright © 2020 - 2020 Just Total Tech</p>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="related-post rel-post">
                <div class="container">
                    <h3>Related Articles</h3>
                    <ul class="sec-list">
                    <?php

                    //echo $post->ID;

                    $args = array(
                        'post_type' => 'post',
                        'post_status' => 'publish',
                        'posts_per_page'=>'3',
                        'orderby'  => 'rand',
                        'category__in' => wp_get_post_categories($post->ID)
                    );
                    $the_query = new WP_Query( $args );
                    $url= wp_get_attachment_image_src( get_post_thumbnail_id(),1000 );
                    $image_thumb=get_the_post_thumbnail( $post_id, array(410,432) );
                    $url = get_the_post_thumbnail_url( $post_id, 'medium' );
                    ?>
                    <?php if ( $the_query->have_posts() ) { ?>
                        <?php while ( $the_query->have_posts() ) : $the_query->the_post();
                            $read_url= wp_get_attachment_image_src( get_post_thumbnail_id(),'1000' );
                            ?>
                            <li>
                                <a href="<?php the_permalink(); ?>"> <div class="img-wrap">  <img src="<?php echo $read_url[0] ?>" alt="<?php echo get_the_title()?>"/> <div class="image-placeholder"></div> </div></a>
                                <h4 class="small-title"><?php echo get_the_category_list();?></h4>
                                <a href="<?php the_permalink(); ?>"><h2 class="main-title"><span class="hover-line"><?php echo  get_the_title() ;?></span></h2></a>
                                <p><?php echo wp_trim_words( get_the_content(), 9, '...' );?></p>
                                <div class="author-data">
                                    <span class="author-name"><?php the_author(); ?></span>
                                    <span class="date"><?php echo get_the_date();?> | <?php echo do_shortcode('[rt_reading_time]');?> Min Read</span>
                                </div>
                            </li>
                        <?php endwhile; ?>
                        <?php wp_reset_postdata(); ?>
                    <?php } ?>

                </ul>
                </div>
            </div>
            <hr>
            <div class="newsletter-blk text-center">
                <div class="container">
                    <div class="left-blk">
                    <h2 class="main-title">Sign up to newsletter</h2>
                    <p>News insights you won’t delete. Delivered to your inbox weekly.</p>
                    <button type="button" class="btn btn-primary subscribe-btn" data-toggle="modal" data-target="#exampleModal">
                        Subscribe
                    </button>
                </div>
                </div>
            </div>
        </section>

        <button class="navbar-toggler collapsed follow" type="button" data-toggle="collapse" data-target="#navbarSupported" aria-controls="navbarSupported" aria-expanded="false" aria-label="Toggle navigation">
            <span class="follow-icon">
                <img alt="logo" src="<?php bloginfo('template_directory'); ?>/assets/images/follow-icon.png" alt="sharing-icon">
            </span>
            <span class="icon-bar top-bar"></span>
            <span class="icon-bar middle-bar"></span>
            <span class="icon-bar bottom-bar"></span>
            <!--                    <span class="navbar-toggler-icon"></span>-->
        </button>
    <div class="collapse navbar-collapse social-icons" id="navbarSupported">
        <h4 class="text-center">Share Article on</h4>
        <ul class="navbar-nav">
            <li class="nav-item home-page">
                <a target="_blank" href="https://twitter.com/justtotaltech">
                    <img alt="logo" src="<?php bloginfo('template_directory'); ?>/assets/images/twitter.svg">
                    <img alt="logo" class="hide-logo" src="<?php bloginfo('template_directory'); ?>/assets/images/twitter-blue.svg">
                </a>
            </li>
            <li class="nav-item home-page">
                <a target="_blank" href="https://www.linkedin.com/company/just-total-tech-limited/">
                    <img alt="logo" src="<?php bloginfo('template_directory'); ?>/assets/images/linkedin.svg">
                    <img alt="logo" class="hide-logo" src="<?php bloginfo('template_directory'); ?>/assets/images/linkedin-blue.svg">
                </a>
            </li>
            <li class="nav-item home-page">
                <a target="_blank" href="https://www.facebook.com/justtotaltech">
                    <img alt="logo" src="<?php bloginfo('template_directory'); ?>/assets/images/facebook.svg">
                    <img alt="logo" class="hide-logo" src="<?php bloginfo('template_directory'); ?>/assets/images/facebook-blue.svg">
                </a>
            </li>
        </ul>
    </div>
    </div>

    <!-- Modal -->
    <div class="modal custom-popup" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body" id="subscribe-popup">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <!--                        <span aria-hidden="true">&times;</span>-->
                        <img src="<?php bloginfo('template_directory'); ?>/assets/images/blog/input-arrow.svg" alt="input-arrow"/>
                    </button>
                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/blog/subscribe-popup-img.svg" alt="popup-img">
                    <!-- Begin Mailchimp Signup Form -->
                    <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
                    <style type="text/css">
                        #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
                        /* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.
                        We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
                    </style>
                    <div id="mc_embed_signup">
                        <form action="https://justtotaltech.us18.list-manage.com/subscribe/post-json?u=24195892ef9a3e2c3b04b68ae&amp;id=a100e07e01&c=?" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                            <div id="mc_embed_signup_scroll">
                                <h2>Subscribe for daily updates</h2>
                                <!--                                <div class="indicates-required"><span class="asterisk">*</span> indicates required</div>-->
                                <div class="mc-field-group">
<!--                                    <label for="mce-EMAIL">Email Address <span class="asterisk">*</span></label>-->
                                    <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" autofocus placeholder="Email Address">
                                </div>
                                <div class="mc-field-group">
<!--                                    <label for="mce-FNAME">First Name <span class="asterisk">*</span></label>-->
                                    <input type="text" value="" name="FNAME" class="required" id="mce-FNAME" placeholder="First Name">
                                </div>
                                <div class="mc-field-group">
<!--                                    <label for="mce-LNAME">Last Name <span class="asterisk">*</span></label>-->
                                    <input type="text" value="" name="LNAME" class="required" id="mce-LNAME" placeholder="Last Name">
                                </div>
                                <div id="mce-responses" class="clear">
                                    <div class="response" id="mce-error-response" style="display:none"></div>
                                    <div class="response" id="mce-success-response" style="display:none"></div>
                                </div> <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_24195892ef9a3e2c3b04b68ae_a100e07e01" tabindex="-1" value=""></div>
                                <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button submit"></div>
                            </div>
                        </form>
                        <div id="subscribe-result"></div>
                    </div>

                    <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[3]='ADDRESS';ftypes[3]='address';fnames[4]='PHONE';ftypes[4]='phone';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
                    <!--End mc_embed_signup-->
                </div>
            </div>
        </div>
        <div class="thanks-popup">
            <div class="modal-body hidden" id="my-popup">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <!--                        <span aria-hidden="true">&times;</span>-->
                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/blog/closecon.svg" alt="input-arrow"/>
                </button>

                <img src="<?php bloginfo('template_directory'); ?>/assets/images/blog/thanks-blog.png" alt="popup-img">
                <h3>You’re almost done</h3>
                <p>Please click on the link sent onto your registered email address to confirem your account and complete the subscription process</p>
            </div>
        </div>
    </div>
    <div class="random-box">
        <div class="lightbox lightbox_1" >
    <div class="box">
        <span id="close"><img src="<?php bloginfo('template_directory'); ?>/assets/images/blog/input-arrow.svg" alt="input-arrow"/></span>
        <div class="content">
            <h3>Thank you for visiting our site</h3>
            <!--                <p class="bottom-info">Before you leave, checkout this trending AI open source tools write-up. Have an insightful reading session!</p>-->
            <div class="d-flex space-between top-box">
                <div class="img-wrap">
                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/blog/open-source.png" alt="open-source"/>
                </div>
                <div class="content-side">
                    <h4>Top 10 Open Source Artificial Intelligence Software You Should Be Aware Of</h4>
                    <!--                        <p>Apple’s Siri. Google’s OK Google. Amazon’s Echo services.</p>-->
                    <div class="read-more">
                        <a href="https://justtotaltech.com/open-source-artificial-intelligence-software/">Read full Article</a>
                    </div>
                </div>
            </div>
            <div class="d-flex space-between bottom-box align-center">
                <div class="info">
                    <p>Don’t miss the new articles!</p>
                </div>
                <div class="footer-subscribe-btn">

                    <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
                    <style type="text/css">
                        #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
                        /* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.
                           We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
                    </style>
                    <div id="mc_embed_signup">
                        <form action="https://thenextscoop.us3.list-manage.com/subscribe/post-json?u=2ea26dad4a6c54fec6e6ff666&amp;id=703166db06&c=?" method="post" id="mc-embedded-subscribe-form-two" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                            <div id="mc_embed_signup_scroll">

                                <div class="mc-field-group">
                                    <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address" >
                                </div>

                                <div id="mce-responses" class="clear">
                                    <div class="response" id="mce-error-response" style="display:none"></div>
                                    <div class="response" id="mce-success-response" style="display:none"></div>
                                </div>
                                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_2ea26dad4a6c54fec6e6ff666_703166db06" tabindex="-1" value=""></div>
                                <div class="clear arrow-img ">
                                    <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe"  class="subscribe-btn">
                                </div>
                            </div>
                        </form>
                        <div id="subscribe-result-two"></div>
                    </div>
                    <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($)
                        {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='ADDRESS';ftypes[3]='address';fnames[4]='PHONE';ftypes[4]='phone';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
                </div>
            </div>
        </div>
    </div>
</div>
        <div class="lightbox lightbox_2 dark" >
        <div class="box">
            <span id="close"><img src="<?php bloginfo('template_directory'); ?>/assets/images/blog/close-white.svg" alt="input-arrow"/></span>
            <div class="content">
                <h3>Thank you for visiting our site</h3>
                <!--                <p class="bottom-info">Before you leave, checkout this trending AI open source tools write-up. Have an insightful reading session!</p>-->
                <div class="d-flex space-between top-box">
                    <div class="img-wrap">
                        <img src="<?php bloginfo('template_directory'); ?>/assets/images/blog/open-source.png" alt="open-source"/>
                    </div>
                    <div class="content-side">
                        <h4>Top 10 Open Source Artificial Intelligence Software You Should Be Aware Of</h4>
                        <!--                        <p>Apple’s Siri. Google’s OK Google. Amazon’s Echo services.</p>-->
                        <div class="read-more">
                            <a href="https://justtotaltech.com/open-source-artificial-intelligence-software/">Read full Article</a>
                        </div>
                    </div>
                </div>
                <div class="d-flex space-between bottom-box align-center">
                    <div class="info">
                        <p>Don’t miss the new articles!</p>
                    </div>
                    <div class="footer-subscribe-btn">
                        <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
                        <style type="text/css">
                            #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
                            /* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.
                               We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
                        </style>
                        <div id="mc_embed_signup">
                            <form action="https://thenextscoop.us3.list-manage.com/subscribe/post-json?u=2ea26dad4a6c54fec6e6ff666&amp;id=703166db06&c=?" method="post" id="mc-embedded-subscribe-form-three" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                <div id="mc_embed_signup_scroll">

                                    <div class="mc-field-group">
                                        <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address" >
                                    </div>

                                    <div id="mce-responses" class="clear">
                                        <div class="response" id="mce-error-response" style="display:none"></div>
                                        <div class="response" id="mce-success-response" style="display:none"></div>
                                    </div>
                                    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_2ea26dad4a6c54fec6e6ff666_703166db06" tabindex="-1" value=""></div>
                                    <div class="clear arrow-img ">
                                        <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe"  class="subscribe-btn">
                                    </div>
                                </div>
                            </form>
                            <div id="subscribe-result-three"></div>
                        </div>
                        <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($)
                            {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='ADDRESS';ftypes[3]='address';fnames[4]='PHONE';ftypes[4]='phone';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    <script defer>
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if(scroll >= 500) {
            $(".subscribe-blk").addClass("change");
        } else {
            $(".subscribe-blk").removeClass("change");
        }
    });
</script>
    <script defer>
        $(document).ready(function () {
            $("#close").click(function () {
                $("#subscribe-blk").hide();
            });
        });



        $(document).ready(function () {
            var $form = $('#mc-embedded-subscribe-form')
            if ($form.length > 0) {
                $('#mc-embedded-subscribe-form input[type="submit"]').bind('click', function (event) {
                    if (event) event.preventDefault()
                    register($form)
                })
            }
            var $form_two = $('#mc-embedded-subscribe-form-two')
            if ($form_two.length > 0) {
                $('#mc-embedded-subscribe-form-two input[type="submit"]').bind('click', function (event) {
                    if (event) event.preventDefault()
                    register2($form_two)
                })
            }
            var $form_three = $('#mc-embedded-subscribe-form-three')
            if ($form_three.length > 0) {
                $('#mc-embedded-subscribe-form-three input[type="submit"]').bind('click', function (event) {
                    if (event) event.preventDefault()
                    register3($form_three)
                })
            }
        })
        function register($form) {
            // $('#mc-embedded-subscribe').val('Sending...');
            $.ajax({
                type: $form.attr('method'),
                url: $form.attr('action'),
                data: $form.serialize(),
                cache: false,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                error: function (err) { alert('Could not connect to the registration server. Please try again later.') },
                success: function (data) {
                    $('#mc-embedded-subscribe').val('subscribe')
                    if (data.result === 'success') {
                        $('#my-popup').show()
                        $('#subscribe-popup').hide()
                        $('#subscribe-result').css('color', 'rgb(53, 114, 210)')
                        $('#subscribe-result').html('<p>Thank you for subscribing. We have sent you a confirmation email.</p>')
                        $('.modal-body #mce-EMAIL').val('')
                        $('.modal-body #mce-FNAME').val('')
                        $('.modal-body #mce-LNAME').val('')
                        $('.modal-body #mce-EMAIL').css('borderColor', '#dedede')
                    } else {
                        $('#my-popup').hide()
                        $('#subscribe-popup').show()
                        $('.modal-body #mce-EMAIL').css('borderColor', 'red')
                        $('.modal-body #mce-FNAME').css('borderColor', 'red')
                        $('.modal-body #mce-LNAME').css('borderColor', 'red')
                        $('#subscribe-result').css('color', '#ff8282')
                        $('#subscribe-result').html('<p>' + data.msg.substring(4) + '</p>')
                    }
                }
            })
        };
        function register2($form_two) {
            // $('#mc-embedded-subscribe').val('Sending...');
            $.ajax({
                type: $form_two.attr('method'),
                url: $form_two.attr('action'),
                data: $form_two.serialize(),
                cache: false,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                error: function (err) { alert('Could not connect to the registration server. Please try again later.') },
                success: function (data) {
                    $('#mc-embedded-subscribe').val('subscribe')
                    if (data.result === 'success') {
                        $('#subscribe-result-two').css('color', 'rgb(53, 114, 210)')
                        $('#subscribe-result-two').html('<p>Thank you for subscribing. We have sent you a confirmation email.</p>')
                        $('.long-subscribe-btn #mce-EMAIL').val('')
                        $('.lightbox .content .bottom-box .footer-subscribe-btn #mc_embed_signup .mc-field-group input#mce-EMAIL').css('borderColor', '#dedede')

                    } else {
                        // console.log(data.msg)
                        $('.lightbox .content .bottom-box .footer-subscribe-btn #mc_embed_signup .mc-field-group input#mce-EMAIL').css('borderColor', 'red')
                        $('.lightbox .content .bottom-box .footer-subscribe-btn #mc_embed_signup .mc-field-group input#mce-FNAME').css('borderColor', 'red')
                        $('.lightbox .content .bottom-box .footer-subscribe-btn #mc_embed_signup .mc-field-group input#mce-LNAME').css('borderColor', 'red')
                        $('#subscribe-result-two').css('color', '#ff8282')
                        $('#subscribe-result-two').html('<p>' + data.msg.substring(4) + '</p>')
                    }
                }
            })
        };
        function register3($form_three) {
            // $('#mc-embedded-subscribe').val('Sending...');
            $.ajax({
                type: $form_three.attr('method'),
                url: $form_three.attr('action'),
                data: $form_three.serialize(),
                cache: false,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                error: function (err) { alert('Could not connect to the registration server. Please try again later.') },
                success: function (data) {
                    $('#mc-embedded-subscribe').val('subscribe')
                    if (data.result === 'success') {
                        $('#subscribe-result-three').css('color', 'rgb(53, 114, 210)')
                        $('#subscribe-result-three').html('<p>Thank you for subscribing. We have sent you a confirmation email.</p>')
                        $('.long-subscribe-btn #mce-EMAIL').val('')
                        $('.long-subscribe-btn #mce-EMAIL').css('borderColor', '#dedede')

                    } else {
                        console.log(data.msg)
                        $('.long-subscribe-btn #mce-EMAIL').css('borderColor', 'red')
                        $('.long-subscribe-btn #mce-FNAME').css('borderColor', 'red')
                        $('.long-subscribe-btn #mce-LNAME').css('borderColor', 'red')
                        $('#subscribe-result-three').css('color', '#ff8282')
                        $('#subscribe-result-three').html('<p>' + data.msg.substring(4) + '</p>')
                    }
                }
            })
        };
        window.onscroll = function() {myFunction()};
        function myFunction() {
            var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
            var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
            var scrolled = (winScroll / height) * 100;
            document.getElementById("myBar").style.width = scrolled + "%";
        }

    </script>
    <script defer>



        // Exit intent
        function addEvent(obj, evt, fn) {
            if (obj.addEventListener) {
                obj.addEventListener(evt, fn, false);
            }
            else if (obj.attachEvent) {
                obj.attachEvent("on" + evt, fn);
            }
        }


        $(document).ready(function(){
            const popup = [ "lightbox_1" , "lightbox_2"];
            const random = Math.floor((Math.random()) * popup.length);
            // console.log(random,popup[random]);

            function setCookie(name,value,days) {
                var expires = "";
                if (days) {
                    var date = new Date();
                    date.setTime(date.getTime() + (days*24*60*60*1000));
                    expires = "; expires=" + date.toUTCString();
                }
                document.cookie = name + "=" + (value || "") + expires + "; path=/";
            }
            function getCookie(name) {
                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for(var i=0;i < ca.length;i++) {
                    var c = ca[i];
                    while (c.charAt(0)==' ') c = c.substring(1,c.length);
                    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
                }
                return null;
            }
            document.addEventListener("mouseleave", function(e){
                var x = getCookie('popup_close');
                if(!x && (e.clientY < 0 )) {
                    if(random == 1) {
                        $('.lightbox_1').fadeOut();
                        $('.lightbox_2').fadeIn();

                    }
                    setCookie('popup_close', '1', 1);
                   if (random == 0) {
                        $('.lightbox_2').fadeOut();
                        $('.lightbox_1').fadeIn();
                    }
                }

            }, false);
        });


        // Closing the Popup Box
        $('.lightbox_1 span#close').click(function(){
            $('.lightbox_1').fadeOut();
        });
        $('.lightbox_2 span#close').click(function(){
            $('.lightbox_2').fadeOut();
        });
        jQuery(".content-box h1").click(function () {
            jQuery(this).siblings('ol').slideToggle();
        });


        jQuery("#exampleModal").click(function () {
            jQuery("#my-popup").hide();
            jQuery("#subscribe-popup").show();
        });




    </script>


    <script defer>
        $(document).on('click', 'a[href^="#"]', function (event) {
            event.preventDefault();

            $('html, body').animate({
                scrollTop: $($.attr(this, 'href')).offset().top - 120
            }, 500);
        });
    </script>

    <script>
        var btn = $('#button');

        $(window).scroll(function() {
        if ($(window).scrollTop() > 300) {
        btn.addClass('show');
        } else {
        btn.removeClass('show');
        }
        });

        btn.on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({scrollTop:0}, '300');
        });
    </script>

    <script>
        jQuery(document).ready(function () {

            if ($('.list-box ol li a').click()) {
                var ToC =
                    "<nav role='navigation'>" +
                    "<h3><span>Table of Contents</span></h3>" +
                    "<ol>";

                var newLine, el, title, link;

                $(".jtt-post-content h2").each(function() {

                    el = $(this);
                    title = el.text();
                    link = "#" + el.attr("id");

                    newLine =
                        "<li>" +
                        "<a href='" + link + "'>" +
                        title +
                        "</a>" +
                        "</li>";

                    ToC += newLine;

                });

                ToC +=
                    "</ol>" +
                    "</nav>";

                $(".list-box").prepend(ToC);

                // $('h2').each(function () {
                //     $(this).removeAttr( 'id','title-' + index);
            }
            jQuery(".list-box nav h3").click(function () {
                jQuery(this).next('ol').slideToggle();
            });

            var sidebar =
                "<div class='navigation-side' id='summery-list'>" +
                "<ul>";

            var newLine1, e2, title1, link1;

            $(".jtt-post-content h2").each(function() {

                e2 = $(this);
                title1 = e2.text();
                link1 = "#" + e2.attr("id");

                newLine1 =
                    "<li>" +
                    "<a href='" + link1 + "'>" +
                    title1 +
                    "</a>" +
                    "</li>";

                sidebar += newLine1;

            });

            sidebar +=
                "</ul>" +
                "</div>";

            $("#summery-list").prepend(sidebar);


            var lastId,
                topMenu = $("#summery-list"),
                topMenuHeight = 100,
                // All list items
                menuItems = topMenu.find("a"),
                // Anchors corresponding to menu items
                scrollItems = menuItems.map(function () {
                    var item = $($(this).attr("href"));
                    if (item.length) {
                        return item;
                    }
                });

            // menuItems.click(function (e) {
            //     var href = $(this).attr("href"),
            //         offsetTop = href === "#" ? 0 : $(href).offset().top - topMenuHeight + 1;
            //     $('html, body').stop().animate({
            //         scrollTop: offsetTop
            //     }, 500);
            //     e.preventDefault();
            // });

            $(window).scroll(function () {
                var fromTop = $(this).scrollTop() + topMenuHeight;

                var cur = scrollItems.map(function () {
                    if ($(this).offset().top - 110 < fromTop)
                        return this;
                });
                cur = cur[cur.length - 1];
                var id = cur && cur.length ? cur[0].id : "";
                if (lastId !== id) {
                    lastId = id;
                    menuItems
                        .parent().removeClass("active")
                        .end().filter("[href='#" + id + "']").parent().addClass("active");
                }
            });
            $(".list-box ol li a").removeAttr("target","_blank");
        });


        // scrollbar js
        jQuery(document).ready(function () {
            jQuery(window).on("scroll", function () {
                if (jQuery(window).scrollTop() > 150) {
                    jQuery(".progress-container").before(jQuery(".progress-container").addClass("animateIt"));
                } else {
                    jQuery(".progress-container").before(jQuery(".progress-container").removeClass("animateIt"));
                }
            });

            if (jQuery(".navbar-collapse").hasClass('in')) {
                jQuery('body').css('overflowY', 'hidden');
            }


        });
        window.onscroll = function () {
            myFunction()
        };
        function myFunction() {
            var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
            var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
            var scrolled = (winScroll / height) * 100;
            document.getElementById("myBar").style.width = scrolled + "%";
        }

    </script>

<?php get_footer();
