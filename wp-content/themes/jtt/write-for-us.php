<?php
/* Template Name: write for us page */
get_header(); ?>
<link href="<?php bloginfo('template_directory'); ?>/assets/css/custom-new-2.css" media="screen" rel="stylesheet"
      type="text/css">

    <div class="policy-page write-for-us">
        <div class="top-panel text-center">
            <h1>Write for Us</h1>
        </div>
        <div class="container">
            <div class="content-panel">
                <p>With advancements in technology, we see technology enthusiasts, tech geeks, and AI professionals interested in the latest happenings in technology. However, there is no single resource available on the internet, where you can browse the latest tech stories and join the next conversation. This is where the JTT blog site comes into the picture.</p>
                <h3>The Just Total Tech Mission</h3>
                <p>Just Total Tech is one of the leading media platforms dedicated solely to providing the latest news about new technologies emerging in the digital era. We collaborate with the top industry thinkers and companies and enumerate stories that drive the world to the next level. </p>
                <p>Our sole objective is to bring you the latest technology happenings, whether it is AI, IoT, or Big Data. Our blog site’s content is intended to bring out new technologies that will significantly impact every industry it touches. </p>

                <h3>Our Posting Focus</h3>
                <ul>
                    <li>Here are some of the domains that we focus on -
                        <ol>
                            <li>Technology - AI, Big Data, Blockchain, Cyber Security, and IoT</li>
                            <li>Entrepreneurship - Business Leadership and Small Business</li>
                            <li>Reviews - AI-Powered Tools and Project Management Tools</li>
                        </ol>
                    </li>
                    <li>We cover new topics that no one has ever thought of before.</li>
                    <li>We even cover existing topics, albeit with a new angle or insight.</li>
                </ul>
                <h3>Not Our Posting Focus</h3>
                <ul>
                    <li>We do not cover startup news, financial world news, press releases, etc. </li>
                    <li>We do not follow crowdfunding campaigns.</li>
                    <li>We do not review games.</li>
                    <li>We do not cover VR games as our posting topic.</li>
                </ul>
                <h3>Who Should Post at Just Total Tech (JTT)?</h3>
                <p>We accept guest posts from technology enthusiasts, tech geeks, and AI professionals.</p>
                <h3>How to contact us?</h3>
                <p>Send email via <a href="#"> hey@justtotaltech.com</a> containing information about yourself, writing focus, three title suggestions followed by a summary of 150 words, and past three published articles. </p>
                <h3>Guest Posting Guidelines</h3>
                <p>Just Total Tech is looking for guest contributors to add valuable content with current information about topics like technology encompassing AI, Big Data, Blockchain, Cyber Security, and IoT. </p>
                <p>You can even write an insightful entrepreneurship blog talking about business leadership or small businesses. </p>
                <p>We even accept reviews for AI-Powered Tools and project management tools. </p>
                <p>We always welcome high-quality guest contributions from experts who fit our content style. You need to write your content to attract tech enthusiasts and tech geeks who love reading about disruptive technologies and bringing innovative solutions to the world. </p>
                <h3>Ideal Length</h3>
                <ol>
                    <li>We look forward to posts that range between 1,500 to 3,000 words. Preferably, somewhere in the region of 2,000 to 3,000 words would be ideal.</li>
                    <li>We believe longer blog posts provide more relevant information that readers can use. This ultimately makes them come to our blog site more often.</li>
                    <li>Longer, in-depth posts also have the scope of being selected by some of our media partners.</li>
                </ol>
                <h3>Some Examples to Consider</h3>
                <p class="blog-links"><a target="_blank" href="https://justtotaltech.com/open-source-artificial-intelligence-software/">https://justtotaltech.com/open-source-artificial-intelligence-software/</a></p>
                <p class="blog-links"><a target="_blank" href="https://justtotaltech.com/artificial-intelligence-trends/">https://justtotaltech.com/artificial-intelligence-trends/</a></p>
                <p class="blog-links"><a target="_blank" href="https://justtotaltech.com/voice-tech/">https://justtotaltech.com/voice-tech/</a></p>
                <p class="blog-links"><a target="_blank" href="https://justtotaltech.com/best-augmented-reality-glasses/">https://justtotaltech.com/best-augmented-reality-glasses/</a></p>
                <p>The blogs you send us need to be extensively written, like the examples mentioned above. Always do thorough research to justify the topic at hand. However, ensure that each and every sub-section that you create has a distinct meaning attached to it. Also, do not forget to add a sub-title in the blog, which will be used as a meta description.</p>
                <h3>Citations, Links & Relationships</h3>
                <ol>
                    <li>We suggest you integrate the first two links in every post point to relevant Internal JTT content related to your topic.</li>
                    <li>Employ external links for any third-party perspectives, evidence, and statistics.</li>
                    <li>Never use Wikipedia as a source. We suggest you utilize research studies and major publications as a source instead.</li>
                    <li>Never overly self-promote your business. If we find anything too promotional, our editors will remove it. </li>
                    <li>Never link to any questionable sources or industries like pornography, casinos, payday loans, or pharmaceuticals.</li>
                    <li>It is strictly prohibited to purchase, sell, or trade links appearing on the post.</li>
                </ol>
                <h3>SEO Best Practices</h3>
                <ol>
                    <li>Always carry out HTML formatting when it comes to bulleted points, headers, bolding, etc.</li>
                    <li>Include shortened titles in the meta-title section.</li>
                    <li>Do not forget to include a meta description of roughly 150 characters while sending the post to us.</li>
                    <li>Use Alt tags for any images utilized in the blog section.</li>
                    <li>Do not go for keyword linking.</li>
                    <li>Cite all the quotes in the blog properly.</li>
                    <li>In case we find any link that is not valuable for the readers, we’ll remove it.</li>
                </ol>
                <h3>Formatting</h3>
                <ol>
                    <li>Write numbers from 1, 2, 3, 4, etc.</li>
                    <li>Write percentages as digits.</li>
                    <li>Utilize one space after punctuation.</li>
                    <li>Employ the Oxford comma format.</li>
                    <li>Capitalize all other words in headings except for articles (a, an, the, etc.), coordinating conjunctions (for, and, or, but, etc.), and prepositions consisting of three or fewer letters (in, one, at, etc.)</li>
                    <li>Put the headers in H2 tags and sub-headers in H3 and H4 tags.</li>
                    <li>Do not use spams or div tags.</li>
                </ol>
                <h3>Images</h3>
                <ol>
                    <li>Always ensure that the featured image is at least 650 pixels wide and 400 pixels tall. Ensure that the featured image appears appropriately before submitting your post.</li>
                    <li>Ensure that the alignment of the blog post photos are center-aligned unless they are aligned with the text. </li>
                    <li>Set the display size for all the images to “large.”</li>
                    <li>Upload a minimum of 1 image per blog post and place it within the first 150 words.</li>
                    <li>All images should contain alt tags and title attributes. You can utilize the title of the post or a brief photo description.</li>
                    <li>Add a credible source of an image by using the format of “Image Source.”</li>
                    <li>Never go for charts, graphs, or illustrations from other websites until our editorial staff approves it.</li>
                    <li>We do not entertain images that consist of a brand, logo, or external links depicted on them.</li>
                    <li>We do not accept Memes as acceptable images for our site.</li>
                </ol>
                <h3>Publishing Process</h3>
                <p>You can apply to become a contributor on our website by sending your unique ideas via <a href="#">hey@justtotaltech.com.</a></p>
                <p>Here’s how our publishing process works for reviewing your content:</p>
                <ol>
                    <li>Our Editorial team will scrutinize your post and ensure that it follows our editorial guidelines.</li>
                    <li>The post then undergoes a copyeditor review where the grammar, spelling mistakes, and content flow is adjudged. </li>
                    <li>Your post has a higher chance of getting published sooner if you swiftly respond and make your post’s desired changes. </li>
                    <li>The Editorial review timeline is of 1 week, i.e., seven days. </li>
                    <li>We will respond to you via email, informing whether we have accepted your post or not.</li>
                </ol>
                <h3>Terms & Conditions</h3>
                <ul>
                    <li>Our Editorial team holds the sole right of accepting or rejecting your blog posts.</li>
                    <li>Once your blog post has been published on our site, you will not have republication rights.</li>
                    <li>If all this sounds good to you, then we look forward to receiving your submissions. </li>
                </ul>
            </div>
        </div>
    </div>

<?php get_footer(); ?>
