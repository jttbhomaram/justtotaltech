<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>





<footer id="site-footer" class="site-footer" role="contentinfo">

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <?php get_template_part( 'template-parts/footer/footer', 'widgets' ); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <a href="<?php echo site_url(); ?>">
                <img alt="logo" src="<?php bloginfo('template_directory'); ?>/assets/images/footer-logo.svg">
                </a>
                <p>© Copyright <?php echo date("Y"); ?> JUST TOTAL TECH™</p>
            </div>

            <div class="col-md-8">
                <ul class="footer-links">
<!--                    <li>-->
<!--                        <a href="https://justtotaltech.com/about-us/">About Us</a>-->
<!--                    </li>-->
                    <li>
                        <a href="https://justtotaltech.com/write-for-us/">Write for us</a>
                    </li>
                    <li>
                        <a href="https://justtotaltech.com/privacy-policy/">Privacy policy</a>
                    </li>
                    <li>
                        <a href="https://justtotaltech.com/cookie-policy/">Cookies policy</a>
                    </li>
                    <li>
                        <a href="https://justtotaltech.com/contact-us/">Contact Us</a>
                    </li>
<!--                    <li>-->
<!--                        <a href="--><?php //echo site_url();?><!--/about-us/">About</a>-->
<!--                    </li>-->
                </ul>
                <div class="footer_social">
                    <a target="_blank" href="https://twitter.com/justtotaltech">
                        <img alt="logo" src="<?php bloginfo('template_directory'); ?>/assets/images/instagram.svg">
                    </a>
                    <a target="_blank" href="http://www.youtube.com/user/justtotaltech">
                        <img alt="logo" src="<?php bloginfo('template_directory'); ?>/assets/images/youtube.svg">
                    </a>
                    <a target="_blank" href="https://twitter.com/justtotaltech">
                        <img alt="logo" src="<?php bloginfo('template_directory'); ?>/assets/images/twitter.svg">
                    </a>
                    <a target="_blank" href="http://www.linkedin.com/company/just-total-tech-limited">
                        <img alt="logo" src="<?php bloginfo('template_directory'); ?>/assets/images/linkedin.svg">
                    </a>
                    <a target="_blank" href="https://www.facebook.com/justtotaltech">
                        <img alt="logo" src="<?php bloginfo('template_directory'); ?>/assets/images/facebook.svg">
                    </a>
                </div>
            </div>
        </div>
    </div>
</footer>




</div>


<!--<div class="home-announcement new-home-announcement popup_close" id="cookies-blk"-->
<!--     style="width: 300px;position:fixed;top:50%;left:0;opacity: 9;margin-left: auto;margin-right: auto;box-shadow: 0 14px 74px 0 rgba(0, 0, 0, 0.1);background-color: #ffffff;padding: 25px;">-->
<!--    <div class="">-->
<!--        <div class="home-ann-con">-->
<!--            <span id="close-btn"><img src="--><?php //bloginfo('template_directory'); ?><!--/assets/images/blog/input-arrow.svg" alt="input-arrow"/></span>-->
<!--            <img alt="cookies" src="--><?php //bloginfo('template_directory'); ?><!--/assets/images//blog/cookies.svg">-->
<!--            <p>Our website use cookies to provide you with a great experience and run this website effectively.</p>-->
<!--        </div>-->
<!--        <!--        <span class="setcookie">Deny Cookies.</span>-->
<!--        <span style="cursor: pointer;" class="setcookie" id="cookie-message-close">Accept</span>-->
<!--    </div>-->
<!--</div>-->



<div id="cookie-bar-bottom" class="cookie-bar">
    <div class="cookie-content">
<!--                    <span id="close-btn"><img src="--><?php //bloginfo('template_directory'); ?><!--/assets/images/blog/input-arrow.svg" alt="input-arrow"/></span>-->
                    <img class="cookies" alt="cookies" src="<?php bloginfo('template_directory'); ?>/assets/images//blog/cookies.svg">
        <p>
            Our website use cookies to provide you with a great experience and run this website effectively.
            <input id="cookie-hide" class="cookie-hide" onclick="this.parentNode.parentNode.style.display = 'none'" value="Accept" type="button">
        </p>
    </div>
</div>



<script>

    if (document.getElementById("contribute_email") !==null) {
        document.getElementById("contribute_email").onchange = function (e) {
            var email = e.target.value;

            var filter = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (!filter.test(email)) {
                alert('Please provide a valid email address : '+email);
            }else {
                alert('Thanks !'+email);
            }
        };
    }



    function check_toggle() {
        let cl = document.getElementById('s_form');
        if (cl.className.indexOf("f_toggle") !==-1)
            cl.classList.toggle('f_toggle');
    }

    function drop_list(elem=null) {
        var lis = document.querySelectorAll('#top-menu .sub-menu.f_toggle');
        if (elem == null && lis.length > 0) {
            let cc = lis[0].className+"";
            if (cc.indexOf("f_toggle") !==-1)
                lis[0].classList.toggle('f_toggle');
        }else if (elem !== null) {

            for (var x = 0; x < lis.length; x++) {
                if (lis[x].id !== elem)
                    lis[x].classList.remove('f_toggle');
            }
            document.getElementById(elem).classList.toggle('f_toggle');
        }
        return false;
    }

    function _getOffset( el ) {
        var _x = 0;
        var _y = 0;
        while( el && !isNaN( el.offsetLeft ) && !isNaN( el.offsetTop ) ) {
            _x += el.offsetLeft - el.scrollLeft;
            _y += el.offsetTop - el.scrollTop;
            el = el.offsetParent;
        }
        return { top: _y, left: _x };
    }


    window.addEventListener('load', ()=> {

        if (document.getElementById('left_content'))
            document.getElementById('left_content').setAttribute('style','height:'+document.getElementById('right_content').clientHeight+'px');

        var lis = document.querySelectorAll('#top-menu .menu-item-has-children');
        for (var x = 0; x < lis.length; x++) {
            let list = lis[x].children;
            for (var i = 0; i < list.length; i++) {
                var id = 'child-'+lis[x].id;
                list[i].setAttribute('onclick', 'drop_list("'+id+'")');
                list[i].removeAttribute('href');
                if (i > 0)
                    list[i].setAttribute('id', id);
            }

        }

    });

    var r = null;
    var s = null;
    if (document.getElementById('single_post_content')) {
        r = document.getElementById('single_post_content');
        s = document.getElementById('social-sidebar');
        var right_offset = _getOffset(r).top;
        var right_height = r.clientHeight;
        var side_bar_height = s.clientHeight;
        var side_bar_offset = s.offsetTop;
    }


    window.addEventListener('scroll', () => {
        var scroll = this.pageYOffset;

        /*if (scroll > 200) {
            document.getElementsByClassName('top-header-bar')[0].classList.add('header-fixed');
        }
        if (scroll < 15) {
            document.getElementsByClassName('top-header-bar')[0].classList.remove('header-fixed');
        }*/


        if (r !==null && s !== null) {
            var side_bar = document.getElementsByClassName('custom-sidebar');
            var length = (right_height - side_bar_height + right_offset);

            var height = s.clientHeight-70 + 'px';

            if (document.getElementById("post-sharing")) {
                var side_bar = document.getElementById("post-sharing");
                if (scroll < _getOffset(document.getElementById('social-sidebar')).top) {
                    side_bar.style.position = 'absolute';
                    side_bar.style.top = 0;

                } else if (scroll > length) {
                    side_bar.style.position = 'absolute';
                    side_bar.style.bottom = 0;
                    side_bar.style.top = 'auto';

                } else {
                    side_bar.style.position = 'fixed';
                    side_bar.style.height = height;
                    side_bar.style.top = '50px';
                }
            }
        }
    });
</script>



<!--<script>-->
<!--    $(document).ready(function(){-->
<!--        function setCookie(name,value,days) {-->
<!--            var expires = "";-->
<!--            if (days) {-->
<!--                var date = new Date();-->
<!--                date.setTime(date.getTime() + (days*24*60*60*1000));-->
<!--                expires = "; expires=" + date.toUTCString();-->
<!--            }-->
<!--            document.cookie = name + "=" + (value || "") + expires + "; path=/";-->
<!--        }-->
<!--        function getCookie(name) {-->
<!--            var nameEQ = name + "=";-->
<!--            var ca = document.cookie.split(';');-->
<!--            for(var i=0;i < ca.length;i++) {-->
<!--                var c = ca[i];-->
<!--                while (c.charAt(0)==' ') c = c.substring(1,c.length);-->
<!--                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);-->
<!--            }-->
<!--            return null;-->
<!--        }-->
<!--        // document.addEventListener("mouseleave", function(e){-->
<!--        //     var x = getCookie('popup_close');-->
<!--        //-->
<!--        //     if(!x && (e.clientY < 0)) {-->
<!--        //         // $('#bookdemo').modal('show');-->
<!--        //         setCookie('popup_close', '1', 1);-->
<!--        //     }-->
<!--        //-->
<!--        // }, false);-->
<!--        window.addEventListener('load', function(){-->
<!--            jQuery('setcookie').click(function(){-->
<!--                jQuery('#cookies-blk').hide();-->
<!--            });-->
<!---->
<!--            if (!Cookies.get("cookie-message-bar"))-->
<!--            {-->
<!--                jQuery('#cookies-blk').show();-->
<!--                Cookies.set('cookie-message-bar', true, { expires: 60 });-->
<!--            }-->
<!--        });-->
<!--    });-->
<!--</script>-->


<style>
    #cookie-bar-bottom{
        display: none;
    }
</style>

<script>
    document.addEventListener('DOMContentLoaded', function(e) {
        console.log(getCookie('ItsAllOk'));
        if (getCookie('ItsAllOk').length === 0) {
            document.getElementById('cookie-bar-bottom').style.display = 'block';
        }
        document.getElementById('cookie-hide').addEventListener('click', function(e) {
            createCookie('ItsAllOk', true, 1);
        })
    });



    var createCookie = function(name, value, days) {
        var expires;
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        }
        else {
            expires = "";
        }
        document.cookie = name + "=" + value + expires + "; path=/";
    }

    function getCookie(c_name) {
        if (document.cookie.length > 0) {
            c_start = document.cookie.indexOf(c_name + "=");
            if (c_start != -1) {
                c_start = c_start + c_name.length + 1;
                c_end = document.cookie.indexOf(";", c_start);
                if (c_end == -1) {
                    c_end = document.cookie.length;
                }
                return unescape(document.cookie.substring(c_start, c_end));
            }
        }
        return "";
    }



</script>


<script>
    $(document).ready(function () {
        $("#close-btn").click(function () {
            $("#cookie-bar-bottom").hide();
        });
    });
</script>


<script>
    $(document).ready(function () {
        jQuery(window).on("scroll", function () {
        if(jQuery(window).scrollTop() > 150){
        jQuery(".top-header-bar").before(jQuery(".top-header-bar").addClass("animateIt"));
        }else{
        jQuery(".top-header-bar").before(jQuery(".top-header-bar").removeClass("animateIt"));
        }
        });
    });
</script>


<script defer>
    window.onload = function() {
        $('.image-placeholder').hide();
    }
</script>

<?php wp_footer(); ?>
</body>
</html>
